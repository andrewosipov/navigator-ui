const _ = require('lodash')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const path = require('path')
const CircularDependencyPlugin = require('circular-dependency-plugin')
const SentryWebpackPlugin = require('@sentry/webpack-plugin')
const GitRevisionPlugin = require('git-revision-webpack-plugin')
const gitRevisionPlugin = new GitRevisionPlugin()

module.exports = (env, argv) => ({
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.s?css$/,
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: true }
          }
        ]
      },
      {
        test: /\.svg/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'svg-inline-loader',
            options: {
              removeSVGTagAttrs: false,
              idPrefix: true,
              removingTags: []
            }
          }
        ]
      }
    ]
  },
  devtool: argv.mode == 'production' ? 'source-map' : 'eval-source-map', // eslint-disable-line
  devServer: {
    historyApiFallback: true
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        sourceMap: true,
        uglifyOptions: {
          output: {
            comments: false
          }
        }
      })
    ]
  },
  resolve: {
    alias: {
      'src': path.resolve(__dirname, './src'),
      'public': path.resolve(__dirname, './public')
    }
  },
  plugins: _.compact([
    gitRevisionPlugin,
    new HtmlWebPackPlugin({
      template: './public/index.html',
      filename: './index.html'
    }),
    new CircularDependencyPlugin({
      // exclude detection of files based on a RegExp
      exclude: /node_modules/,
      // add errors to webpack instead of warnings
      failOnError: false,
      // allow import cycles that include an asyncronous import,
      // e.g. via import(/* webpackMode: "weak" */ './file.js')
      allowAsyncCycles: false,
      // set the current working directory for displaying module paths
      cwd: process.cwd()
    }),
    new webpack.DefinePlugin({
      SIM_ID: env ? (env.SIM_ID || 1) : 1,
      APP_MODE: env ? (JSON.stringify(env.APP_MODE) || null) : null,
      GLOBAL_DEFAULT_THEME: env ? (JSON.stringify(env.DEFAULT_THEME) || null) : null,
      TARGET: env ? (JSON.stringify(env.TARGET) || null) : null,
      DXP_ENVIRONMENT: env ? (JSON.stringify(env.DXP_ENVIRONMENT) || JSON.stringify('SAGAR')) : JSON.stringify('SAGAR'),
      SENTRY_RELEASE: JSON.stringify(gitRevisionPlugin.commithash())
    }),
    argv.mode == 'production' && new SentryWebpackPlugin({
      release: gitRevisionPlugin.commithash(),
      include: path.resolve(__dirname, './dist'),
      ignoreFile: '.sentrycliignore',
      ignore: ['node_modules', 'webpack.config.js']
    })
  ])
})
