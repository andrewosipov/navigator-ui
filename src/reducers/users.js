import User from '../models/User'
import { RECIEVE_ALL_USERS, ADD_USERS } from '../actions/users'
import { SECTOR_ID_TO_DECKS } from '../constants'

export default (state = [], action) => {
  switch (action.type) {
    case RECIEVE_ALL_USERS:
      if (state.length === 0) {
        return action.users
      }
      let users = [...state]
      return users

    case ADD_USERS: {
      const usersState = [...state]
      const newUsers = action.users.map(user => new User({
        id: user.user_id,
        name: user.userName,
        iconUrl: '/public/images/contacts/francisco-patton.png',
        x: user.x,
        y: user.y,
        sectorId: user.sectorId,
        venueName: 'venue',
        deck: SECTOR_ID_TO_DECKS[user.sectorId],
        isCrew: user.roleName !== 'Sailor',
        roleName: user.roleName,
        roleId: user.roleId,
        subrole: user.subrole,
        inElevator: false,
        venue: user.venue,
        cabin: undefined
      }))
      newUsers.forEach(newUser => {
        const userIndex = usersState.findIndex(u => u.id === newUser.id)
        if (userIndex === -1) {
          usersState.push(newUser)
        } else {
          usersState[userIndex] = newUser
        }
      })
      return usersState
    }

    default:
      return state
  }
}
