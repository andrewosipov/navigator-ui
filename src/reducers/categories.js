import { RECEIVE_CATEGORIES, SELECT_CATEGORY } from '../actions/categories'

export default (state = { categories: []}, action) => {
    switch (action.type) {
        case RECEIVE_CATEGORIES:
            return { ...state, categories: action.categories }
        case SELECT_CATEGORY:
            return { ...state, selected: action.categoryId, crewOnly: action.crewOnly }
        default:
            return state
    }
}
