import {
  CHANGE_DECK,
  RECEIVE_PATHS,
  RECEIVE_SELECTED_PATHS,
  CHANGE_OFFSET_X,
  CHANGE_OFFSET_Y,
  CHANGE_ROTATION,
  CHANGE_ZOOM,
  CHANGE_PERSPECTIVE_ROTATION,
  RESET,
  SELECT_VENUE,
  DESELECT_VENUE,
  SELECT_END_VENUE,
  SET_END_POINT,
  REQUEST_V_TO_V,
  REQUEST_X_TO_V,
  REQUEST_V_TO_X,
  REQUEST_X_TO_X,
  REQUEST_X_TO_USER,
  REQUEST_VENUE_TO_X,
  REQUEST_X_TO_VENUE,
  REQUEST_VENUE_TO_VENUE
} from '../actions/deck'
import { DIRECTIONS_ERASE_SELECTED } from '../actions/directions'
import { toInt } from '../helpers/misc'

const initialState = {
  deck: null,
  paths: [],

  offsetX: 0,
  offsetY: 0,
  zoom: 1,
  perspectiveZoom: 1,
  rotation: 0,
  perspectiveRotation: 0,

  selectedVenue: null,
  endVenue: null,
  // crewOnly: false,
  selectedPaths: [],
  isSelectedPathFetching: false,

  selectedVenueGlobalId: null,
  selectedVenueId: null,

  endPoint: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_DECK:
      let newState = {
        ...state,
        deck: toInt(action.deck)
      }
      return newState
    case RECEIVE_PATHS:
      return { ...state, paths: action.paths }
    case RECEIVE_SELECTED_PATHS:
      return { ...state, selectedPaths: action.paths, isSelectedPathFetching: false }
    case CHANGE_OFFSET_X:
      return { ...state, offsetX: toInt(action.offsetX) }
    case CHANGE_OFFSET_Y:
      return { ...state, offsetY: toInt(action.offsetY) }
    case CHANGE_ROTATION:
      return { ...state, rotation: toInt(action.rotation) }
    case CHANGE_ZOOM:
      return { ...state, [action.perspective ? 'perspectiveZoom' : 'zoom']: parseFloat(action.zoom) }
    case CHANGE_PERSPECTIVE_ROTATION:
      return { ...state, perspectiveRotation: toInt(action.perspectiveRotation) }
    case RESET:
      return {
        ...state,
        offsetX: initialState.offsetX,
        offsetY: initialState.offsetY,
        zoom: initialState.zoom,
        perspectiveZoom: initialState.zoom,
        rotation: initialState.rotation,
        perspectiveRotation: initialState.perspectiveRotation
      }
    case SELECT_VENUE:
      return { ...state, selectedVenue: action.venue }
    case DESELECT_VENUE:
      return { ...state, selectedVenue: null }
    case SELECT_END_VENUE:
      return { ...state, endVenue: action.venue }

    case SET_END_POINT:
      return { ...state, endPoint: { x: action.x, y: action.y, deck: action.deck } }
    case REQUEST_V_TO_V:
    case REQUEST_X_TO_V:
    case REQUEST_V_TO_X:
    case REQUEST_X_TO_X:
    case REQUEST_VENUE_TO_X:
    case REQUEST_X_TO_VENUE:
    case REQUEST_VENUE_TO_VENUE:
    case REQUEST_X_TO_USER:
    case DIRECTIONS_ERASE_SELECTED:
      return { ...state, selectedPaths: [], isSelectedPathFetching: true }

    default:
      return state
  }
}
