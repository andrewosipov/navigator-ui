import { DIRECTIONS_CHANGE_TARGET_TYPE, DIRECTIONS_SELECT, DIRECTIONS_ERASE_SELECTED } from '../actions/directions'
import { GO_BACK } from '../actions/app'
import {
  SELECT_VENUE,
  DESELECT_VENUE
} from '../actions/deck'

const initialState = {
  from: null,
  to: null,
  selectingTarget: 'to'
}

export default (state = initialState, action) => {
  switch (action.type) {
    case DIRECTIONS_SELECT:
      return { ...state, [state.selectingTarget]: action.target, selectingTarget: 'to' }
    case SELECT_VENUE:
      return { ...state, [state.selectingTarget]: action.venue, selectingTarget: 'to' }
    case DESELECT_VENUE:
      return { ...state, to: null }
    case DIRECTIONS_CHANGE_TARGET_TYPE:
      return { ...state, selectingTarget: action.targetType }
    case GO_BACK:
      return { ...state, selectingTarget: 'to' }
    case DIRECTIONS_ERASE_SELECTED:
      return { ...state, from: null, to: null }
    default:
      return state
  }
}
