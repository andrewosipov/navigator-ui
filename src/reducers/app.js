/* global TARGET, SIM_ID */
import { handleActions } from 'redux-actions'
import {
  GET_WINDOW_SIZE,
  SET_ORIENTATION,
  CHANGE_PAGE,
  CHANGE_DEBUG,
  CHANGE_THEME,
  CHANGE_CREW_ONLY,
  GO_BACK,
  RECEIVE_SHIP_HEADING,
  RECEIVE_USER_HEADING,
  SHOW_MODAL,
  HIDE_MODAL,
  RECEIVE_USER_POSITION,
  SET_USER_POSITION,
  UPDATE_USER,
  RECEIVE_PARTY,
  GET_USER,
  ADD_TO_PARTY,
  ADD_NEARBY_USERS,
  REMOVE_NEARBY_USERS,
  LOGIN,
  CHANGE_MAP_MODE,
  CHANGE_MAP_SUB_MODE,
  CHANGE_CONNECTION_TYPE,
  COMMUNICATION_CHANNEL_ESTABLISHED,
  UPDATE_USER_PREFERENCES
} from 'src/actions/app'
import { ERASE_INITIAL_VENUE_DATA } from 'src/actions/deck'
import User from 'src/models/User'
import {
  HEIGHT,
  NORMAL_MAP_MODE,
  HEATMAP_MAP_MODE,
  OCCUPANCY_MAP_SUB_MODE,
  LABELS_MAP_SUB_MODE,
  DEFAULT_THEME,
  CONNECTION_TYPES,
  ORIENTATION_PORTRAIT,
  ORIENTATION_LANDSCAPE
} from 'src/constants'
import { toInt } from 'src/helpers/misc'

const USER_HARDCODE = { // Dev needs
  1: {
    id: 12168,
    name: 'Nilson Anthony',
    cabin: '8142Z',
    ReservationGuestId: '55487-57228'
  },
  2: {
    id: 12168,
    name: 'Minn Lori',
    cabin: '10144',
    ReservationGuestId: '37187689-243741780'
  },
  1415: {
    id: 26103,
    cabin: '8142Z'
  }
}

const TOKEN_HARDCODE = { // Dev needs
  1: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55aWQiOiJiMzVhMzI1YS0wMzZhLTRhZDQtODgzNi02NDY5MjNlNjAxZGUiLCJzY29wZSI6WyJ0cnVzdCIsInJlYWQiLCJ3cml0ZSJdLCJleHAiOjE1NTEwODQzNzcsImF1dGhvcml0aWVzIjpbIlJPTEVfVFJVU1RFRF9DTElFTlQiXSwianRpIjoiMjhhMDc2NTctZmUxNC00NmZlLWJkYTMtZmNlNzQxMWQzMmUzIiwiY2xpZW50X2lkIjoiNDllMTQ2NzEtMzJkMy00MWU4LWI1NzktZDc0N2I1NGZmZDIzIn0.ezIZRYukgJn2Ho_Enwm2JB4Qmk9o3v6YQdXeq028zUFgUIjTUvYPPHygPeOHyk0gLi0-omor9eiMwgfpQi97ZLLISCDLbXAqfj8GD82LhsSytwNQCEROXQ4Ik9HMCPYFvELNGL2Yu5Sv_wU4rS1YCgiuU6OaYjeyDZWZHiCQUbw5XkUEofzTcoptXPx-pXxGvyD50zs-LWR7gEXrWrE9r7C8J0zb7dxIX43xH5ky17N9_ojDBo3ZKLwJ_hs_c0XFsIztucLcP5fKOmZdy6PlRDuBIdCP97UD_yUYpWM4O-4hO8xnJIxnVKfCsBxsCiZpJjt3m_tdY0CAcs-GySH8oA',
  2: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55aWQiOiJiMzVhMzI1YS0wMzZhLTRhZDQtODgzNi02NDY5MjNlNjAxZGUiLCJzY29wZSI6WyJ0cnVzdCIsInJlYWQiLCJ3cml0ZSJdLCJleHAiOjE1NTEwODQzNzcsImF1dGhvcml0aWVzIjpbIlJPTEVfVFJVU1RFRF9DTElFTlQiXSwianRpIjoiMjhhMDc2NTctZmUxNC00NmZlLWJkYTMtZmNlNzQxMWQzMmUzIiwiY2xpZW50X2lkIjoiNDllMTQ2NzEtMzJkMy00MWU4LWI1NzktZDc0N2I1NGZmZDIzIn0.ezIZRYukgJn2Ho_Enwm2JB4Qmk9o3v6YQdXeq028zUFgUIjTUvYPPHygPeOHyk0gLi0-omor9eiMwgfpQi97ZLLISCDLbXAqfj8GD82LhsSytwNQCEROXQ4Ik9HMCPYFvELNGL2Yu5Sv_wU4rS1YCgiuU6OaYjeyDZWZHiCQUbw5XkUEofzTcoptXPx-pXxGvyD50zs-LWR7gEXrWrE9r7C8J0zb7dxIX43xH5ky17N9_ojDBo3ZKLwJ_hs_c0XFsIztucLcP5fKOmZdy6PlRDuBIdCP97UD_yUYpWM4O-4hO8xnJIxnVKfCsBxsCiZpJjt3m_tdY0CAcs-GySH8oA',
  1415: null
}

const initialState = {
  page: 'home',
  visitedPages: [],
  theme: DEFAULT_THEME,
  orientation: ORIENTATION_LANDSCAPE,
  ready: TARGET === 'web',
  debug: false,
  crewOnly: false,
  mapMode: NORMAL_MAP_MODE,
  mapSubModes: [],
  shipHeading: 0,
  userHeading: 0,
  lastUserHeadingUpdate: 0,
  modalType: null,
  modalProps: {},
  accessToken: TOKEN_HARDCODE[SIM_ID], // eslint-disable-line
  nearbyUsers: [],
  user: {
    id: null,
    x: 10500,
    y: HEIGHT - 1500,
    deck: 6,
    sectorId: 1,
    isCrew: false,
    inElevator: false,
    cabin: null,
    ReservationGuestId: null,
    ...USER_HARDCODE[SIM_ID] // eslint-disable-line
  },
  userPreferences: {
    avoidStairs: false,
    useImperialSystem: false
  },
  party: [],
  connectionType: CONNECTION_TYPES.WIFI,

  venueId: null,
  globalVenueId: null
}

export default handleActions({
  [COMMUNICATION_CHANNEL_ESTABLISHED]: (state, action) => ({
    ...state,
    ready: !action.initialData.venueId && !action.initialData.globalVenueId,
    ...action.initialData
  }),

  [GET_WINDOW_SIZE]: (state, action) => ({
    ...state,
    windowSize: action.windowSize
  }),

  [SET_ORIENTATION]: (state, action) => ({
    ...state,
    orientation: action.orientation === ORIENTATION_PORTRAIT || action.orientation === ORIENTATION_LANDSCAPE ? action.orientation : state.orientation
  }),

  [CHANGE_PAGE]: (state, action) => {
    let visitedPages = [...state.visitedPages]
    if (action.saveVisited) {
      visitedPages = visitedPages.concat(state.page)
    }
    if (action.page === 'position') {
      let directionsPageIndex = visitedPages.lastIndexOf('position')
      if (
        directionsPageIndex !== -1 &&
                  !visitedPages.slice(directionsPageIndex).some(p => p !== 'position' && p !== 'catalog' && p !== 'search')
      ) {
        visitedPages.splice(directionsPageIndex)
      }
    }
    return { ...state, page: action.page, visitedPages }
  },

  [CHANGE_DEBUG]: (state, action) => ({
    ...state,
    debug: !!+action.debug
  }),

  [CHANGE_THEME]: (state, action) => {
    let theme = action.theme
    if (SIM_ID === 2 && theme === `light`) {
      theme = `bliss`
    }
    return { ...state, theme }
  },

  [GO_BACK]: (state) => {
    const visitedPages = [...state.visitedPages]
    let page = visitedPages.pop()
    return { ...state, page, visitedPages }
  },

  [RECEIVE_SHIP_HEADING]: (state, action) => ({
    ...state,
    shipHeading: action.heading
  }),

  [RECEIVE_USER_HEADING]: (state, action) => {
    const ts = +(new Date())
    if (ts - state.lastUserHeadingUpdate > 500) {
      return { ...state, userHeading: action.heading, lastUserHeadingUpdate: ts }
    }
    return state
  },

  [SHOW_MODAL]: (state, action) => ({
    ...state,
    modalType: action.modalType,
    modalProps: action.modalProps
  }),

  [HIDE_MODAL]: (state) => ({
    ...state,
    modalType: null,
    modalProps: {}
  }),

  [GET_USER]: (state, action) => ({
    ...state,
    user: { ...action.user }
  }),

  [SET_USER_POSITION]: (state, action) => ({
    ...state,
    user: {
      ...state.user,
      x: action.x,
      y: action.y,
      deck: action.deck,
      inElevator: action.inElevator
    }
  }),

  [UPDATE_USER]: (state, action) => ({
    ...state,
    user: {
      ...state.user,
      id: action.user.ReservationGuestId || state.user.id,
      ...action.user
    }
  }),

  [RECEIVE_USER_POSITION]: (state, action) => {
    if (action.user.id === state.user.id) {
      return { ...state, user: { ...state.user, ...action.user } }
    }
    let i = state.party.findIndex(u => u.id === action.user.id && u.hardcoded)
    if (i > -1) {
      let party = [...state.party]
      party.splice(i)
      party.push({ ...action.user, hardcoded: true })
      return { ...state, party }
    }
    return state
  },

  [CHANGE_CREW_ONLY]: (state, action) => ({
    ...state,
    user: {
      ...state.user,
      isCrew: !!+action.crewOnly
    },
    crewOnly: !!+action.crewOnly
  }),

  [ADD_TO_PARTY]: (state, action) => ({
    ...state,
    party: [
      ...state.party,
      new User({ ...action.user, isParty: true })
    ]
  }),

  [RECEIVE_PARTY]: (state, action) => ({
    ...state,
    party: [
      ...state.party.filter(u => u.hardcoded),
      ...action.party
    ]
  }),

  [ADD_NEARBY_USERS]: (state, action) => {
    const userState = [...state.nearbyUsers]
    const newUsers = action.users.map(user => new User(user))
    newUsers.forEach(newUser => {
      const userIndex = userState.findIndex(u => toInt(u.id) === toInt(newUser.id))
      if (userIndex === -1) {
        userState.push(newUser)
      } else {
        userState[userIndex] = newUser
      }
    })
    return { ...state, nearbyUsers: userState }
  },

  [REMOVE_NEARBY_USERS]: (state, action) => {
    const userState = [...state.nearbyUsers]
    const newUserState = userState
      .filter(user =>
        action.users.findIndex(u => toInt(u.id) === toInt(user.id)) === -1
      )
    return { ...state, nearbyUsers: newUserState }
  },

  [LOGIN]: (state, action) => ({
    ...state,
    accessToken: action.accessToken
  }),

  [CHANGE_MAP_MODE]: (state, action) => {
    let mapSubModes = []
    if (action.mapMode === HEATMAP_MAP_MODE) {
      mapSubModes = [OCCUPANCY_MAP_SUB_MODE, LABELS_MAP_SUB_MODE]
    }
    return { ...state, mapMode: action.mapMode, mapSubModes: mapSubModes }
  },

  [CHANGE_MAP_SUB_MODE]: (state, action) => {
    if (action.mapSubMode === '') { return { state } }

    const mapSubModes = [...state.mapSubModes]
    const index = mapSubModes.indexOf(action.mapSubMode)
    if (index === -1) {
      mapSubModes.push(action.mapSubMode)
    } else {
      mapSubModes.splice(index, 1)
    }
    return { ...state, mapSubModes: mapSubModes }
  },

  [CHANGE_CONNECTION_TYPE]: (state, action) => ({
    ...state,
    connectionType: action.connectionType
  }),

  [ERASE_INITIAL_VENUE_DATA]: (state, action) => ({
    ...state,
    venueId: null,
    globalVenueId: null,
    ready: true
  }),

  [UPDATE_USER_PREFERENCES]: (state, { payload: preferences }) => ({
    ...state,
    userPreferences: { ...state.userPreferences, ...preferences }
  })
}, initialState)
