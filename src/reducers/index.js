/* globals APP_MODE */
import { combineReducers } from 'redux'
import app from './app'
import deck from './deck'
import categories from './categories'
import directions from './directions'
import venues from './venues'
import shell from './shell'
import users from './users'
import roles from './roles'
import tv from './tv'

let reducers = {
  app,
  users,
  deck,
  shell,
  categories,
  venues,
  directions,
  roles
}

if (APP_MODE === 'tv') {
  reducers.tv = tv
}

export default combineReducers(reducers)
