import { RECEIVE_ALL_VENUES, REQUEST_VENUES, RECEIVE_CMS_VENUES } from '../actions/venues'
import Venue from 'src/models/Venue'

const initialState = {
  venues: [],
  nonServiceVenues: {},
  cmsVenues: {},
  isVenuesFetching: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_VENUES: {
      return { ...state, isVenuesFetching: true }
    }

    case RECEIVE_ALL_VENUES: {
      let venues = [...state.venues]
      let nonServiceVenues = { ...state.nonServiceVenues }
      action.venues.forEach(venue => {
        let venueIndex = venues.findIndex(v => v.id === venue.id)
        let venueData = venue
        if (venueData.globalVenueId && state.cmsVenues[venueData.globalVenueId]) {
          venueData = new Venue({ ...venue.toObject(), ...state.cmsVenues[venue.globalVenueId], time: venue.time })
        }
        if (venueIndex === -1) {
          venues.push(venueData)
        } else {
          if (venues[venueIndex].time) {
            venueData.time = venues[venueIndex].time
          }
          venues[venueIndex] = venueData
        }
        if (!venueData.isService()) {
          nonServiceVenues[venueData.id] = venueData
        }
      })
      return { ...state, venues, nonServiceVenues, isVenuesFetching: false }
    }

    case RECEIVE_CMS_VENUES: {
      let cmsVenues = {}
      action.cmsVenues.forEach(v => {
        cmsVenues[v.Id] = {
          description: v.values.description,
          images: v.values.images.map(i => i.link),
          thumbnails: v.values.thumbnails.map(i => i.link),
          name: v.values.displayName
        }
      })
      let nonServiceVenues = { ...state.nonServiceVenues }
      let venues = []
      state.venues.forEach(v => {
        let venue = v
        if (v.globalVenueId && cmsVenues[v.globalVenueId]) {
          venue = new Venue({ ...v.toObject(), ...cmsVenues[v.globalVenueId] })
        }
        venues.push(venue)
        if (!venue.isService()) {
          nonServiceVenues[venue.id] = venue
        }
      })
      return { ...state, cmsVenues, venues }
    }

    default:
      return state
  }
}
