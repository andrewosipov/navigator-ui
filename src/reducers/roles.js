import { RECEIVE_ROLES } from '../actions/roles'

export default (state = [], action) => {
    switch (action.type) {
        case RECEIVE_ROLES:
            return action.roles
        default:
            return state
    }
}
