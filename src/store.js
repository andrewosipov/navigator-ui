import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
import rootReducer from 'src/reducers'
import rootSaga from 'src/sagas'

const IGNORED_ACTIONS = [
  'CHANGE_OFFSET_X',
  'CHANGE_OFFSET_Y',
  'RECEIVE_SHIP_HEADING'
]

const shellActionsResender = store => next => action => {
  if (action) {
    if (!IGNORED_ACTIONS.includes(action.type)) {
      // console.log(action)
      window.provideInfoToShell(`wayfinder.${action.type}`, action)
    }
  }
  let result = next(action)
  return result
}
const sagaMiddleware = createSagaMiddleware()
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const middleware = [ thunk, shellActionsResender, sagaMiddleware ]

export const store = createStore(
  rootReducer,
  composeEnhancers(
    applyMiddleware(...middleware)
  )
)

setTimeout(() => {
  // this file could be imported in some imported modules from src/sagas,
  // so we resolve import issue with setTimeout
  sagaMiddleware.run(rootSaga)
}, 0)
