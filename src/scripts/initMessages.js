import * as fs from 'fs'
import { sync as globSync } from 'glob'

const MESSAGES_PATTERN = 'build/messages/**/*.json'
const LANG_DIR = 'src/translations'
const LANG_PATTERN = 'src/translations/*.json'

// Try to delete current json files from public/locales
try {
  fs.unlinkSync('src/translations/data.json')
} catch (error) {
  console.log(error)
}

// Merge translated json files (es.json, fr.json, etc) into one object
// so that they can be merged with the eggregated "en" object below

const mergedTranslations = globSync(LANG_PATTERN)
  .map(filename => {
    let p = filename.split('/')
    const locale = p[p.length - 1].split('.json')[0]
    return { [locale]: JSON.parse(fs.readFileSync(filename, 'utf8')) }
  })
  .reduce((acc, localeObj) => {
    console.log(acc, localeObj)
    return { ...acc, ...localeObj }
  }, {})

// Aggregates the default messages that were extracted from the example app's
// React components via the React Intl Babel plugin. An error will be thrown if
// there are messages in different components that use the same `id`. The result
// is a flat collection of `id: message` pairs for the app's default locale.

const defaultMessages = globSync(MESSAGES_PATTERN)
  .map(filename => fs.readFileSync(filename, 'utf8'))
  .map(file => JSON.parse(file))
  .reduce((collection, descriptors) => {
    descriptors.forEach(({ id, defaultMessage }) => {
      if (collection.hasOwnProperty(id)) {
        // throw new Error(`Duplicate message id: ${id}`)
        return
      }
      collection[id] = defaultMessage
    })

    return collection
  }, {})

// Merge aggregated default messages with the translated json files and
// write the messages to this directory
console.log(mergedTranslations)
fs.writeFileSync(
  `${LANG_DIR}/data.json`,
  JSON.stringify({ en: defaultMessages, ...mergedTranslations }, null, 2)
)
