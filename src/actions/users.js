import User from '../models/User'


export const RECIEVE_ALL_USERS = 'RECIEVE_ALL_USERS'
export const REQUEST_USERS = 'REQUEST_USERS'
export const ADD_USER = 'ADD_USER'
export const ADD_USERS = 'ADD_USERS'

export function requestUsers() {
    return { type: REQUEST_USERS }
}


export function reciveUsers(users) {
    return {
        type: RECIEVE_ALL_USERS,
        users
    }
}


export const fetchUsers = (user, crewOnly, token) => async dispatch => {
    dispatch(requestUsers())
    let users = await User.fetch(crewOnly, token)
    return dispatch(reciveUsers(users))
}


export function addUser(user = {}) {
    return {
        type: ADD_USERS,
        users: [user]
    }
}


export function addUsers(users = []) {
    return {
        type: ADD_USERS,
        users
    }
}
