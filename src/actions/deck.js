import Path from 'src/models/Path'

import {
  HEIGHT,
  DECK_TO_SECTORS_ID,
  SECTOR_ID_TO_DECKS
} from 'src/constants'
import { toInt } from 'src/helpers/misc'

export const CHANGE_DECK = 'CHANGE_DECK'
export const REQUEST_PATHS = 'REQUEST_PATHS'
export const RECEIVE_PATHS = 'RECEIVE_PATHS'
export const RECEIVE_SELECTED_PATHS = 'RECEIVE_SELECTED_PATHS'

export const REQUEST_V_TO_V = 'REQUEST_V_TO_V'
export const REQUEST_X_TO_V = 'REQUEST_X_TO_V'
export const REQUEST_V_TO_X = 'REQUEST_V_TO_X'
export const REQUEST_X_TO_X = 'REQUEST_X_TO_X'
export const REQUEST_VENUE_TO_X = 'REQUEST_VENUE_TO_X'
export const REQUEST_X_TO_VENUE = 'REQUEST_VENUE_TO_X'
export const REQUEST_VENUE_TO_VENUE = 'REQUEST_VENUE_TO_VENUE'
export const REQUEST_X_TO_USER = 'REQUEST_USER_TO_X'

export const CHANGE_OFFSET_X = 'CHANGE_OFFSET_X'
export const CHANGE_OFFSET_Y = 'CHANGE_OFFSET_Y'
export const CHANGE_ZOOM = 'CHANGE_ZOOM'
export const CHANGE_ROTATION = 'CHANGE_ROTATION'
export const CHANGE_PERSPECTIVE_ROTATION = 'CHANGE_PERSPECTIVE_ROTATION'

export const RESET = 'RESET'
export const TOGGLE_LETS_GO = 'TOGGLE_LETS_GO'

export const SELECT_VERTEX = 'SELECT_VERTEX'
export const SELECT_VENUE = 'SELECT_VENUE'
export const DESELECT_VENUE = 'DESELECT_VENUE'
export const SELECT_END_VENUE = 'SELECT_END_VENUE'
export const SET_END_POINT = 'SET_END_POINT'

export const ERASE_INITIAL_VENUE_DATA = 'ERASE_INITIAL_VENUE_DATA'

export const requestPaths = (sectorId) => ({
  type: REQUEST_PATHS,
  sectorId
})

export const receivePaths = (sectorId, paths) => ({
  type: RECEIVE_PATHS,
  sectorId,
  paths
})

export const fetchPaths = (sectorId) => async dispatch => {
  dispatch(requestPaths())
  let paths = await Path.fetch(sectorId)
  return dispatch(receivePaths(sectorId, paths))
}

export const changeDeck = (deck, debug = false) => async dispatch => {
  if (debug) {
    let sectorId = DECK_TO_SECTORS_ID[deck]
    dispatch(fetchPaths(sectorId))
  }
  return dispatch({
    type: CHANGE_DECK,
    deck
  })
}

export const changeOffsetX = (offsetX) => ({
  type: CHANGE_OFFSET_X,
  offsetX
})

export const changeOffsetY = (offsetY) => ({
  type: CHANGE_OFFSET_Y,
  offsetY
})

export const changeRotation = (rotation) => ({
  type: CHANGE_ROTATION,
  rotation
})

export const changeZoom = (zoom, perspective = false) => ({
  type: CHANGE_ZOOM,
  zoom,
  perspective
})

export const changePerspectiveRotation = (perspectiveRotation) => ({
  type: CHANGE_PERSPECTIVE_ROTATION,
  perspectiveRotation
})

export const reset = () => ({
  type: RESET
})

export const fetchUserToUserPath = (user, selectedUser, crewOnly, avoidStairs, insideOnly) => async dispatch => {
  dispatch({ type: REQUEST_X_TO_USER })
  let paths = await Path.fetchXtoUser(
    {
      x: user.x,
      y: user.y
    },
    selectedUser,
    DECK_TO_SECTORS_ID[user.deck],
    DECK_TO_SECTORS_ID[selectedUser.deck],
    crewOnly,
    avoidStairs,
    insideOnly
  )
  delayedPaths(paths, dispatch)
}

export const fetchUserToVenuePath = (user, selectedVenue, crewOnly, avoidStairs, insideOnly) => async dispatch => {
  dispatch({ type: REQUEST_X_TO_VENUE })
  let paths = await Path.fetchXtoVenue(
    {
      x: user.x,
      y: user.y
    },
    selectedVenue,
    DECK_TO_SECTORS_ID[user.deck],
    crewOnly,
    avoidStairs,
    insideOnly
  )
  delayedPaths(paths, dispatch)
}

export const fetchVenueToVenuePath = (selectedVenue, endVenue, crewOnly, avoidStairs, insideOnly) => async dispatch => {
  dispatch({ type: REQUEST_VENUE_TO_VENUE })
  let paths = await Path.fetchVenueToVenue(
    selectedVenue,
    endVenue,
    crewOnly,
    avoidStairs,
    insideOnly
  )
  delayedPaths(paths, dispatch)
}

export const fetchUserToXPath = (user, endPoint, crewOnly) => async dispatch => {
  dispatch({ type: REQUEST_X_TO_X })
  let paths = await Path.fetchXtoX(
    user.x,
    user.y,
    endPoint.x,
    endPoint.y,
    DECK_TO_SECTORS_ID[user.deck],
    DECK_TO_SECTORS_ID[endPoint.deck],
    crewOnly
  )
  delayedPaths(paths, dispatch)
}

export const fetchVenueToUserPath = (user, selectedVenue, crewOnly) => async dispatch => {
  dispatch({ type: REQUEST_VENUE_TO_X })
  let paths = await Path.fetchVenueToX(
    {
      x: user.x,
      y: HEIGHT - user.y
    },
    selectedVenue,
    DECK_TO_SECTORS_ID[user.deck],
    crewOnly
  )
  delayedPaths(paths, dispatch)
}

const delayedPaths = (paths, dispatch) => {
  dispatch({
    type: RECEIVE_SELECTED_PATHS,
    paths
  })
}

export const selectVenue = (venue) => ({
  type: SELECT_VENUE,
  venue
})

export const deselectVenue = () => ({
  type: DESELECT_VENUE
})

export const selectEndVenue = (venue) => ({
  type: SELECT_END_VENUE,
  venue
})

export const setEndPoint = (x, y, deck) => ({
  type: SET_END_POINT,
  x,
  y,
  deck
})

export const selectInitialVenue = (venues, venueId, globalVenueId) => async (dispatch, getState) => {
  let venue = null
  if (venueId) {
    venue = venues.find(v => v.id === toInt(venueId))
  } else if (globalVenueId) {
    venue = venues.find(v => v.globalVenueId === globalVenueId)
  }

  if (venue) {
    dispatch(changeDeck(SECTOR_ID_TO_DECKS[venue.sectorId]))
    dispatch(selectVenue(venue))
    setTimeout(() => {
      dispatch({
        type: ERASE_INITIAL_VENUE_DATA
      })
    }, 100)
  }
}
