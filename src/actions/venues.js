export const RECEIVE_ALL_VENUES = 'RECEIVE_ALL_VENUES'
export const REQUEST_VENUES = 'REQUEST_VENUES'
export const REQUEST_CMS_VENUES = 'REQUEST_CMS_VENUES'
export const RECEIVE_CMS_VENUES = 'RECEIVE_CMS_VENUES'

export function requestVenues (user, crewOnly, onlyPublic, sectorId) {
  return {
    type: REQUEST_VENUES,
    user,
    crewOnly,
    onlyPublic,
    sectorId
  }
}

export function receiveVenues (venues) {
  return {
    type: RECEIVE_ALL_VENUES,
    venues
  }
}

export function requestCMSVenues () {
  return {
    type: REQUEST_CMS_VENUES
  }
}

export function receiveCMSVenues (cmsVenues) {
  return {
    type: RECEIVE_CMS_VENUES,
    cmsVenues
  }
}
