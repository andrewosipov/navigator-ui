import { createAction } from 'redux-actions'
import API from 'src/helpers/api'
import Auth from 'src/helpers/auth.js'
import User from 'src/models/User'
import $ from 'jquery'
import { fetchCategories } from './categories'
import { requestVenues } from './venues'
import { fetchRoles } from './roles'
import { NORMAL_MAP_MODE, ORIENTATION_LANDSCAPE, ORIENTATION_PORTRAIT } from 'src/constants'

export const GET_WINDOW_SIZE = 'GET_WINDOW_SIZE'
export const SET_ORIENTATION = 'SET_ORIENTATION'
export const CHANGE_PAGE = 'CHANGE_PAGE'
export const CHANGE_DEBUG = 'CHANGE_DEBUG'
export const CHANGE_THEME = 'CHANGE_THEME'
export const GO_BACK = 'GO_BACK'
export const RECEIVE_SHIP_HEADING = 'RECEIVE_SHIP_HEADING'
export const RECEIVE_USER_HEADING = 'RECEIVE_USER_HEADING'
export const SHOW_MODAL = 'SHOW_MODAL'
export const HIDE_MODAL = 'HIDE_MODAL'

export const GET_USER = 'GET_USER'
export const RECEIVE_A_USER = 'RECEIVE_A_USER'
export const UPDATE_USER = 'UPDATE_USER'
export const CHANGE_CREW_ONLY = 'CHANGE_CREW_ONLY'
export const RECEIVE_USER_POSITION = 'RECEIVE_USER_POSITION'
export const SET_USER_POSITION = 'SET_USER_POSITION'

export const REQUEST_PARTY = 'REQUEST_PARTY'
export const RECEIVE_PARTY = 'RECEIVE_PARTY'
export const REQUEST_FRIENDS = 'REQUEST_FRIENDS'
export const RECEIVE_FRIENDS = 'RECEIVE_FRIENDS'
export const ADD_TO_PARTY = 'ADD_TO_PARTY'

export const ADD_NEARBY_USERS = 'ADD_NEARBY_USERS'
export const REMOVE_NEARBY_USERS = 'REMOVE_NEARBY_USERS'

export const LOGIN = 'LOGIN'

export const CHANGE_MAP_MODE = 'CHANGE_MAP_MODE'
export const CHANGE_MAP_SUB_MODE = 'CHANGE_MAP_SUB_MODE'

export const CHANGE_CONNECTION_TYPE = 'CHANGE_CONNECTION_TYPE'

export const COMMUNICATION_CHANNEL_ESTABLISHED = 'COMMUNICATION_CHANNEL_ESTABLISHED'

export const FETCH_USER_PREFERENCES = 'FETCH_USER_PREFERENCES'
export const UPDATE_USER_PREFERENCES = 'UPDATE_USER_PREFERENCES'

export const setUserPreferences = createAction(UPDATE_USER_PREFERENCES)
export const fetchUserPreferences = createAction(FETCH_USER_PREFERENCES)

export const getWindowSizes = () => ({
  type: GET_WINDOW_SIZE,
  windowSize: { width: $(window).width(), height: $(window).height() }
})

export const setOrientation = (param) => {
  let orientation = param
  if (!orientation) {
    orientation = $(window).width() >= $(window).height() ? ORIENTATION_LANDSCAPE : ORIENTATION_PORTRAIT
  }
  return {
    type: SET_ORIENTATION,
    orientation
  }
}

export const changePage = (page, saveVisited = true) => {
  const routeData = { saveVisited }
  window.provideInfoToShell('changePage', { page, routeData })
  return {
    type: CHANGE_PAGE,
    page,
    saveVisited
  }
}

export const goBack = () => ({
  type: GO_BACK
})

export const changeDebug = (debug) => ({
  type: CHANGE_DEBUG,
  debug
})

export const changeTheme = (theme) => ({
  type: CHANGE_THEME,
  theme
})

export const fetchShipHeading = () => async (dispatch) => {
  let { heading } = await API.getHeading()
  return dispatch({
    type: RECEIVE_SHIP_HEADING,
    heading
  })
}

export const receiveUserHeading = (heading) => ({
  type: RECEIVE_USER_HEADING,
  heading
})

export const showModal = (modalType, modalProps) => ({
  type: SHOW_MODAL,
  modalType,
  modalProps
})

export const hideModal = () => ({
  type: HIDE_MODAL
})

export const changeCrewOnly = (crewOnly) => async (dispatch, getState) => {
  let { user } = getState().app
  crewOnly = !!+crewOnly
  dispatch(fetchCategories(crewOnly))
  dispatch(requestVenues(user, crewOnly))
  dispatch({
    type: CHANGE_CREW_ONLY,
    crewOnly
  })
}

export const receiveUserPosition = (user) => ({
  type: RECEIVE_USER_POSITION,
  user
})

export const requestParty = (reservationGuestId, crewOnly = false, accessToken) => async (dispatch) => {
  dispatch({ type: REQUEST_PARTY })
  const party = await User.getParty(reservationGuestId, crewOnly, accessToken)
  return dispatch(receiveParty(party))
}

export const receiveParty = (party) => ({
  type: RECEIVE_PARTY,
  party
})

export const addToParty = (user) => (dispatch) => {
  dispatch({ type: ADD_TO_PARTY, user })
  // dispatch(requestParty())
}

export const addNearbyUser = (user = {}) => {
  return {
    type: ADD_NEARBY_USERS,
    users: [user]
  }
}

export const addNearbyUsers = (users = []) => {
  return {
    type: ADD_NEARBY_USERS,
    users: users
  }
}

export const removeNearbyUser = (user = {}) => {
  return {
    type: REMOVE_NEARBY_USERS,
    users: [user]
  }
}

export const removeNearbyUsers = (users = []) => {
  return {
    type: REMOVE_NEARBY_USERS,
    users: users
  }
}

export const getUser = () => (dispatch) => {
  const user = User.getUser()
  dispatch({ type: GET_USER, user: { ...user } })
  // return dispatch(receiveUser(result))
}

export const receiveUser = (user) => ({
  type: RECEIVE_A_USER,
  user
})

export const updateUser = (user) => ({
  type: UPDATE_USER,
  user
})

export const fetchReservationGuest = (reservationGuestId, accessToken) => async (dispatch) => {
  const data = await API.getReservationGuest(reservationGuestId, accessToken)
  if (data) {
    dispatch(updateUser({ cabin: data.stateroom }))
    if (reservationGuestId) {
      dispatch(requestParty(reservationGuestId, false, accessToken))
      dispatch(requestFriends(reservationGuestId, accessToken))
    }
  }
}

export const setUserPosition = (x, y, deck, inElevator = false) => ({
  type: SET_USER_POSITION,
  x,
  y,
  deck,
  inElevator
})

export const loginUser = (username, password) => async (dispatch) => {
  await Auth.init()
  const res = await Auth.login(username, password)
  const { accessToken } = res

  // todo save token somewhere
  let reservationGuestId = '10055-14329'

  dispatch(receiveAccessToken(accessToken))
  dispatch(fetchReservationGuest(reservationGuestId, accessToken))
  dispatch(fetchRoles(accessToken))
}

export const receiveAccessToken = (accessToken) => ({
  type: LOGIN,
  accessToken
})

export const changeMapMode = (mapMode = NORMAL_MAP_MODE) => ({
  type: CHANGE_MAP_MODE,
  mapMode
})

export const changeMapSubMode = (mapSubMode = '') => ({
  type: CHANGE_MAP_SUB_MODE,
  mapSubMode
})

export const changeConnection = connectionType => ({
  type: CHANGE_CONNECTION_TYPE,
  connectionType
})

export const communicationChannelEstablished = (initialData) => ({
  type: COMMUNICATION_CHANNEL_ESTABLISHED,
  initialData
})

export const requestFriends = (reservationGuestId, accessToken) => async (dispatch) => {
  dispatch({ type: REQUEST_FRIENDS })
  const friends = await API.getFriends(reservationGuestId, accessToken)
  return dispatch(receiveFriends(friends))
}

export const receiveFriends = (friends) => ({
  type: RECEIVE_FRIENDS,
  friends
})
