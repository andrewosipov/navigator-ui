import Category from '../models/Category'


export const RECEIVE_CATEGORIES = 'RECEIVE_CATEGORIES'
export const REQUEST_CATEGORIES = 'REQUEST_CATEGORIES'
export const SELECT_CATEGORY = 'SELECT_CATEGORY'


export function requestCategories() {
    return { type: REQUEST_CATEGORIES }
}


export const fetchCategories = (crewOnly) => async dispatch => {
    dispatch(requestCategories())
    let categories = await Category.fetch(crewOnly)
    return dispatch(receiveCategories(categories))
}


export function receiveCategories(categories) {
    return {
        type: RECEIVE_CATEGORIES,
        categories
    }
}


export function selectCategory(categoryId, crewOnly) {
    return {
        type: SELECT_CATEGORY,
        categoryId,
        crewOnly
    }
}