export const DIRECTIONS_CHANGE_TARGET_TYPE = 'DIRECTIONS_CHANGE_TO'
export const DIRECTIONS_SELECT = 'DIRECTIONS_SELECT'
export const DIRECTIONS_ERASE_SELECTED = 'DIRECTIONS_ERASE_SELECTED'

export const selectTarget = (target) => ({
  type: DIRECTIONS_SELECT,
  target
})

export const changeTargetType = (targetType) => ({
  type: DIRECTIONS_CHANGE_TARGET_TYPE,
  targetType
})

export const eraseSelectedDirections = () => ({
  type: DIRECTIONS_ERASE_SELECTED
})
