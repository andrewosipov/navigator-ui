export const SET_LOGGED_IN_USER = 'SET_LOGGED_IN_USER'
export const SET_CATALOG_OFFSET = 'SET_CATALOG_OFFSET'

export function setLoggedInUser (user) {
  return {
    type: SET_LOGGED_IN_USER,
    user
  }
}

export function setCatalogOffset (offset) {
  return {
    type: SET_CATALOG_OFFSET,
    offset
  }
}
