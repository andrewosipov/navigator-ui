import Role from '../models/Role'

export const RECEIVE_ROLES = 'RECEIVE_ROLES'
export const REQUEST_ROLES = 'REQUEST_ROLES'


export function requestRoles() {
    return { type: REQUEST_ROLES }
}


export const fetchRoles = (accessToken) => async dispatch => {
    dispatch(requestRoles())
    let roles = await Role.fetch(accessToken)
    return dispatch(receiveRoles(roles))
}


export function receiveRoles(roles) {
    return {
        type: RECEIVE_ROLES,
        roles
    }
}
