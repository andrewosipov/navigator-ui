import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import fetch from 'isomorphic-fetch'

import shellInitialize from './shell/shell_integration'

Enzyme.configure({ adapter: new Adapter() })

window.fetch = fetch;
shellInitialize()