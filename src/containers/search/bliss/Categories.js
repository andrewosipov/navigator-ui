import { connect } from 'react-redux'

import { changePage, goBack, showModal, addToParty } from 'src/actions/app'
import { selectTarget } from 'src/actions/directions'
import { selectCategory } from 'src/actions/categories'
import { selectVenue, changeDeck } from 'src/actions/deck'

import Categories from 'src/components/search/SearchComponent/bliss/Categories'

const mapStateToProps = state => ({
  categories: state.categories.categories,
  destinationDirection: state.app.visitedPages[state.app.visitedPages.length - 1] === 'position' && state.directions.selectingTarget,
  crewOnly: state.app.crewOnly,
  user: state.app.user,
  users: state.users,
  party: state.app.party,
  allVenues: state.venues.venues,
  visitedPages: state.app.visitedPages,
  theme: state.app.theme,
  accessToken: state.app.accessToken,
  roles: state.roles,
  connectionType: state.app.connectionType
})

const mapDispatchToProps = dispatch => ({
  changePage: (page) => dispatch(changePage(page)),
  selectVenue: (venue) => dispatch(selectVenue(venue)),
  changeDeck: deck => dispatch(changeDeck(deck)),
  goBack: () => dispatch(goBack()),
  selectTarget: (target) => dispatch(selectTarget(target)),
  selectCategory: (categoryId, crewOnly) => dispatch(selectCategory(categoryId, crewOnly)),
  showModal: (name, payload) => dispatch(showModal(name, payload)),
  addToParty: (user) => dispatch(addToParty(user))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Categories)
