import { connect } from 'react-redux'

import { changePage } from 'src/actions/app'
import { selectTarget } from 'src/actions/directions'
import { selectVenue, changeDeck } from 'src/actions/deck'

import ResultItem from 'src/components/search/ResultItem'

const mapStateToProps = state => ({
  crewOnly: state.app.crewOnly,
  theme: state.app.theme,
  visitedPages: state.app.visitedPages
})

const mapDispatchToProps = dispatch => ({
  selectTarget: (target) => dispatch(selectTarget(target)),
  selectVenue: (venue) => dispatch(selectVenue(venue)),
  changeDeck: deck => dispatch(changeDeck(deck)),
  changePage: (page) => dispatch(changePage(page))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResultItem)
