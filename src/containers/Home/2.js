import { connect } from 'react-redux'
import { changePage, showModal, hideModal, changeMapMode } from 'src/actions/app'
import {
  selectVenue,
  deselectVenue
} from 'src/actions/deck'
import { selectTarget } from 'src/actions/directions'
import Home from 'src/components/Home'

const mapStateToProps = state => ({
  deck: state.deck.deck,
  debug: state.app.debug,
  crewOnly: state.app.crewOnly,
  mapMode: state.app.mapMode,
  selectedVenue: state.deck.selectedVenue,
  directionTo: state.directions.to,
  modalType: state.app.modalType,
  venues: state.venues.venues,
  isVenuesFetching: state.venues.isVenuesFetching,
  user: state.app.user,
  theme: state.app.theme,
  connectionType: state.app.connectionType
})

const mapDispatchToProps = dispatch => ({
  changePage: (page) => dispatch(changePage(page)),
  selectVenue: (venue) => dispatch(selectVenue(venue)),
  deselectVenue: () => dispatch(deselectVenue()),
  selectTarget: (target) => dispatch(selectTarget(target)),
  showModal: (name, payload) => dispatch(showModal(name, payload)),
  hideModal: () => dispatch(hideModal()),
  changeMapMode: (mode) => dispatch(changeMapMode(mode))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
