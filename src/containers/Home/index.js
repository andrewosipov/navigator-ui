/* global SIM_ID */
let component
try {
  component = require(`./${SIM_ID}.js`)
} catch (e) {
  component = require(`./default.js`)
}
export default component.default
