import { connect } from 'react-redux'
import { hideModal } from 'src/actions/app'
import { changeDeck } from 'src/actions/deck'
import { DECK_TO_SECTORS_ID } from 'src/constants'
import SelectDeck from 'src/components/select-deck/SelectDeck'

const mapStateToProps = state => ({
  currentDeck: state.deck.deck,
  decks: Object.keys(DECK_TO_SECTORS_ID).map(deck => parseInt(deck, 10)).slice(4),
  theme: state.app.theme
})

const mapDispatchToProps = dispatch => ({
  onClose: () => dispatch(hideModal()),
  onSelect: deck => {
    dispatch(changeDeck(deck))
    dispatch(hideModal())
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectDeck)
