import { connect } from 'react-redux'
import { changePage, goBack } from 'src/actions/app'
import { changeDeck } from 'src/actions/deck'
import SelectDeck from 'src/components/select-deck/SelectDeck'

const mapStateToProps = state => ({
  deck: state.deck.deck,
  venues: state.venues.venues,
  categories: state.categories.categories,
  user: state.app.user,
  debug: state.app.debug,
  crewOnly: state.app.crewOnly
})

const mapDispatchToProps = dispatch => ({
  changePage: (page) => dispatch(changePage(page)),
  changeDeck: (deck, debug) => dispatch(changeDeck(deck, debug)),
  goBack: () => dispatch(goBack())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectDeck)
