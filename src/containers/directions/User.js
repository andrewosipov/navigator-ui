import { connect } from 'react-redux'

import User from 'src/components/directions/User'

const mapStateToProps = state => ({
  theme: state.app.theme,
  connectionType: state.app.connectionType
})

export default connect(
  mapStateToProps
)(User)
