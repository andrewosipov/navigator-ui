import { connect } from 'react-redux'
import App from '../components/App'

const mapStateToProps = state => ({
  page: state.app.page,
  theme: state.app.theme,
  ready: state.app.ready
})

const mapDispatchToProps = dispatch => ({
  // requestUser: () => dispatch(getUser('self')),
  // requestParty: (user) => dispatch(requestParty(user))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
