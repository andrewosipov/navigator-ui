import { connect } from 'react-redux'
import { changePage, goBack } from '../actions/app'
import {
  fetchUserToVenuePath,
  fetchVenueToVenuePath,
  fetchUserToUserPath
} from '../actions/deck'
import { changeTargetType, eraseSelectedDirections, selectTarget } from '../actions/directions'
import Directions from '../components/directions/Directions'

const mapStateToProps = state => ({
  from: state.directions.from,
  to: state.directions.to,
  selectedPaths: state.deck.selectedPaths,
  isSelectedPathFetching: state.deck.isSelectedPathFetching,
  venues: state.venues.venues,
  users: state.users,
  user: state.app.user,
  avoidStairs: state.app.userPreferences.avoidStairs,
  useImperialSystem: state.app.userPreferences.useImperialSystem,
  theme: state.app.theme,
  crewOnly: state.app.crewOnly,
  heading: parseFloat(state.app.shipHeading) + parseFloat(state.app.userHeading),
  windowSize: state.app.windowSize,
  connectionType: state.app.connectionType
})

const mapDispatchToProps = dispatch => ({
  changePage: (page) => dispatch(changePage(page)),
  goBack: () => dispatch(goBack()),
  changeTargetType: (targetType) => dispatch(changeTargetType(targetType)),
  selectTarget: (target) => dispatch(selectTarget(target)),
  eraseSelectedDirections: () => dispatch(eraseSelectedDirections()),

  fetchUserToVenuePath: (user, selectedVenue, crewOnly, avoidStairs, insideOnly) => dispatch(fetchUserToVenuePath(user, selectedVenue, crewOnly, avoidStairs, insideOnly)),
  fetchVenueToVenuePath: (selectedVenue, endVenue, crewOnly, avoidStairs, insideOnly) => dispatch(fetchVenueToVenuePath(selectedVenue, endVenue, crewOnly, avoidStairs, insideOnly)),
  fetchUserToUserPath: (user, selectedUser, crewOnly, avoidStairs, insideOnly) => dispatch(fetchUserToUserPath(user, selectedUser, crewOnly, avoidStairs, insideOnly))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Directions)
