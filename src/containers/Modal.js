import { connect } from 'react-redux'

import Modal from '../components/common/Modal'
import { showModal, hideModal } from '../actions/app'

const mapStateToProps = state => ({
    modalType: state.app.modalType,
    modalProps: state.app.modalProps,
    page: state.app.page
})

const mapDispatchToProps = dispatch => ({
    showModal: (name, payload) => dispatch(showModal(name, payload)),
    hideModal: () => dispatch(hideModal()),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Modal)
