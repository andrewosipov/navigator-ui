import { connect } from 'react-redux'

import NavigationTip from 'src/components/navigation/NavigationTip'

const mapStateToProps = state => ({
  useImperialSystem: state.app.userPreferences.useImperialSystem
})

export default connect(
  mapStateToProps
)(NavigationTip)
