import { connect } from 'react-redux'

import NavigationInfo from 'src/components/navigation/NavigationInfo'

const mapStateToProps = state => ({
  useImperialSystem: state.app.userPreferences.useImperialSystem
})

export default connect(
  mapStateToProps
)(NavigationInfo)
