import { connect } from 'react-redux'
import MyParty from "../components/search/MyParty"
import {changePage} from "../actions/app"
import {selectTarget} from "../actions/directions"

const mapStateToProps = state => ({
    user: state.app.user,
    crewOnly: state.app.crewOnly,
    party: state.app.party
})

const mapDispatchToProps = dispatch => ({
    changePage: (page, saveVisited) => dispatch(changePage(page, saveVisited)),
    selectTarget: (target) => dispatch(selectTarget(target)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyParty)