import { connect } from 'react-redux'
import DynamicDeck from 'src/components/DynamicDeck'
import {
  changeOffsetX,
  changeOffsetY,
  changeRotation,
  changeZoom,
  changeDeck,
  selectVenue,
  deselectVenue,
  selectEndVenue
} from 'src/actions/deck'
import { showModal, setUserPosition } from 'src/actions/app'
import Venue from 'src/models/Venue'

const mapStateToProps = state => ({
  ...state.deck,
  debug: state.app.debug,
  user: state.app.user,
  users: state.users,
  crewOnly: state.app.crewOnly,
  mapMode: state.app.mapMode,
  mapSubModes: state.app.mapSubModes,
  categories: state.categories.categories,
  page: state.app.page,
  directionTo: state.directions.to,
  theme: state.app.theme,
  venues: Venue.getDeckVenues(state.venues.venues, state.deck.deck),
  selectedVenue: state.deck.selectedVenue,
  party: state.app.party
})

const mapDispatchToProps = dispatch => ({
  changeOffsetX: (offset) => dispatch(changeOffsetX(offset)),
  changeOffsetY: (offset) => dispatch(changeOffsetY(offset)),
  changeRotation: (rotation) => dispatch(changeRotation(rotation)),
  changeZoom: (zoom, perspective) => dispatch(changeZoom(zoom, perspective)),
  changeDeck: (deck, debug) => dispatch(changeDeck(deck, debug)),

  selectVenue: (venue) => dispatch(selectVenue(venue)),
  deselectVenue: () => dispatch(deselectVenue()),
  selectEndVenue: (venue) => dispatch(selectEndVenue(venue)),
  setUserPosition: (x, y, deck) => dispatch(setUserPosition(x, y, deck)),
  showModal: (name, payload) => dispatch(showModal(name, payload))
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(DynamicDeck)
