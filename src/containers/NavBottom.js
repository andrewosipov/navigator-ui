import { connect } from 'react-redux'
import { changeMapMode, changeMapSubMode, changePage, showModal } from '../actions/app'
import { selectTarget } from '../actions/directions'
import NavBottom from '../components/NavBottom'

const mapStateToProps = state => ({
  crewOnly: state.app.crewOnly,
  venues: state.venues.venues,
  user: state.app.user,
  users: state.users,
  deck: state.deck,
  theme: state.app.theme,
  mapMode: state.app.mapMode,
  mapSubModes: state.app.mapSubModes
})

const mapDispatchToProps = dispatch => ({
  changePage: (page) => dispatch(changePage(page)),
  showModal: (name, payload) => dispatch(showModal(name, payload)),
  selectTarget: (target) => dispatch(selectTarget(target)),
  changeMapMode: (mapMode) => dispatch(changeMapMode(mapMode)),
  changeMapSubMode: (mapMode) => dispatch(changeMapSubMode(mapMode))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NavBottom)
