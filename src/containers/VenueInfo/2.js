import { connect } from 'react-redux'
import { changePage } from 'src/actions/app'
import { deselectVenue } from 'src/actions/deck'
import { getSelectedVenue } from 'src/selectors/venues'
import VenueInfo from 'src/components/venues/VenueInfo'

const mapStateToProps = state => ({
  user: state.app.user,
  venue: getSelectedVenue(state),
  theme: state.app.theme
})

const mapDispatchToProps = dispatch => ({
  changePage: (page) => dispatch(changePage(page)),
  deselectVenue: () => dispatch(deselectVenue())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VenueInfo)
