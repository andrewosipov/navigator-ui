import { connect } from 'react-redux'

import { changePage, goBack, showModal, addToParty } from 'src/actions/app'
import { selectTarget } from 'src/actions/directions'
import { selectCategory } from 'src/actions/categories'
import { selectVenue, changeDeck } from 'src/actions/deck'

import Search from 'src/components/search/Search'

const mapStateToProps = state => ({
  categories: state.categories.categories,
  destinationDirection: state.app.visitedPages[state.app.visitedPages.length - 1] === 'position' && state.directions.selectingTarget,
  crewOnly: state.app.crewOnly,
  user: state.app.user,
  users: state.users,
  party: state.app.party,
  allVenues: state.venues.venues,
  nonServiceVenues: state.venues.nonServiceVenues,
  theme: state.app.theme,
  visitedPages: state.app.visitedPages,
  accessToken: state.app.accessToken,
  roles: state.roles,
  connectionType: state.app.connectionType
})

const mapDispatchToProps = dispatch => ({
  changePage: (page) => dispatch(changePage(page)),
  goBack: () => dispatch(goBack()),
  selectTarget: (target) => dispatch(selectTarget(target)),
  selectVenue: (venue) => dispatch(selectVenue(venue)),
  changeDeck: deck => dispatch(changeDeck(deck)),
  selectCategory: (categoryId, crewOnly) => dispatch(selectCategory(categoryId, crewOnly)),
  showModal: (name, payload) => dispatch(showModal(name, payload)),
  addToParty: (user) => dispatch(addToParty(user))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search)
