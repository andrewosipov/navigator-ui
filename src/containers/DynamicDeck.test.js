import React from 'react'
import { Provider } from 'react-redux'
import { mount } from 'enzyme'

import DynamicDeck from './DynamicDeck'
import data from '../helpers/tests/data'
import { store } from '../store'

test('DynamicDeck', () => {
  let component = mount(
    <Provider store={store}>
      <DynamicDeck
        user={data.user}
      />
    </Provider>
  )
  expect(component.find('.DynamicDeck').exists()).toBe(true)
})
