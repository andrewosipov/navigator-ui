import { connect } from 'react-redux'

import Venue from 'src/components/layers/elements/Venue'

const mapStateToProps = state => ({
  theme: state.app.theme,
  page: state.app.page
}) // TODO: move here as much props as possible

export default connect(
  mapStateToProps
)(Venue)
