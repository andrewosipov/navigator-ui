import { connect } from 'react-redux'
import { changePage, goBack } from '../actions/app'
import {
  fetchUserToVenuePath,
  fetchUserToXPath,
  fetchVenueToUserPath,
  fetchVenueToVenuePath
} from '../actions/deck'
import { changeTargetType, eraseSelectedDirections, selectTarget } from '../actions/directions'
import NavigationOverview from '../components/navigation/NavigationOverview'

const mapStateToProps = state => ({
  from: state.directions.from,
  to: state.directions.to,
  selectedPaths: state.deck.selectedPaths,
  venues: state.venues.venues,
  user: state.app.user,
  theme: state.app.theme,
  windowSize: state.app.windowSize,
  connectionType: state.app.connectionType
})

const mapDispatchToProps = dispatch => ({
  changePage: (page) => dispatch(changePage(page)),
  goBack: () => dispatch(goBack()),
  changeTargetType: (targetType) => dispatch(changeTargetType(targetType)),
  selectTarget: (target) => dispatch(selectTarget(target)),
  eraseSelectedDirections: () => dispatch(eraseSelectedDirections()),

  fetchUserToVenuePath: (user, selectedVenue, crewOnly) => dispatch(fetchUserToVenuePath(user, selectedVenue, crewOnly)),
  fetchUserToXPath: (user, endPoint, crewOnly) => dispatch(fetchUserToXPath(user, endPoint, crewOnly)),
  fetchVenueToUserPath: (user, selectedVenue, crewOnly) => dispatch(fetchVenueToUserPath(user, selectedVenue, crewOnly)),
  fetchVenueToVenuePath: (selectedVenue, endVenue, crewOnly) => dispatch(fetchVenueToVenuePath(selectedVenue, endVenue, crewOnly))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NavigationOverview)
