import { connect } from 'react-redux'

import User from 'src/components/layers/elements/User'

const mapStateToProps = state => ({
  heading: parseFloat(state.app.shipHeading) + parseFloat(state.app.userHeading),
  theme: state.app.theme,
  connectionType: state.app.connectionType
})

export default connect(
  mapStateToProps
)(User)
