import { connect } from 'react-redux'
import Navigation from '../components/navigation/Navigation'
import Venue from '../models/Venue'
import {
  changePage,
  goBack,
  showModal,
  setUserPosition
} from '../actions/app'
import {
  changeRotation,
  changeZoom,
  fetchUserToVenuePath,
  fetchUserToUserPath
} from '../actions/deck'
import { eraseSelectedDirections } from '../actions/directions'

const mapStateToProps = state => ({
  ...state.deck,
  user: state.app.user,
  theme: state.app.theme,
  venues: Venue.getDeckVenues(state.venues.venues, state.deck.deck),
  to: state.directions.to,
  heading: parseFloat(state.app.shipHeading) + parseFloat(state.app.userHeading),
  connectionType: state.app.connectionType
})

const mapDispatchToProps = dispatch => ({
  changeRotation: (rotation) => dispatch(changeRotation(rotation)),
  changeZoom: (zoom) => dispatch(changeZoom(zoom)),
  changePage: (page) => dispatch(changePage(page)),
  goBack: (page) => dispatch(goBack()),
  showModal: (name, payload) => dispatch(showModal(name, payload)),

  fetchUserToVenuePath: (user, selectedVenue, crewOnly) => dispatch(fetchUserToVenuePath(user, selectedVenue, crewOnly)),
  fetchUserToUserPath: (user, selectedUser, crewOnly) => dispatch(fetchUserToUserPath(user, selectedUser, crewOnly)),
  setUserPosition: (x, y, deck, inElevator) => dispatch(setUserPosition(x, y, deck, inElevator)),
  eraseSelectedDirections: () => dispatch(eraseSelectedDirections())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navigation)
