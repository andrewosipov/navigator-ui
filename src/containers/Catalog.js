import { connect } from 'react-redux'
import { changePage, goBack } from '../actions/app'
import { selectTarget } from '../actions/directions'
import { requestVenues } from '../actions/venues'
import Catalog from '../components/catalog/Catalog'

const mapStateToProps = state => ({
  categories: state.categories.categories,
  selected: state.categories.selected,
  crewOnly: state.app.crewOnly,
  crewOnlyCategories: state.categories.crewOnly,
  user: state.app.user,
  theme: state.app.theme,
  venues: state.venues.venues,
  windowSize: state.app.windowSize,
  connectionType: state.app.connectionType
})

const mapDispatchToProps = dispatch => ({
  changePage: (page, saveVisited) => dispatch(changePage(page, saveVisited)),
  selectTarget: (target) => dispatch(selectTarget(target)),
  goBack: () => dispatch(goBack()),
  fetchVenues: (user, crewOnly) => dispatch(requestVenues(user, crewOnly))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Catalog)
