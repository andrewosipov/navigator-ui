import Authentication from 'dxp-authentication-sdk'

const Auth = {

    init() {
        return Authentication.init({
            url: 'https://dev.virginvoyages.com/svc/user-account-service/',
            basicToken: 'Basic NGI4MDUzNjEtZmYwNi00MjViLTgxZTMtY2Y2NmE2ZWRmZDZmOnNlY3JldA=='
        })
        // }).then(success => {
        //     console.log('if success is true then you can start using authentication ', success)
        // })
    },

    getVersion() {
        return Authentication.version
    },

    login(email, password) {
        return Authentication.loginEmail({
            email: email,
            password: password
        })
    },

    logout() {
        Authentication.logout().then((isLoggedin) => {
            console.log('the users logged status:' , isLoggedin)
        })
    },

    getAuthToken() {
        Authentication.isAuth().then(isAuth => {
            if (isAuth) {
                Authentication.getAuthToken().then(token => {
                    console.log(token)
                })
            }
        })
    }
}

export default Auth




