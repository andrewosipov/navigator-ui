export const randomChoice = (items) => {
    return items[Math.floor(Math.random() * items.length)];
}

export const range = (a, b) => {
    if (b === undefined) {
        b = a;
        a = 0;
    }

    return Array.from(Array(b - a).keys()).map(i => i + a);
}

export const swap = (o) => {
    let ret = {};
    for (let key in o) {
        ret[o[key]] = key;
    }
    return ret;
}

export const last = (l) => {
    return l[l.length - 1]
}

export const deepClone = (source) => {
    return JSON.parse(JSON.stringify(source));
}