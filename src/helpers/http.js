import { APIError } from './exceptions'
import { store } from 'src/store'

const Http = {
  defaultHeaders: {
    'Content-Type': 'application/json'
  },

  async post (url, data, secured = false, dataType = 'json', headers = {}) {
    const response = await window.fetch(url, {
      method: 'post',
      body: JSON.stringify(data),
      headers: Object.assign({}, this._updateHeaders(secured), headers)
    })
    return this._handleResponse(dataType, response)
  },

  async get (url, secured = false, dataType = 'json', headers = {}, token = null) {
    const response = await window.fetch(url, {
      method: 'get',
      headers: Object.assign({}, this._updateHeaders(secured, token), headers)
    })
    return this._handleResponse(dataType, response)
  },

  async delete (url, secured = false, dataType = 'text', headers = {}) {
    const response = await window.fetch(url, {
      method: 'delete',
      headers: Object.assign({}, this._updateHeaders(secured), headers)
    })

    return this._handleResponse(dataType, response)
  },

  async put (url, data, secured = false, dataType = 'json', headers = {}) {
    const response = await window.fetch(url, {
      method: 'put',
      body: JSON.stringify(data),
      headers: Object.assign({}, this._updateHeaders(secured), headers)
    })

    return this._handleResponse(dataType, response)
  },

  _updateHeaders (secured, _token = null) {
    let accessToken = store.getState().app.accessToken
    let headers = { ...this.defaultHeaders }
    if (accessToken) {
      headers['Authorization'] = `Bearer ${accessToken}`
    }
    if (secured) {
      const token = _token || window.localStorage.getItem('x-auth-token')
      if (token) {
        headers['X-auth-token'] = token
      }
    }

    return headers
  },

  _handleResponse (dataType, response) {
    if (response.status === 200) {
      switch (dataType) {
        case 'json': return response.json()
        case 'text': return response.text()
        case 'headers': return response.headers
      }
    } else {
      let err = new APIError(`${response.status} ${response.statusText}`)
      throw err
    }
  }
}

export default Http
