/* eslint-env jest */
import * as misc from './misc'
import { resizeTo } from './tests/utils'

test('inRange()', () => {
  expect(misc.inRange(2, 0, 5)).toBe(2)
  expect(misc.inRange(3, -5, 5)).toBe(3)
  expect(misc.inRange(2, 3, 5)).toBe(3)
  expect(misc.inRange(2, 0, 1)).toBe(1)
})

test('parseParams()', () => {
  expect(misc.parseParams('param1=aaa&param2=bbb')).toEqual({
    param1: 'aaa',
    param2: 'bbb'
  })

  expect(misc.parseParams('#param1=aaa&param2=bbb')).toEqual({
    param1: 'aaa',
    param2: 'bbb'
  })
})

test('toInt()', () => {
  expect(misc.toInt('15')).toBe(15)
  expect(misc.toInt(15)).toBe(15)
  expect(misc.toInt(15.5)).toBe(15)
  expect(misc.toInt('-15')).toBe(-15)
})

test('isWideScreen()', () => {
  resizeTo(1024)
  expect(misc.isWideScreen()).toBe(true)

  resizeTo(428)
  expect(misc.isWideScreen()).toBe(false)
})

test('meterToFoot()', () => {
  expect(misc.meterToFoot(15)).toBe(49.213)
  expect(misc.meterToFoot(1)).toBe(3.281)
})
