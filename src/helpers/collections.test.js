import * as collections from './collections'

test('range()', () => {
    let l = collections.range(-10, 10)
    expect(l[0]).toBe(-10)
    expect(l[l.length - 1]).toBe(9)
    expect(l.length).toBe(20)
})

test('randomChoice()', () => {
    let min = -50000
    let max = 50000
    let l = collections.range(min, max)
    let a = collections.randomChoice(l)
    let b = collections.randomChoice(l)
    expect(a).not.toBe(b)
    expect(a).toBeGreaterThanOrEqual(min)
    expect(a).toBeLessThan(max)
})

test('swap()', () => {
    expect(collections.swap({ a: 1, b: 2})).toEqual({ 1: 'a', 2: 'b' })
})

test('last()', () => {
    expect(collections.last([1, 2, 3, 4, 5])).toBe(5)
})