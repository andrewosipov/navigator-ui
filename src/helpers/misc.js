import $ from 'jquery'
const FOOT_IN_METER = 3.28084

export const parseParams = (hash) => {
  let result = {}

  if (hash.startsWith('#')) {
    hash = hash.substr(1)
  }

  hash.split('&').forEach(p => {
    let [key, val] = p.split('=')
    result[key] = val
  })

  return result
}

export const inRange = (v, min, max) => {
  return Math.max(Math.min(v, max), min)
}

export const getMobileOperatingSystem = () => {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera

  // Windows Phone must come first because its UA also contains "Android"
  if (/windows phone/i.test(userAgent)) {
    return 'Windows Phone'
  }

  if (/android/i.test(userAgent)) {
    return 'Android'
  }

  // iOS detection from: http://stackoverflow.com/a/9039885/177710
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    return 'iOS'
  }

  return 'unknown'
}

export const toInt = (v) => {
  return parseInt(v, 10)
}

export const isWideScreen = () => {
  return $(window).width() >= 750
}

export const isSafari = () => {
  return /^((?!chrome|android).)*safari/i.test(navigator.userAgent)
}

export const meterToFoot = meters => Math.round(meters * FOOT_IN_METER * 1000) / 1000
