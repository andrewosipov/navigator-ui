let icons = {}
let full = {}
let tabs = {}

for (let i = 1; i <= 23; i++) {
  icons[i] = {}
  try {
    icons[i].public = require(`../../public/images/venue-categories/icons/category-${i}.svg`)
    icons[i].crew = require(`../../public/images/venue-categories/icons/category-${i}-crew.svg`)
    icons[i].tv = require(`../../public/images/venue-categories/icons/category-${i}-tv.svg`)
    icons[i].tv_venue = require(`../../public/images/venue-categories/icons/category-${i}-tv-venue.svg`)
  } catch (e) {
    try {
      icons[i].crew = require(`../../public/images/venue-categories/icons/category-${i}-crew.svg`)
    } catch (e) {

    }
    try {
      icons[i].tv = require(`../../public/images/venue-categories/icons/category-${i}-tv.svg`)
      icons[i].tv_venue = require(`../../public/images/venue-categories/icons/category-${i}-tv-venue.svg`)
    } catch (e) {

    }
  }

  try {
    full[i] = {}
    full[i].public = require(`../../public/images/venue-categories/full/category-${i}.svg`)
    full[i].crew = require(`../../public/images/venue-categories/full/category-${i}-crew.svg`)
  } catch (e) {
    try {
      full[i].crew = require(`../../public/images/venue-categories/full/category-${i}-crew.svg`)
    } catch (e) {

    }
  }

  try {
    tabs[i] = {}
    tabs[i].public = require(`../../public/images/venue-categories/tabs/category-${i}.svg`)
    tabs[i].crew = require(`../../public/images/venue-categories/tabs/category-${i}-crew.svg`)
  } catch (e) {
    try {
      tabs[i].crew = require(`../../public/images/venue-categories/tabs/category-${i}-crew.svg`)
    } catch (e) {

    }
  }
}

export { icons, full, tabs }
