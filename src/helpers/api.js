import { get } from 'lodash'
import {
  API_URL,
  DXP_URL,
  DXP_CMS_URL,
  DXP_SOCIAL_URL,
  DXP_MEDIA_URL,
  DECK_TO_SECTORS_ID,
  HEIGHT,
  VERTEX_TAGS
} from 'src/constants'
import Http from './http'
import { randomChoice } from './collections'

export const apiUrl = (resource) => `${API_URL}/${resource}`
/**
 * WayFinder REST Api queries. c - success callback, e - error handler
*/

let PARTY_MEMBERS = [] // TODO: GET RID OF ME
let FRIENDS = [] // TODO: GET RID OF ME

function formTags (crewOnly, avoidStairs = false, insideOnly = false) {
  const tags = []

  if (crewOnly) tags.push(VERTEX_TAGS['crew_only'])
  if (!avoidStairs) tags.push(VERTEX_TAGS['is_stairs'])
  if (!insideOnly) tags.push(VERTEX_TAGS['outside'])

  return tags
}

function getTagsQuery (tags) {
  if (!tags.length) return ''

  return `withTags=${tags.join(',')}`
}

const API = {
  async getPaths (sectorId) {
    return Http.get(apiUrl(`sectors/${sectorId}/paths`))
  },

  async pathXToX (x1, y1, x2, y2, startSectorId, endSectorId, crewOnly, avoidStairs, insideOnly) {
    const tags = formTags(crewOnly, avoidStairs, insideOnly)
    const tagsOuery = getTagsQuery(tags)
    return Http.get(apiUrl(`testtags/path_x_to_x?x1=${x1}&y1=${y1}&x2=${x2}&y2=${y2}&startSectorId=${startSectorId}&endSectorId=${endSectorId}&${tagsOuery}`), true, 'json', {})
  },

  async pathXToVenue ({ x, y }, venueId, startSectorId, crewOnly, avoidStairs, insideOnly) {
    const tags = formTags(crewOnly, avoidStairs, insideOnly)
    const tagsOuery = getTagsQuery(tags)
    return Http.get(apiUrl(`testtags/path_x_to_venue?venueId=${venueId}&x=${x}&y=${y}&startSectorId=${startSectorId}&${tagsOuery}`), true, 'json', {})
  },

  async pathVenueToX ({ x, y }, venueId, endSectorId, crewOnly) {
    const tags = formTags(crewOnly)
    const tagsOuery = getTagsQuery(tags)
    return Http.get(apiUrl(`testtags/path_venue_to_x?venueId=${venueId}&x=${x}&y=${y}&endSectorId=${endSectorId}&${tagsOuery}`), true, 'json', {})
  },

  async pathVenueToVenue (venue1Id, venue2Id, crewOnly, avoidStairs, insideOnly) {
    const tags = formTags(crewOnly, avoidStairs, insideOnly)
    const tagsOuery = getTagsQuery(tags)
    return Http.get(apiUrl(`testtags/path_venue_to_venue?venue1Id=${venue1Id}&venue2Id=${venue2Id}&${tagsOuery}`), true, 'json', {})
  },

  async getVenues (sectorId = null, q = null, categoryId = null, crewOnly = false, user = null, onlyPublic = false, ids = null) {
    let crewOnlyParam = crewOnly ? `sailorOnly=false&withTags=1` : ''
    /* eslint-disable */
    let params = `simId=${SIM_ID}`
    /* eslint-enable */
    if (q) {
      params += `&name=${q}`
    }
    if (categoryId) {
      params += `&categoryId=${categoryId}`
    }
    if (sectorId) {
      params += `&sectorId=${sectorId}`
    }
    if (ids) {
      params += `&ids=${ids}`
    }
    if (user) {
      params += `&x=${user.x}&y=${user.y}&startSectorId=${DECK_TO_SECTORS_ID[user.deck]}`
    } else if (onlyPublic) {
      params += `&onlyPublic=1`
    }
    return Http.get(apiUrl(`venues?${params}&${crewOnlyParam}`), true, 'json', {})
  },

  async getCategories (crewOnly = false) {
    let crewOnlyParam = crewOnly ? `sailorOnly=false` : ''
    return Http.get(apiUrl(`categories?${crewOnlyParam}`), true, 'json', {})
  },

  async getHeading () {
    return Http.get(apiUrl(`heading/1`))
  },

  /** * dxp endpoints ***/

  async getReservationGuest (reservationGuestId, token) {
    if (!DXP_URL) {
      return null
    }

    return Http.get(`${DXP_URL}/guest-service/reservationguests/${reservationGuestId}`)
  },

  async getRoles (token) {
    if (!DXP_URL) {
      return []
    }

    let roles = await Http.get(`${DXP_URL}/teammember-service/roles/?size=1000`)
    return (roles._embedded || { roles: [] }).roles
  },

  async getUsers (name, crewOnly, token, roles) {
    if (!DXP_URL) {
      return []
    }

    let result = FRIENDS.length ? FRIENDS : PARTY_MEMBERS
    result = result.filter(
      r => r.name.toLowerCase().includes(name.toLowerCase()) || (r.subrole && r.subrole.toLowerCase().includes(name.toLowerCase()))
    )
    if (crewOnly) {
      let crew = await Http.post(`${DXP_URL}/teammember-service/teammembers/search/findbynameandstateroom`, {
        name
      })
      let sailors = await Http.post(`${DXP_URL}/guest-service/guests/search/findbyfirstnameorlastname`, {
        firstName: name,
        lastName: name
      })
      sailors = (sailors._embedded || { guests: [] }).guests
      result = result.concat((crew._embedded || { teammembers: [] }).teammembers.map(r => {
        r.isCrew = true
        if (r.teamMemberRoles && r.teamMemberRoles.length) {
          r.subrole = roles.find(p => p.roleId === r.teamMemberRoles[0].roleId)
          if (r.subrole) {
            r.subrole = r.subrole.name
          }
        }
        return r
      }).concat(sailors).map(r => {
        r.name = `${r.firstName} ${r.lastName}`
        r.role = ''
        r.id = r.teamMemberId
        r.iconUrl = `/public/images/contacts/${randomChoice(['francisco-patton', 'bernice-guerrero', 'bettie-logan'])}.png`
        r.x = randomChoice([3053, 7406, 22770])
        r.y = randomChoice([2322, 2407, 1175])
        r.deck = randomChoice([5, 6, 7, 8, 9, 10, 15, 16])
        r.sectorId = DECK_TO_SECTORS_ID[r.deck]
        if (r.role) {
          r.subrole = randomChoice(['Waiter', 'Manager', 'Engineer'])
        }
        return r
      }))
    }
    return result
  },

  async getUser () {
    return {
      id: 12168,
      x: 10500,
      y: HEIGHT - 1500,
      deck: 6,
      isCrew: false,
      inElevator: false,
      cabin: 3529
    }
  },

  async getParty (reservationGuestId, crewOnly = false, token) {
    if (!DXP_URL) {
      return []
    }

    let reservationId = reservationGuestId.split('-')[0]
    let res = await Http.post(`${DXP_URL}/guest-service/reservationguests/search/findbyreservationids`, {
      reservationIds: [reservationId]
    })
    let friends = res._embedded.reservationGuests.map(f => ({
      id: `${reservationId}-${f.guestId}`,
      cabin: f.stateroom
    })).filter(f => f.id !== reservationGuestId)
    res = await getPersonalInformation(friends.map(f => f.id), token)
    friends = res.map(f => {
      let friend = friends.find(o => o.id === f.id)
      if (friend) {
        f.cabin = friend.cabin
      }
      return f
    })
    PARTY_MEMBERS = friends
    return friends
  },

  async getFriends (reservationGuestId, token) {
    if (!DXP_SOCIAL_URL) {
      return []
    }

    let res = await Http.get(`${DXP_SOCIAL_URL}/socialnetworking-service/userconnections/search/findbypersontypecodeandpersonids?personids=${reservationGuestId}&persontypecode=RG`)
    let friends = res._embedded.userconnections.map(f => ({
      id: f.connectedPersonId
      // cabin: f.stateroom
    })).filter(f => f.id !== reservationGuestId)
    friends = await getPersonalInformation(friends.map(f => f.id), token)
    FRIENDS = friends
    return friends
  },

  async getCMSVenues () {
    let res = await Http.get(`${DXP_CMS_URL}/content-management-service/resources/venues`)
    return res.fields
  },

  async getUserPreferences (reservationGuestId) {
    if (!DXP_URL) {
      return []
    }

    const result = await Http.post(`${DXP_URL}/guest-service/guestpreferences/search/findbyreservationguestids`, {
      reservationGuestIds: [reservationGuestId]
    })

    return get(result, '_embedded.guestPreferences', [])
  }
}

const getPersonalInformation = async (reservationGuestIds, token) => {
  if (!DXP_URL) {
    return []
  }

  let res = await Http.post(`${DXP_URL}/guest-service/reservationguests/search`, {
    reservationGuestIds
  })
  if (!res._embedded || !res._embedded.reservationGuestDetailsResponses) {
    return []
  }

  return res._embedded.reservationGuestDetailsResponses.map(d => {
    const iconId = get(d, 'reservationDetails[0].securityPhotoMediaItemId', null)
    const id = get(d, 'reservationDetails[0].reservationGuestId', null)
    const deck = randomChoice([5, 6, 7, 8, 9, 10, 15, 16])
    return {
      id,
      deck,
      name: `${d.personalDetails.firstName} ${d.personalDetails.lastName}`,
      x: randomChoice([3053, 7406, 22770]),
      y: randomChoice([2322, 2407, 1175]),
      sectorId: DECK_TO_SECTORS_ID[deck],
      isCrew: false,
      inElevator: false,
      optOut: false,
      subrole: '',
      venueName: randomChoice(['Lido Restaurant', 'AC Station Deck 6-2', 'Athletic Club Area']),
      iconUrl: iconId && `${DXP_MEDIA_URL}/multimediastorage-service/mediaitems/${iconId}?access_token=${token}`
    }
  })
}

export default API
