import fetch from 'isomorphic-fetch';
import API from './api';
import { DECK_TO_SECTORS_ID } from '../constants';

window.fetch = fetch;

test('get paths', async () => {
    const sectorId = Object.values(DECK_TO_SECTORS_ID)[0];
    let paths = await API.getPaths(sectorId);
    paths = paths.path;
    expect(paths.length).toBeGreaterThan(0);

    const p = paths[0];
    expect(p).toHaveProperty('vertexes');
    expect(p.vertexes).toHaveProperty('begin');
    expect(p.vertexes).toHaveProperty('end');
    expect(p.vertexes.begin).toHaveProperty('x');
    expect(p.vertexes.begin).toHaveProperty('y');
    expect(p.vertexes.begin).toHaveProperty('id');
    expect(p.vertexes.begin).toHaveProperty('name');
    expect(p.vertexes.end).toHaveProperty('x');
    expect(p.vertexes.end).toHaveProperty('y');
    expect(p.vertexes.end).toHaveProperty('id');
    expect(p.vertexes.end).toHaveProperty('name');
});
