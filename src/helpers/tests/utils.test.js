import $ from 'jquery'
import { shallow } from 'enzyme'

import { resizeTo } from './utils'

test('resizeTo', () => {
    expect($(window).width()).not.toBe(1000)
    expect($(window).height()).not.toBe(600)
    resizeTo(1000, 600)
    expect($(window).width()).toBe(1000)
    expect($(window).height()).toBe(600)
})