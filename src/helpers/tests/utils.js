import $ from 'jquery'

const originalJqueryWidth = $.fn.width
const originalJqueryHeight = $.fn.height

export const resizeTo = (width, height) => {
    global.window.innerWidth = width || global.window.innerWidth;
    global.window.innerHeight = height || global.window.innerHeight;

    $.fn.width = function (margin, value) {
        if ($.isWindow(this[0])) {
            return width
        }
        return originalJqueryWidth.apply(this, margin, value)
    }

    $.fn.height = function (margin, value) {
        if ($.isWindow(this[0])) {
            return height
        }
        return originalJqueryHeight.apply(this, margin, value)
    }

    global.window.dispatchEvent(new Event('resize'))
}