/* globals TARGET, SIM_ID, APP_MODE, SENTRY_RELEASE */
import $ from 'jquery'

import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { IntlProvider, addLocaleData } from 'react-intl'
import * as Sentry from '@sentry/browser'

import en from 'react-intl/locale-data/en'
import fr from 'react-intl/locale-data/fr'
import nr from 'react-intl/locale-data/nr'
import ru from 'react-intl/locale-data/ru'

import messagesEn from './translations/en'
import messagesFr from './translations/fr'
import messagesNr from './translations/nr'
import messagesRu from './translations/ru'

import './styles/index.scss'
import { store } from './store'
import App from './containers/App'

import {
  changeDebug,
  changeCrewOnly,
  updateUser,
  receiveUserPosition,
  receiveParty,
  addNearbyUsers,
  removeNearbyUser,
  changePage,
  changeTheme,
  fetchShipHeading,
  getWindowSizes,
  setOrientation,
  requestParty,
  requestFriends
} from './actions/app'
import { fetchCategories } from './actions/categories'
import { addUsers } from './actions/users'
import * as deckActions from './actions/deck'
import { SECTOR_ID_TO_DECKS, CONFIG_ID, TV_USERS } from './constants'
import { parseParams, toInt } from './helpers/misc'

addLocaleData([...en, ...ru, ...fr, ...nr])

if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'shell') {
  Sentry.init({
    dsn: 'https://7bb1b853f1134d9aabcc9e7ef82bf088@sentry.io/1384486',
    release: SENTRY_RELEASE
  })
}

const TV_DEBUG = false

if (TV_DEBUG) {
  window.console.log = msg => {
    let id = +new Date()
    document.getElementById('log-messages').innerHTML += `<div id="log-msg-${id}">${JSON.stringify(msg)}</div>`
    setTimeout(() => {
      $(`#log-msg-${id}`).hide()
    }, 5000)
  }
}

let lang = 'en'

const applyUrlParams = () => {
  let params = parseParams(document.location.hash)
  Object.keys(params).forEach(field => {
    let value = params[field]
    let f
    switch (field) {
      case 'deck':
        f = deckActions.changeDeck
        break
      case 'page':
        f = changePage
        break
      case 'debug':
        f = changeDebug
        break
      case 'crewonly':
        f = changeCrewOnly
        break
      case 'theme':
        f = changeTheme
        break
      case 'offsetX':
        f = deckActions.changeOffsetX
        break
      case 'offsetY':
        f = deckActions.changeOffsetY
        break
      case 'rotation':
        f = deckActions.changeRotation
        break
      case 'zoom':
        f = deckActions.changeZoom
        break
      case 'perspectiveRotation':
        f = deckActions.changePerspectiveRotation
        break
      case 'userId':
        f = updateUser
        value = { id: value }
        break
      case 'party':
        f = receiveParty
        value = value.split(',').map(id => ({
          id,
          hardcoded: true
        }))
        break
      case 'userPosition':
        f = receiveUserPosition
        value = value.split(',').map(toInt)
        break
      case 'lang':
        lang = value
        break
      case 'orientation':
        f = setOrientation
    }
    if (f) {
      store.dispatch(f(value))
    }
  })
  return params
}

window.addEventListener('hashchange', applyUrlParams)

const crewOnly = !!(+applyUrlParams().crewonly)

store.dispatch(getWindowSizes())
store.dispatch(setOrientation())

store.dispatch(fetchShipHeading())
store.dispatch(fetchCategories(crewOnly))

setInterval(() => {
  store.dispatch(fetchShipHeading())
}, 5000)

const configId = !Number.isNaN(toInt(applyUrlParams().configid)) ? toInt(applyUrlParams().configid) : CONFIG_ID

if (APP_MODE !== 'tv') {
  let userSocket = new window.WebSocket(`wss://dxp-wayfinder-api.simis.ai/ws/user_location?offset=-1&configId=${configId}`)
  userSocket.onmessage = e => {
    let data = JSON.parse(e.data)
    // don't forget it !!!!!!!!!
    // unable reveiveUserPosition in socket
    store.dispatch(
      receiveUserPosition({
        id: data.user_id,
        x: data.x,
        y: data.y,
        deck: SECTOR_ID_TO_DECKS[data.sectorId]
      })
    )
  }
}

let venueSocket = new window.WebSocket(`wss://dxp-wayfinder-api.simis.ai/ws/user_venue?offset=-1&configId=${configId}`)
venueSocket.onmessage = e => {
  // let data = JSON.parse(e.data)
  // store.dispatch(
  //     deckActions.receiveUserVenue({
  //         x: data.x,
  //         y: data.y,
  //         deck: SECTOR_ID_TO_DECKS[data.sectorId]
  //     })
  // )
}

const usersTimeouts = {}
window.addEventListener('proximity', e => {
  window.provideInfoToShell('debug', e.detail)
  const data = e.detail
  const users = TV_USERS.filter(u => !!data.find(d => d.uuid === u.uuid && parseInt(d.major, 10) === parseInt(u.major, 10) && parseInt(d.minor, 10) === parseInt(u.minor, 10)))
  if (users.length) {
    store.dispatch(addNearbyUsers(users))
    users.forEach((user, index) => {
      if (usersTimeouts[user.id]) {
        clearTimeout(usersTimeouts[user.id])
      }
      let f = (u) => () => store.dispatch(removeNearbyUser(u))
      usersTimeouts[user.id] = setTimeout(f(user), 10000 + index * 50)
    })
  }
})
document.addEventListener('message', (event) => {
  const data = JSON.parse(event.data)
  if (data.action === 'changePage') {
    const { page, routeData } = data.actionData
    const { saveVisited } = routeData || {}
    store.dispatch(changePage(page, saveVisited))
  }
})

if (toInt(applyUrlParams().nearbyUsers) >= 1) {
  const lengthMin = Math.min(TV_USERS.length, toInt(applyUrlParams().nearbyUsers))
  for (let i = 0; i < lengthMin; i++) {
    store.dispatch(addNearbyUsers([TV_USERS[i]]))
  }
}

// heatmap
if (APP_MODE !== 'tv') {
  let heatmapSocket = new window.WebSocket(`wss://dxp-wayfinder-api.simis.ai/ws/heatmap_location?offset=1&configId=${configId}`)

  heatmapSocket.onmessage = e => {
    let users = JSON.parse(e.data)
    if (Array.isArray(users)) {
      store.dispatch(addUsers(users))
    }
  }
}

if (TARGET === 'web') {
  const state = store.getState()
  const { accessToken, crewOnly, user: { ReservationGuestId } } = state.app

  if (ReservationGuestId && accessToken) {
    store.dispatch(requestParty(ReservationGuestId, crewOnly, accessToken))
    if (SIM_ID !== 2) {
      store.dispatch(requestFriends(ReservationGuestId, accessToken))
    }
  }

  window.logPanel = (d) => {
    $('#log-panel').appendTo(JSON.stringify(d))
  }
}

ReactDOM.render(
  <IntlProvider
    textComponent={React.Fragment}
    locale={lang}
    defaultLocale={lang}
    messages={{
      en: messagesEn,
      fr: messagesFr,
      nr: messagesNr,
      ru: messagesRu
    }[lang]}
  >
    <Provider store={store}>
      <App />
    </Provider>
  </IntlProvider>,
  document.getElementById('root')
)
