import Vertex from './Vertex'

test('Vertex.getFields', () => {
    expect(Vertex.getFields().length).toBeGreaterThan(0)
})

test('Vertex.distanceToPoint', () => {
    let vertex = new Vertex({ x: 500, y: 600 })
    expect(vertex.distanceToPoint({ x: vertex.x + 50, y: vertex.y + 50 })).toBe(Math.hypot(50, 50))
})
