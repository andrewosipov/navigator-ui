import Venue from './Venue'
import data from '../helpers/tests/data'
import { toInt } from '../helpers/misc'
import { DECK_TO_SECTORS_ID, SECTOR_ID_TO_DECKS } from '../constants'
import Vertex from './Vertex'
import Category from './Category'
import Path from './Path'

test('Venue.getFields', () => {
  expect(Venue.getFields().length).toBeGreaterThan(0)
})

test('Venue.fetch', async () => {
  let venues = await Venue.fetch()
  expect(venues.length).toBeGreaterThan(0)

  let venue = venues[0]
  expect(venue).toBeInstanceOf(Venue)
})

test('Venue.getDeckVenues', () => {
  let venues = Venue.getDeckVenues(data.venues, 6)
  venues.forEach(v => expect(toInt(SECTOR_ID_TO_DECKS[v.sectorId])).toBe(6))
})

test('Venue.hasExitPoint', () => {
  let v = new Venue({ exitPoints: [5, 6, 7] })
  expect(v.hasExitPoint(new Vertex({ id: 3 }))).toBe(false)
  expect(v.hasExitPoint(new Vertex({ id: 5 }))).toBe(true)
  expect(v.hasExitPoint(new Vertex({ id: 7 }))).toBe(true)
})

test('Venue.hasExitPointPath', () => {
  let v = new Venue({ exitPoints: [5, 6, 7] })
  expect(v.hasExitPointPath(
    new Path({
      vertexes: {
        begin: new Vertex({ id: 3 }),
        end: new Vertex({ id: 9 })
      }
    })
  )).toBe(false)
  expect(v.hasExitPointPath(
    new Path({
      vertexes: {
        begin: new Vertex({ id: 3 }),
        end: new Vertex({ id: 5 })
      }
    })
  )).toBe(true)
  expect(v.hasExitPointPath(
    new Path({
      vertexes: {
        begin: new Vertex({ id: 6 }),
        end: new Vertex({ id: 9 })
      }
    })
  )).toBe(true)
})

test('Venue.getIcon', () => {
  let v = new Venue({ categories: [{ id: Category.HEALTH }] })
  expect(typeof (v.getIcon())).toBe('object')
})

test('Venue.getDefaultCategoryId', () => {
  let v = new Venue({ categories: [{ id: Category.HEALTH }] })
  expect(v.getDefaultCategoryId()).toBe(Category.HEALTH)

  v = new Venue({ categories: [{ id: Category.EMBARKATION }, { id: Category.HEALTH }] })
  expect(v.getDefaultCategoryId()).toBe(Category.EMBARKATION)
  expect(typeof (v.getDefaultCategoryIcon())).toBe('object')
})

test('Venue.getCategoryIcons', () => {
  let v = new Venue({ categories: [{ id: Category.EMBARKATION }, { id: Category.HEALTH }] })
  expect(v.getCategoryIcons()).toHaveLength(2)
})

test('Venue.isRestroom', () => {
  let v = new Venue({ categories: [{ id: Category.HEALTH }] })
  expect(v.isRestroom()).toBe(false)

  v = new Venue({ categories: [{ id: Category.RESTROOM }] })
  expect(v.isRestroom()).toBe(true)
})

test('Venue.isEmbarkation', () => {
  let v = new Venue({ categories: [{ id: Category.HEALTH }] })
  expect(v.isEmbarkation()).toBe(false)

  v = new Venue({ categories: [{ id: Category.EMBARKATION }] })
  expect(v.isEmbarkation()).toBe(true)
})

test('Venue.isPortal', () => {
  let v = new Venue({ categories: [{ id: Category.HEALTH }] })
  expect(v.isPortal()).toBe(false)

  v = new Venue({ categories: [{ id: Category.STAIRS }] })
  expect(v.isPortal()).toBe(true)

  v = new Venue({ categories: [{ id: Category.ELEVATOR }] })
  expect(v.isPortal()).toBe(true)
})

test('Venue.isElevator', () => {
  let v = new Venue({ categories: [{ id: Category.HEALTH }] })
  expect(v.isElevator()).toBe(false)

  v = new Venue({ categories: [{ id: Category.STAIRS }] })
  expect(v.isElevator()).toBe(false)

  v = new Venue({ categories: [{ id: Category.ELEVATOR }] })
  expect(v.isElevator()).toBe(true)
})

test('Venue.isStairs', () => {
  let v = new Venue({ categories: [{ id: Category.HEALTH }] })
  expect(v.isStairs()).toBe(false)

  v = new Venue({ categories: [{ id: Category.STAIRS }] })
  expect(v.isStairs()).toBe(true)

  v = new Venue({ categories: [{ id: Category.ELEVATOR }] })
  expect(v.isStairs()).toBe(false)
})

test('Venue.isService', () => {
  let v = new Venue({ categories: [{ id: Category.HEALTH }] })
  expect(v.isService()).toBe(false)

  v = new Venue({ categories: [{ id: Category.STAIRS }] })
  expect(v.isService()).toBe(true)

  v = new Venue({ categories: [{ id: Category.ELEVATOR }] })
  expect(v.isService()).toBe(true)

  v = new Venue({ categories: [{ id: Category.EMBARKATION }] })
  expect(v.isService()).toBe(true)

  v = new Venue({ categories: [{ id: Category.RESTROOM }] })
  expect(v.isService()).toBe(true)
})

test('Venue.isCabin', () => {
  let v = new Venue({ categories: [{ id: Category.HEALTH }] })
  expect(v.isCabin()).toBe(false)

  v = new Venue({ categories: [{ id: Category.CABIN }] })
  expect(v.isCabin()).toBe(true)
})

test('Venue.limitByCategory', () => {
  let limit = 3
  let venues = Venue.limitByCategory(data.venues, DECK_TO_SECTORS_ID[6], limit)
  expect(venues.length).toBeGreaterThan(0)
  let categories = {}
  venues.forEach(v => {
    expect(toInt(SECTOR_ID_TO_DECKS[v.sectorId])).toBe(6)
    let cat = v.getDefaultCategoryId()
    if (categories[cat] === undefined) {
      categories[cat] = 0
    }

    categories[cat] += 1
  })

  Object.values(categories).forEach(count => expect(count).toBeLessThanOrEqual(limit))
})

test('Venue.search', async () => {
  let q = 'd'
  let venues = await Venue.search(q, null, false, data.user)
  expect(venues.length).toBeGreaterThan(0)
  venues.forEach(v => {
    expect(v.name.toLowerCase().includes(q)).toBe(true)
  })

  let cat = 5 // bars
  venues = await Venue.search('', cat, false, data.user)
  expect(venues.length).toBeGreaterThan(0)
  venues.forEach(v => {
    expect(v.categories.map(c => c.id)).toContain(cat)
  })
})

test('Venue.findRestroom', async () => {
  let venues = await Venue.findRestroom('M', false, false, data.user)
  expect(venues.length).toBeGreaterThan(0)
  venues.forEach(v => {
    expect(v.categories.map(c => c.id)).toContain(Category.RESTROOM)
  })
})
