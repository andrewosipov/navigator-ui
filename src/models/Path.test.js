import Path from './Path'
import Vertex from './Vertex'
import data from '../helpers/tests/data'
import { toInt } from '../helpers/misc'
import { DECK_TO_SECTORS_ID, SECTOR_ID_TO_DECKS } from '../constants'


test('Path.getFields', () => {
    expect(Path.getFields().length).toBeGreaterThan(0)
})

test('Path.fetch', async () => {
    let paths = await Path.fetch(1)
    expect(paths.length).toBeGreaterThan(0)

    let path = paths[0]
    expect(path).toBeInstanceOf(Path)
    expect(path.vertexes.begin).toBeInstanceOf(Vertex)
})

test('Path.fetchXtoVenue', async () => {
    let pathsTo = await Path.fetchXtoVenue(data.user, data.venues[5], DECK_TO_SECTORS_ID[data.user.deck], false)
    expect(pathsTo.length).toBeGreaterThan(0)
    let path = pathsTo[0]
    expect(path).toBeInstanceOf(Path)
    expect(path.vertexes.begin).toBeInstanceOf(Vertex)
    expect(path.id).toBe('from-user')
    expect(pathsTo[pathsTo.length - 1].id).not.toBe('from-user')
})

test('Path.fetchVenueToX', async () => {
    let pathsTo = await Path.fetchVenueToX(data.user, data.venues[5], DECK_TO_SECTORS_ID[data.user.deck], false)
    expect(pathsTo.length).toBeGreaterThan(0)
    let path = pathsTo[0]
    expect(path).toBeInstanceOf(Path)
    expect(path.vertexes.begin).toBeInstanceOf(Vertex)
    expect(path.id).not.toBe('to-user')
    expect(pathsTo[pathsTo.length - 1].id).toBe('to-user')
})

test('Path.fetchVenueToVenue', async () => {
    let pathsTo = await Path.fetchVenueToVenue(data.venues[3], data.venues[5], false)
    expect(pathsTo.length).toBeGreaterThan(0)
    let path = pathsTo[0]
    expect(path).toBeInstanceOf(Path)
    expect(path.vertexes.begin).toBeInstanceOf(Vertex)
    expect(path.id).not.toBe('from-user')
    expect(pathsTo[pathsTo.length - 1].id).not.toBe('to-user')
})

test('Path.fetchXtoX', async () => {
    let pathsTo = await Path.fetchXtoX(data.user.x, data.user.y, data.user.x + 200, data.user.y + 200, DECK_TO_SECTORS_ID[data.user.deck], DECK_TO_SECTORS_ID[7])
    expect(pathsTo.length).toBeGreaterThan(0)
    let path = pathsTo[0]
    expect(path).toBeInstanceOf(Path)
    expect(path.vertexes.begin).toBeInstanceOf(Vertex)
    expect(path.id).toBe('from-user')
    expect(pathsTo[pathsTo.length - 1].id).toBe('to-x')
})

test('Path.distanceFromCenterToPoint', () => {
    let d = data.path.distanceFromCenterToPoint({
        x: (data.path.vertexes.begin.x + data.path.vertexes.end.x) / 2 + 50,
        y: (data.path.vertexes.begin.y + data.path.vertexes.end.y) / 2 + 50
    })
    expect(d).toBe(Math.hypot(50, 50))
})

test('Path.findNearestPathToPoint', () => {
    let expectedPath = data.selectedPaths[3]
    let path = Path.findNearestPathToPoint(
        data.selectedPaths,
        {
            deck: SECTOR_ID_TO_DECKS[expectedPath.vertexes.begin.sectorId],
            x: expectedPath.vertexes.begin.x + 40,
            y: expectedPath.vertexes.begin.y + 40,
        }
    )
    expect(path).toEqual(expectedPath)
})

test('Path.getDeckPaths', () => {
    let deck = 6
    let paths = Path.getDeckPaths(data.selectedPaths, deck)
    paths.forEach(p => expect(toInt(SECTOR_ID_TO_DECKS[p.vertexes.begin.sectorId])).toBe(deck))
})

test('Path.getDirection', () => {
    let p = data.selectedPaths[3].getDirection()
    expect(p).toHaveProperty('distance')
    expect(p).toHaveProperty('key')
})

test('Path.getUserPathData', () => {
    let d = Path.getUserPathData(data.user, data.selectedPaths, data.venues)
    expect(d).toHaveProperty('path')
    expect(d).toHaveProperty('direction')
    expect(d).toHaveProperty('time')
    expect(d).toHaveProperty('distance')
    expect(d).toHaveProperty('needRebuildPath')
    expect(d).toHaveProperty('isArrived')
    expect(d).toHaveProperty('portalTo')
})
