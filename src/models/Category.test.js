import Category from './Category'

test('Category.getFields', () => {
  expect(Category.getFields().length).toBeGreaterThan(0)
})

test('Category model', async () => {
  let categories = await Category.fetch()
  expect(categories.length).toBeGreaterThan(0)

  let category = categories[0]
  expect(category).toBeInstanceOf(Category)

  categories.forEach(c => {
    expect(typeof (c.getIcon())).toBe('object')
    expect(typeof (c.getFullIcon())).toBe('object')
    expect(typeof (c.getIconUrl())).toBe('string')
    expect(c.toObject()).toHaveProperty('id')
    expect(c.toObject()).toHaveProperty('name')
    expect(c.toObject()).toHaveProperty('isService')
    expect(c.toObject()).toHaveProperty('venuesCount')

    if (c.id === Category.ELEVATOR) {
      expect(Category.isElevator(c.id)).toBe(true)
      expect(Category.isPortalCategoryId(c.id)).toBe(true)
      expect(Category.isStairs(c.id)).toBe(false)
      expect(c.isPortal()).toBe(true)
    } else if (c.id === Category.STAIRS) {
      expect(Category.isElevator(c.id)).toBe(false)
      expect(Category.isPortalCategoryId(c.id)).toBe(true)
      expect(Category.isStairs(c.id)).toBe(true)
      expect(c.isPortal()).toBe(true)
    } else {
      expect(Category.isPortalCategoryId(c.id)).toBe(false)
      expect(c.isPortal()).toBe(false)
      if (!c.isService) {
        expect(typeof (c.getTabIcon())).toBe('object')
      }
    }
  })
})
