export default class Model {
    constructor(values = {}) {
        Object.keys(values).forEach((k) => {
            this[k] = values[k];
        })
    }

    static getFields() {
        throw new TypeError("Must override method");
    }

    toObject() {
        let result = {};
        this.constructor.getFields().forEach(field => {
            result[field] = this[field];
        });

        return result;
    }
}