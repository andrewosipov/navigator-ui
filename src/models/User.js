import Model from './Model'
import API from '../helpers/api'

export default class User extends Model {
  static getFields () {
    return [
      'id',
      'name',
      'avatar',
      'x',
      'y',
      'sectorId',
      'venueName',
      'deck',
      'isCrew',
      'subrole',
      'inElevator',
      'cabin'
    ]
  }

  isService () {
    return false
  }

  isCabin () {
    return false
  }

  static async fetch (user = null, crewOnly = false, token) {
    let response = await API.getParty(user, crewOnly, token)
    return response.map(u => new User(u))
  }

  static getUser () {
    return API.getUser()
  }

  static async search (q, crewOnly, token, roles) {
    const users = await API.getUsers(q, crewOnly, token, roles)
    return users.map(u => new User(u))
  }

  static async getParty (reservationGuestId, crewOnly = false, accessToken) {
    let users = await API.getParty(reservationGuestId, crewOnly, accessToken)
    return users.map(u => new User(u))
  }

  static async getPreferences (reservationGuestId) {
    const preferences = await API.getUserPreferences(reservationGuestId)
    return preferences
  }
}
