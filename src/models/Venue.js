import Model from './Model'
import API from 'src/helpers/api'
import Category from './Category'
import { toInt } from 'src/helpers/misc'
import { DECK_TO_SECTORS_ID, VENUES_TAGS } from 'src/constants'
import { icons } from 'src/helpers/categorySvgSet'

let SECTOR_VENUES = {}

export default class Venue extends Model {
  static getFields () {
    return [
      'id',
      'x',
      'y',
      'name',
      'len',
      'width',
      'teleportX',
      'teleportY',
      'tags',
      'sectorId',
      'yawValue',
      'categories',
      'distance',
      'exitPoints',
      'globalVenueId',
      'representations',
      'time'
    ]
  };

  static getDeckVenues (venues, deck) {
    let sectorId = toInt(DECK_TO_SECTORS_ID[deck])
    SECTOR_VENUES[sectorId] = venues.filter(v => v.sectorId === sectorId)

    return SECTOR_VENUES[sectorId]
  }

  static async fetch (sectorId = null, q = null, categoryId = null, crewOnly = false, user = null, onlyPublic = false, ids = null) {
    let response = await API.getVenues(sectorId, q, categoryId, crewOnly, user, onlyPublic, ids)
    return response.map(({
      venue = {}, categories, distance, time, attributes, exitPoints, representations, tags
    }) => {
      return new Venue({
        ...venue,
        categories,
        distance,
        time,
        attributes,
        exitPoints,
        representations,
        tags
      })
    })
  }

  hasExitPoint (vertex) {
    return this.exitPoints.includes(vertex.id)
  }

  hasExitPointPath (path) {
    return this.hasExitPoint(path.vertexes.begin) || this.hasExitPoint(path.vertexes.end)
  }

  getIcon (crew = false) {
    return this.getDefaultCategoryIcon(crew)
  }

  getDefaultCategoryId () {
    if (this.categories.length) {
      let c = this.categories.find(c => c.isDefaultCategory)
      if (c) {
        return c.id
      } else {
        return this.categories[0].id
      }
    }
  }

  getDefaultCategoryIcon (crew = false) {
    const id = this.getDefaultCategoryId()
    return crew ? icons[id].crew : icons[id].public
  }

  getCategoryIcons () {
    return this.categories.map(c => icons[c.id].public)
  }

  isRestroom () {
    return Category.isRestroom(this.getDefaultCategoryId())
  }

  isEmbarkation () {
    return Category.isEmbarkation(this.getDefaultCategoryId())
  }

  isPortal () {
    return Category.isPortalCategoryId(this.getDefaultCategoryId())
  }

  isElevator () {
    return Category.isElevator(this.getDefaultCategoryId())
  }

  isStairs () {
    return Category.isStairs(this.getDefaultCategoryId())
  }

  isService () {
    return this.isRestroom() || this.isEmbarkation() || this.isPortal() || this.isCabin()
  }

  isCabin () {
    return Category.isCabin(this.getDefaultCategoryId())
  }

  isVerticalCabin () {
    return this.isCabin() && this.name.includes('M')
  }

  isMusterStation () {
    return !!this.tags && !!this.tags.find(tag => tag.id === VENUES_TAGS.MUSTER_STATION)
  }

  static limitByCategory (venues, sectorId, amountByCategory = 3) {
    let categoriesAmount = {}
    return venues.filter(v => {
      if (toInt(v.sectorId) !== toInt(sectorId)) {
        return false
      }

      for (let i = 0; i < v.categories.length; i++) {
        let cat = v.categories[i]
        if (categoriesAmount[cat] === undefined) {
          categoriesAmount[cat] = 0
        }
        if (categoriesAmount[cat] < amountByCategory) {
          categoriesAmount[cat] += 1
          return true
        }
      }

      return false
    })
  }

  static async search (q, categoryId = null, crewOnly = false, user = null, venues = null) {
    let ids = null
    if (venues) {
      let searchQuery = q.toLowerCase()
      ids = venues.filter(v =>
        v.name.toLowerCase().includes(searchQuery) ||
        v.categories.some(
          c => c.name.toLowerCase().includes(searchQuery)
        )
      ).map(v => v.id)
      if (!ids.length) {
        return []
      }
    }
    let result = await Venue.fetch(null, !venues ? q : null, categoryId, crewOnly, user, false, ids)
    if (venues) {
      result = result.map(v => {
        let venue = venues.find(el => el.id === v.id)
        if (!venue) {
          return v
        }

        return new Venue({
          ...v.toObject(),
          ...venue.toObject(),
          time: v.time
        })
      })
      console.log(result)
    }
    return result.filter(v => !v.isPortal() && v.time > 0)
  }

  static async findRestroom (gender, invalid, crewOnly = false, user = null) {
    let venues = await this.search('', Category.RESTROOM, crewOnly, user)
    let attrId = gender === 'M' ? 22 : 21
    return venues.filter(v => {
      let attr = v.attributes.find(attr => attr.id === attrId)
      let result = attr && attr.value
      if (invalid) {
        attr = v.attributes.find(attr => attr.id === 20)
        result = result && attr && attr.value
      }
      return result
    }).sort((a, b) => a.time - b.time)
  }

  getThumbnail () {
    if (this.thumbnails && this.thumbnails.length) {
      return this.thumbnails[0]
    }

    return `/public/images/venues/${this.id}_1.jpg`
  }
}
