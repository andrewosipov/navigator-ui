import Model from './Model'
import API from '../helpers/api'


export default class Role extends Model {
    static async fetch(accessToken) {
        let response = await API.getRoles(accessToken);
        return response.map(
            v => new Role(Object.assign(v))
        );
    }
}