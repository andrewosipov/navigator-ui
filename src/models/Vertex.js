import Model from './Model';
import API from '../helpers/api';


export default class Vertex extends Model {
    static getFields() {
        return [
            'id',
            'x',
            'y',
            'sectorId',
            'name',
            'displayName',
            'isPortal',
            'crewOnly'
        ];
    };

    distanceToPoint({ x, y }) {
        return Math.hypot(
            this.x - x,
            this.y - y
        )
    }
}