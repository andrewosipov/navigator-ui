import Model from './Model'
import Vertex from './Vertex'
import API from '../helpers/api'
import { toInt } from '../helpers/misc'
import { DECK_TO_SECTORS_ID, SECTOR_ID_TO_DECKS } from '../constants'

export default class Path extends Model {
  static getFields () {
    return [
      'id',
      'distance',
      'direction',
      'vertexes'
    ]
  };

  static async fetch (sectorId) {
    let response = await API.getPaths(sectorId)
    return response.path.filter(p =>
      p.vertexes.begin.sectorId === sectorId && p.vertexes.end.sectorId === sectorId
    ).map(p => Path.responseObjectToPath(p, response.directions))
  }

  static async fetchXtoUser ({ x, y }, selectedUser, startSectorId, endSectorId, crewOnly, avoidStairs, insideOnly) {
    let response = await API.pathXToX(toInt(x), toInt(y), toInt(selectedUser.x), toInt(selectedUser.y), startSectorId, endSectorId, crewOnly, avoidStairs, insideOnly)
    let paths = response.path.map(p => Path.responseObjectToPath(p, response.directions))
    if (paths.length) {
      let distance = Math.hypot(x - paths[0].vertexes.begin.x, y - paths[0].vertexes.begin.y)
      paths = [new Path({
        id: 'from-user',
        distance,
        time: distance / 1300,
        vertexes: {
          begin: new Vertex({ x, y, sectorId: startSectorId }),
          end: paths[0].vertexes.begin
        }
      })].concat(paths)
    }

    return paths
  }

  static async fetchXtoVenue ({ x, y }, venue, startSectorId, crewOnly, avoidStairs, insideOnly) {
    let response = await API.pathXToVenue({ x: toInt(x), y: toInt(y) }, venue.id, startSectorId, crewOnly, avoidStairs, insideOnly)
    let paths = response.path.map(p => Path.responseObjectToPath(p, response.directions))
    if (paths.length) {
      let distance = Math.hypot(x - paths[0].vertexes.begin.x, y - paths[0].vertexes.begin.y)
      paths = [new Path({
        id: 'from-user',
        distance,
        time: distance / 1300,
        vertexes: {
          begin: new Vertex({ x, y, sectorId: startSectorId }),
          end: paths[0].vertexes.begin
        }
      })].concat(paths)
    }

    return paths
  }

  static async fetchVenueToX ({ x, y }, venue, endSectorId, crewOnly) {
    let response = await API.pathVenueToX({ x: toInt(x), y: toInt(y) }, venue.id, endSectorId, crewOnly)
    let paths = response.path.map(p => Path.responseObjectToPath(p, response.directions))
    if (paths.length) {
      let distance = Math.hypot(x - paths[0].vertexes.begin.x, y - paths[0].vertexes.begin.y)
      paths = paths.concat(new Path({
        id: 'to-user',
        distance,
        time: distance / 1300,
        vertexes: {
          begin: paths[paths.length - 1].vertexes.end,
          end: new Vertex({ x, y, sectorId: endSectorId })
        }
      }))
    }

    return paths
  }

  static async fetchVenueToVenue (venue1, venue2, crewOnly, avoidStairs, insideOnly) {
    let response = await API.pathVenueToVenue(venue1.id, venue2.id, crewOnly, avoidStairs, insideOnly)
    let paths = response.path.map(p => Path.responseObjectToPath(p, response.directions))
    return paths
  }

  static async fetchXtoX (x1, y1, x2, y2, startSectorId, endSectorId, crewOnly) {
    let response = await API.pathXToX(
      toInt(x1),
      toInt(y1),
      toInt(x2),
      toInt(y2),
      startSectorId,
      endSectorId,
      crewOnly
    )
    let paths = response.path.map(p => Path.responseObjectToPath(p, response.directions))
    if (paths.length) {
      paths = [new Path({
        id: 'from-user',
        vertexes: {
          begin: new Vertex({ x: x1, y: y1, sectorId: startSectorId }),
          end: paths[0].vertexes.begin
        }
      })].concat(
        paths
      ).concat(
        new Path({
          id: 'to-x',
          vertexes: {
            begin: paths[paths.length - 1].vertexes.end,
            end: new Vertex({ x: x2, y: y2, sectorId: endSectorId })
          }
        })
      )
    } else {
      paths = [new Path({
        id: 'x-to-x',
        vertexes: {
          begin: new Vertex({ x: x1, y: y1, sectorId: startSectorId }),
          end: new Vertex({ x: x2, y: y2, sectorId: endSectorId })
        }
      })]
    }

    return paths
  }

  static responseObjectToPath (p, directions) {
    let path = new Path({
      ...p,
      vertexes: {
        begin: new Vertex(p.vertexes.begin),
        end: new Vertex(p.vertexes.end)
      }
    })
    if (directions) {
      path.vertexes.begin.direction = directions.find(d => d.vertexId === p.vertexes.begin.id)
      path.vertexes.end.direction = directions.find(d => d.vertexId === p.vertexes.end.id)
    }
    return path
  }

  distanceFromCenterToPoint ({ x, y }) {
    return Math.hypot(
      this.vertexes.begin.x / 2 + this.vertexes.end.x / 2 - x,
      this.vertexes.begin.y / 2 + this.vertexes.end.y / 2 - y
    )
  }

  static findNearestPathToPoint (paths, point) {
    paths = paths.filter(p => p.vertexes.end.sectorId === toInt(DECK_TO_SECTORS_ID[point.deck]))
    return paths.sort((a, b) => a.distanceFromCenterToPoint(point) - b.distanceFromCenterToPoint(point))[0]
  }

  static getDeckPaths (paths, deck) {
    let sectorId = DECK_TO_SECTORS_ID[deck]
    return paths.filter(p => p.vertexes.begin.sectorId === sectorId)
  }

  getDirection () {
    let direction = { ...(this.vertexes.end.direction || this.vertexes.begin.direction) }
    direction.key = direction.text && direction.text.replace(' ', '-')
    direction.distance = this.distance
    return direction
  }

  static getUserPathData (user, paths, venues) {
    let fullDistance = paths.reduce((acc, v) => acc + v.distance || 0, 0)
    let fullTime = paths.reduce((acc, v) => acc + v.time || 0, 0)
    let deckPaths = Path.getDeckPaths(paths, user.deck)
    let path = Path.findNearestPathToPoint(deckPaths, user)

    if (!path) {
      return {
        needRebuildPath: true
      }
    }

    let pathIndex = paths.indexOf(path)
    let distanceToEndVertex = Math.hypot(
      path.vertexes.end.x - user.x,
      path.vertexes.end.y - user.y
    )
    let direction = path.getDirection()
    direction.distance = distanceToEndVertex

    let portalTo = null
    let nextPath = paths[pathIndex + 1]
    if (path.vertexes.begin.sectorId !== path.vertexes.end.sectorId) {
      portalTo = path.vertexes.end
    } else if (nextPath) {
      if (nextPath.vertexes.end.sectorId !== path.vertexes.begin.sectorId && nextPath.vertexes.begin.sectorId === path.vertexes.begin.sectorId) {
        portalTo = nextPath.vertexes.end
      }
    }

    let nextPaths = paths.slice(pathIndex + 1)
    if (portalTo && distanceToEndVertex < 200) {
      let portalDirectionTop = toInt(SECTOR_ID_TO_DECKS[portalTo.sectorId]) > toInt(user.deck)
      let venue = venues.find(
        v => v.hasExitPointPath(path) && toInt(SECTOR_ID_TO_DECKS[v.sectorId]) === toInt(user.deck)
      )
      if (nextPaths.length) {
        let nextNonPortalIndex = nextPaths.findIndex(p => p.vertexes.begin.sectorId === p.vertexes.end.sectorId)
        if (nextNonPortalIndex > -1) {
          portalTo = nextPaths[nextNonPortalIndex].vertexes.begin
        }
      }
      direction = {
        key: `${(venue && venue.isStairs()) ? 'stairs' : 'elevator'}-${portalDirectionTop ? 'up' : 'down'}`,
        deck: SECTOR_ID_TO_DECKS[portalTo.sectorId],
        portalDirection: portalDirectionTop ? 'Up' : 'Down'
      }
    } else if (portalTo) {
      direction.key = 'straight'
    } else if (direction.key === 'straight') {
      let nearestNonStraightPath = nextPaths.find((p) =>
        p.vertexes.begin.direction.text !== 'straight' || p.vertexes.end.sectorId !== path.vertexes.begin.sectorId
      )
      let straightPaths = paths.slice(pathIndex + 1, nearestNonStraightPath ? paths.indexOf(nearestNonStraightPath) : paths.length)
      direction.distance += straightPaths.reduce((acc, v) => acc + v.distance || 0, 0)
    }
    let passedPaths = paths.slice(0, pathIndex)
    if (distanceToEndVertex < 200 && !paths[pathIndex + 1]) {
      passedPaths.push(path)
    }
    let distance = fullDistance - passedPaths.reduce((acc, v) => acc + v.distance || 0, 0)

    let distanceToMiddleOfPath = path.distanceFromCenterToPoint(user)
    let distanceBetweenVertexes = path.vertexes.begin.distanceToPoint(path.vertexes.end)
    let needRebuildPath = distanceToMiddleOfPath > distanceBetweenVertexes / 2 + 300
    return {
      path,
      pathIndex,
      direction,
      time: fullTime - passedPaths.reduce((acc, v) => acc + v.time || 0, 0),
      distance,
      needRebuildPath,
      isArrived: distance < 150,
      portalTo
    }
  }
}
