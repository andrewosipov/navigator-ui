import Model from './Model'
import { get } from 'lodash'
import API from 'src/helpers/api'
import { icons, tabs, full } from 'src/helpers/categorySvgSet'

export default class Category extends Model {
  static CABIN = 9
  static EMBARKATION = 3
  static RESTROOM = 11
  static ELEVATOR = 1
  static STAIRS = 2
  static HEALTH = 14
  static MUSTER = 24

  static getFields () {
    return [
      'id',
      'name',
      'iconUrl',
      'venuesCount',
      'isService'
    ]
  }

  static async fetch (crewOnly = false) {
    let response = await API.getCategories(crewOnly)
    return response.map(
      v => new Category(Object.assign(v.category, { venuesCount: v.venuesCount, crewVenuesCount: v.crewVenuesCount, sailorVenuesCount: v.sailorVenuesCount }))
    )
  }

  getIconUrl (crewOnly = false) {
    const isCrew = crewOnly ? '-crew' : ''
    return this.iconUrl || `public/images/venue-categories/full/category-${this.id}${isCrew}.svg`
  }

  isPortal () {
    return Category.isPortalCategoryId(this.id)
  }

  static isRestroom (categoryId) {
    return categoryId === Category.RESTROOM
  }

  static isEmbarkation (categoryId) {
    return categoryId === Category.EMBARKATION
  }

  static isCabin (categoryId) {
    return categoryId === Category.CABIN
  }

  static isElevator (categoryId) {
    return categoryId === Category.ELEVATOR
  }

  static isStairs (categoryId) {
    return categoryId === Category.STAIRS
  }

  static isPortalCategoryId (categoryId) {
    return [
      Category.ELEVATOR,
      Category.STAIRS
    ].includes(categoryId)
  }

  getIcon (crewOnly = false, param = null) {
    switch (param) {
      case 'tv':
        return icons[this.id].tv
      case 'tv_venue':
        return icons[this.id].tv_venue
    }
    return crewOnly ? get(icons, [this.id, 'crew']) : get(icons, [this.id, 'public'])
  }

  getFullIcon (crewOnly = false) {
    return crewOnly ? get(full, [this.id, 'crew']) : get(full, [this.id, 'public'])
  }

  getTabIcon (crewOnly = false) {
    return crewOnly ? get(tabs, [this.id, 'crew']) : get(tabs, [this.id, 'public'])
  }
}
