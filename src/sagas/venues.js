/* globals SIM_ID */
import { put, takeEvery, call, select, fork } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import { REQUEST_VENUES, receiveVenues, receiveCMSVenues } from 'src/actions/venues'
import { CHANGE_DECK, selectInitialVenue } from 'src/actions/deck'
import { DECK_TO_SECTORS_ID } from 'src/constants'
import API from 'src/helpers/api'
import Venue from 'src/models/Venue'
import Category from 'src/models/Category'

const isBliss = SIM_ID === 2

function * fetchVenues ({ user, onlyPublic, sectorId, categoryId }) {
  const { crewOnly, venueId, globalVenueId } = yield select(state => state.app)
  const venues = yield call(Venue.fetch, sectorId, null, categoryId, crewOnly, user, onlyPublic)
  yield put(receiveVenues(venues))
  yield put(selectInitialVenue(venues, venueId, globalVenueId))
}

function * fetchCMSVenues () {
  const cmsVenues = yield call(API.getCMSVenues)
  yield put(receiveCMSVenues(cmsVenues))
}

function * fetchCurrentVenues () {
  const { app: { user }, deck: { deck } } = yield select(state => state)

  yield call(fetchVenues, { user, sectorId: DECK_TO_SECTORS_ID[deck] })
}

function * updatingCurrentVenues () {
  while (true) {
    yield delay(60000)
    yield fork(fetchCurrentVenues)
  }
}

function * updatingCMSVenues () {
  while (true) {
    yield delay(300000)
    yield fork(fetchCMSVenues)
  }
}

function * initialVenuesFetch () {
  const { user } = yield select(state => state.app)
  // at first we fetch only public venues (it's really small amount)
  if (isBliss) {
    yield fork(fetchCurrentVenues)
  } else {
    yield fork(fetchVenues, { onlyPublic: true })
  }
  // then fetch mustering stations (it's a service category object,
  // so it will not be returned in previous request)
  yield fork(fetchVenues, { user, categoryId: Category.MUSTER })
  // then we fetch all of them
  yield fork(fetchVenues, { onlyPublic: false })
}

export default function * venues () {
  yield fork(initialVenuesFetch)

  if (isBliss) {
    yield fork(updatingCurrentVenues)
    yield fork(updatingCMSVenues)
    yield takeEvery(CHANGE_DECK, fetchCurrentVenues)
    yield fork(fetchCMSVenues)
  }

  yield takeEvery(REQUEST_VENUES, fetchVenues)
}
