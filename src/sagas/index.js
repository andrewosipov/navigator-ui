import { all } from 'redux-saga/effects'

import app from './app'
import venues from './venues'

export default function * rootSaga () {
  yield all([
    app(),
    venues()
  ])
}
