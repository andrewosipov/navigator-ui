/* globals SIM_ID */
import { put, call, select, fork, takeEvery } from 'redux-saga/effects'
import { get } from 'lodash'
import User from 'src/models/User'
import { PREFERENCES_VALUES } from 'src/constants'
import { FETCH_USER_PREFERENCES, setUserPreferences } from 'src/actions/app'

const isBliss = SIM_ID === 2

function * userPreferencesSaga () { // TODO: move user login here
  const { user } = yield select(state => state.app)
  const reservationGuestId = get(user, 'ReservationGuestId')

  if (reservationGuestId) {
    const preferences = yield call(User.getPreferences, reservationGuestId)
    const hasStairs = !!preferences.find(pref => get(pref, 'preferenceValueId') === PREFERENCES_VALUES.STAIR)
    const hasElevators = !!preferences.find(pref => get(pref, 'preferenceValueId') === PREFERENCES_VALUES.ELEVATOR)
    const hasImperial = !!preferences.find(pref => get(pref, 'preferenceValueId') === PREFERENCES_VALUES.IMPERIAL)

    yield put(setUserPreferences({
      avoidStairs: hasElevators && !hasStairs,
      useImperialSystem: hasImperial
    }))
  }
}

export default function * app () {
  if (isBliss) {
    yield fork(userPreferencesSaga)
    yield takeEvery(FETCH_USER_PREFERENCES, userPreferencesSaga)
  }
}
