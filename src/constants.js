/* globals DXP_ENVIRONMENT, SIM_ID, GLOBAL_DEFAULT_THEME, APP_MODE */
import { swap } from './helpers/collections'
import User from 'src/models/User'
import { randomChoice } from 'src/helpers/collections'

const SIM_WIDTH = {
  1: 27864,
  2: 34000,
  1415: 27864
}
const SIM_HEIGHT = {
  1: 4592,
  2: 4592,
  1415: 4592
}
const SIM_DECK_TO_SECTORS = {
  1: {
    1: 7,
    2: 2,
    3: 3,
    4: 4,
    5: 5,
    6: 1,
    7: 15,
    8: 16,
    9: 17,
    10: 18,
    11: 19,
    12: 20,
    13: 21,
    14: 22,
    15: 106,
    16: 23,
    17: 24
  },
  2: {
    1: 399,
    2: 398,
    3: 397,
    4: 396,
    5: 30,
    6: 28,
    7: 29,
    8: 382,
    9: 383,
    10: 384,
    11: 385,
    12: 387,
    13: 388,
    14: 389,
    15: 390,
    16: 391,
    17: 386,
    18: 392,
    19: 393,
    20: 394
  },
  1415: {
    1: 1292,
    2: 1301,
    3: 1289,
    4: 1295,
    5: 1297,
    6: 1302,
    7: 1294,
    8: 1293,
    9: 1304,
    10: 1291,
    11: 1305,
    12: 1300,
    13: 1306,
    14: 1303,
    15: 1296,
    16: 1298,
    17: 1299,
    18: 1290
  }
}

export const CONFIG_ID = {
  1: 1,
  2: 2,
  1415: 4340
}[SIM_ID]

export const DEFAULT_THEME = (GLOBAL_DEFAULT_THEME) || ((APP_MODE === 'tv') ? 'dark' : 'light')

export const WIDTH = SIM_WIDTH[SIM_ID]
export const HEIGHT = SIM_HEIGHT[SIM_ID]

export const WIDTH_TV = 27864
export const HEIGHT_TV = 7040

export const ORIENTATION_LANDSCAPE = 'L'
export const ORIENTATION_PORTRAIT = 'P'

export const PATH_STROKE_WIDTH = 90
export const TEXT_SIZE = 60

let dxpGuestUrl
let dxpMediaUrl
let dxpSocialUrl
let dxpCMSUrl
if (SIM_ID === 1 || SIM_ID === 1415) {
  if (DXP_ENVIRONMENT === 'DEV') {
    dxpGuestUrl = 'https://vvshore-dev.d-aws-vxp.com/svc'
  } else {
    dxpGuestUrl = 'https://dc-sagar.dxp-decurtis.com'
  }
} else if (SIM_ID === 2) {
  if (DXP_ENVIRONMENT === 'QA1') {
    dxpGuestUrl = 'https://dxpguestservice-dxp-ship-apps.ncl-qa1.dxp-decurtis.com'
    dxpMediaUrl = 'https://dxpmultimediastorageservice-dxp-ship-apps.ncl-qa1.dxp-decurtis.com'
    dxpSocialUrl = 'https://dxpsocialnetworkingservice-dxp-ship-apps.ncl-qa1.dxp-decurtis.com'
    dxpCMSUrl = 'https://dxpcontentmanagementservice-dxp-ship-apps.ncl-qa1.dxp-decurtis.com'
  } else if (DXP_ENVIRONMENT === 'QA2') {
    dxpGuestUrl = 'https://dxpguestservice-location-solution.ncl-qa2-ship.dxp-decurtis.com'
    dxpMediaUrl = 'https://dxpmultimediastorageservice-location-solution.ncl-qa2-ship.dxp-decurtis.com'
    dxpSocialUrl = 'https://dxpsocialnetworkingservice-dxp-ship-apps.ncl-qa1.dxp-decurtis.com'
    dxpCMSUrl = 'https://dxpcontentmanagementservice-location-solution.ncl-qa2-ship.dxp-decurtis.com'
  } else if (DXP_ENVIRONMENT === 'QA') {
    dxpCMSUrl = 'http://dxpcontentmanagementservice-dxp-location-services-qa.app.ocqaship.discovery.ncl.com'
  } else { // DEV
    dxpGuestUrl = 'https://dxpguestservice-dxp-dev-apps.ncl-dev.dxp-decurtis.com'
    dxpMediaUrl = 'https://dxpmultimediastorageservice-dxp-dev-apps.ncl-dev.dxp-decurtis.com'
    dxpSocialUrl = 'https://dxpsocialnetworkingservice-dxp-dev-apps.ncl-dev.dxp-decurtis.com'
    dxpCMSUrl = 'https://dxpcontentmanagementservice-dxp-dev-apps.ncl-dev.dxp-decurtis.com'
  }
}

if (!dxpMediaUrl) {
  dxpMediaUrl = dxpGuestUrl
}

if (!dxpSocialUrl) {
  dxpSocialUrl = dxpGuestUrl
}

export const API_URL = 'https://dev-api-location.simis.ai'
export const DXP_URL = dxpGuestUrl
export const DXP_MEDIA_URL = dxpMediaUrl
export const DXP_SOCIAL_URL = dxpSocialUrl
export const DXP_CMS_URL = dxpCMSUrl

/* eslint-disable */
export const DECK_TO_SECTORS_ID = SIM_DECK_TO_SECTORS[SIM_ID]
/* eslint-enable */

export const SECTOR_ID_TO_DECKS = swap(DECK_TO_SECTORS_ID)

export const COLORS = {
  vertex: 'blue',
  path: '#cecece',
  animatedPath: '#4285f4',
  sensor: 'rgb(152, 221, 170)'
}

export const ICON_COLORS = {
  4: ['#FCE38A', '#F38181'],
  5: ['#FF7676', '#F54EA2'],
  6: ['#00ADFF', '#3A6FDE'],
  7: ['#17EAD9', '#6078EA'],
  8: ['#C79CCB', '#6127AE'],
  10: ['#E8E878', '#A58D3D'],
  12: ['#DD0AF7', '#6A195E'],
  13: ['#8AFCA5', '#81AEF3'],
  14: ['#43E695', '#3BB2B8'],
  15: ['#FF9A65', '#923C19'],
  16: ['#CB52EF', '#923A58'],
  17: ['#5791F4', '#4C35BC'],
  18: ['#FF6565', '#8A2323'],
  19: ['#70E643', '#A8B83B']
}

export const SHELL_CONFIG = {
  devicetype: 'mobile',
  isShell: true,
  moduleType: 'WAYFINDING',
  code: 'WAI'
}

export const DECK_SHIFT_X = 31
// export const DECK_SHIFT_X = 0
export const DECK_START = 5

export const VENUE_ICON_HEIGHT = 223
export const VENUE_ROUNDED_ICON_WIDTH = 205
export const VENUE_RECT_ICON_WIDTH = 195
export const VENUE_CIRCLE_ICON_WIDTH = 240

export const DEFAULT_PERSPECTIVE_ROTATION = 68
export const WIDE_PERSPECTIVE_ROTATION = 35

export const NORMAL_MAP_MODE = 'normal'
export const LOST_KIDS_MAP_MODE = 'lostkids'

export const HEATMAP_MAP_MODE = 'heatmap'
export const OCCUPANCY_MAP_SUB_MODE = 'occupancy'
export const LABELS_MAP_SUB_MODE = 'labels'
export const SAILORS_MAP_SUB_MODE = 'sailors'
export const CREW_MAP_SUB_MODE = 'crew'

export const SHORT_USERS_COUNT = 3
export const ALL_USERS_COUNT = 9
export const ICONS_USERS_NAV_MODE = 'icons_users_nav'
export const ALL_USERS_NAV_MODE = 'all_users_nav'
export const LOGIN_USERS_NAV_MODE = 'login_users_nav'
export const AUTHORIZED_USER = 'authorized_user'

export const TV_PAGE_VENUE = 'venues'
export const TV_PAGE_EVENTS = 'events'
export const TV_PAGE_PERSONAL = 'personal'

export const VERTEX_TAGS = {
  crew_only: 1,
  outside: 4,
  is_stairs: 6
}

export const VENUES_TAGS = {
  MUSTER_STATION: 8
}

export const THEMES = {
  DARK: 'dark',
  LIGHT: 'light',
  BLISS: 'bliss'
}

export const CONNECTION_TYPES = {
  WIFI: 'wifi',
  CELLULAR: 'cellular',
  UNKNOWN: 'unknown',
  NONE: 'none'
}

export const PAGES = {
  POSITION: 'position',
  CATALOG: 'catalog',
  NAVIGATION: 'navigation',
  NAVIGATION_OVERVIEW: 'navigation-overview',
  SEARCH: 'search',
  TV: 'tv',
  HOME: 'home'
}

const TV_USERS_WITH_RANDOM_POSITIONS = [
  new User({ id: 1001, reservationId: 1, uuid: '0ad9a315-b6ba-46be-aa0d-ede0eda9560f', pincode: '1234', name: 'Elena', iconUrl: '/public/images/contacts/elena_matvienko.jpg', navTip: 'left', isCrew: false, subrole: undefined, inElevator: false, cabin: '9138A' }),
  new User({ id: 1002, reservationId: 1, uuid: '9eceb454-6ccb-4143-afeb-e978133fb844', pincode: '1234', name: 'Max', iconUrl: '/public/images/contacts/max_matvienko.jpg', navTip: 'left', isCrew: false, subrole: undefined, inElevator: false, cabin: '9138A' }),
  new User({ id: 1004, reservationId: 1, uuid: 'e1f54e02-1e23-44e0-9c3d-512eb56adec9', major: '100', minor: '100', pincode: '1234', name: 'Dave', iconUrl: '/public/images/contacts/dave.jpeg', navTip: 'left', isCrew: false, subrole: undefined, inElevator: false, cabin: '9138A' }),
  new User({ id: 1004, reservationId: 55464, uuid: '633377c4-7265-6c65-7373-000000000000', major: '1001', minor: '0001', pincode: '1234', name: 'David', iconUrl: '/public/images/contacts/dave.jpeg', navTip: 'left', isCrew: false, subrole: undefined, inElevator: false, cabin: '9138A' }),
  new User({ id: 1005, reservationId: 55464, uuid: '633377c4-7265-6c65-7373-000000000000', major: '1001', minor: '0002', pincode: '1234', name: 'Derek', iconUrl: '/public/images/contacts/derek.jpeg', navTip: 'left', isCrew: false, subrole: undefined, inElevator: false, cabin: '9138A' }),
  new User({ id: 1006, reservationId: 55464, uuid: '633377c4-7265-6c65-7373-000000000000', major: '1001', minor: '0003', pincode: '1234', name: 'Jim', iconUrl: '/public/images/contacts/jim.jpeg', navTip: 'left', isCrew: false, subrole: undefined, inElevator: false, cabin: '9138A' }),
  new User({ id: 1007, reservationId: 55464, uuid: '633377c4-7265-6c65-7373-000000000000', major: '1001', minor: '0004', pincode: '1234', name: 'Keri', iconUrl: '/public/images/contacts/keri.png', navTip: 'left', isCrew: false, subrole: undefined, inElevator: false, cabin: '9138A' }),
  new User({ id: 1008, reservationId: 55465, uuid: '633377c4-7265-6c65-7373-000000000000', major: '1001', minor: '0005', pincode: '1234', name: 'Mitch', iconUrl: '/public/images/contacts/mitch.jpeg', navTip: 'left', isCrew: false, subrole: undefined, inElevator: false, cabin: '9138A' }),
  new User({ id: 1009, reservationId: 55465, uuid: '633377c4-7265-6c65-7373-000000000000', major: '1001', minor: '0006', pincode: '1234', name: 'Ryan', iconUrl: '/public/images/contacts/ryan.jpeg', navTip: 'left', isCrew: false, subrole: undefined, inElevator: false, cabin: '9138A' }),
  new User({ id: 1010, reservationId: 55465, uuid: '633377c4-7265-6c65-7373-000000000000', major: '1002', minor: '0001', pincode: '1234', name: 'Ghost', iconUrl: '/public/images/contacts/ghost.png', navTip: 'left', isCrew: false, subrole: undefined, inElevator: false, cabin: '9138A' }),
  new User({ id: 1011, reservationId: 55466, uuid: '633377c4-7265-6c65-7373-000000000000', major: '1002', minor: '0002', pincode: '1234', name: 'Ringo', iconUrl: '/public/images/contacts/ringo.jpeg', navTip: 'left', isCrew: false, subrole: undefined, inElevator: false, cabin: '9138A' }),
  new User({ id: 1012, reservationId: 55466, uuid: '633377c4-7265-6c65-7373-000000000000', major: '1002', minor: '0003', pincode: '1234', name: 'John', iconUrl: '/public/images/contacts/john.jpeg', navTip: 'left', isCrew: false, subrole: undefined, inElevator: false, cabin: '9138A' }),
  new User({ id: 1013, reservationId: 55466, uuid: '633377c4-7265-6c65-7373-000000000000', major: '1002', minor: '0004', pincode: '1234', name: 'Paul', iconUrl: '/public/images/contacts/paul.jpeg', navTip: 'left', isCrew: false, subrole: undefined, inElevator: false, cabin: '9138A' }),
  new User({ id: 1014, reservationId: 55466, uuid: '633377c4-7265-6c65-7373-000000000000', major: '1002', minor: '0005', pincode: '1234', name: 'George', iconUrl: '/public/images/contacts/george.jpeg', navTip: 'left', isCrew: false, subrole: undefined, inElevator: false, cabin: '9138A' })
]

export const TV_USERS = [

  ...TV_USERS_WITH_RANDOM_POSITIONS.map(f => {
    f.deck = randomChoice([5, 6, 7, 8, 9, 10, 15, 16])
    f.x = randomChoice([7406, 22770])
    f.y = randomChoice([2322, 2407, 1175])
    f.sectorId = DECK_TO_SECTORS_ID[f.deck]
    f.subrole = ''
    f.venueName = randomChoice(['Lido Restaurant', 'AC Station Deck 6-2', 'Athletic Club Area'])
    return f
  })
]

export const PREFERENCES_VALUES = {
  ELEVATOR: '832b1d5d-836b-423b-8916-b8dd242c4dd7',
  STAIR: '1b94cebd-0b2a-4161-a0c3-b65d12a909ee',
  METRIC: 'f17321e5-f9ea-4be8-98a4-ca21e3092402',
  IMPERIAL: 'd0dab07f-3fb8-46ea-8cd8-965c5d7d3271'
}
