import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { THEMES } from 'src/constants'
import DeckButton from 'src/components/DeckButton'

import questionSvg from 'public/images/bliss/question.svg'
import recenterSvg from 'public/images/recenter.svg'
import zoomMinusSvg from 'public/images/zoom-minus.svg'
import zoomPlusSvg from 'public/images/zoom-plus.svg'
import decksSvg from 'public/images/decks-icon.svg'

import './index.scss'

export default class DeckButtons extends PureComponent {
  static propTypes = {
    theme: PropTypes.string,
    stickToUser: PropTypes.bool,
    showOverviewButton: PropTypes.bool,
    showDeckButton: PropTypes.bool,
    showInfoButton: PropTypes.bool,
    zoom: PropTypes.number,
    isNavigation: PropTypes.bool,
    centerToUser: PropTypes.func,
    toggleOverview: PropTypes.func,
    toggleSelectDeck: PropTypes.func,
    onInfoClick: PropTypes.func
  }

  static defaultProps = {
    theme: THEMES.DARK,
    stickToUser: false,
    showOverviewButton: false,
    showDeckButton: false,
    showInfoButton: false,
    zoom: 1,
    isNavigation: false,
    centerToUser: () => {},
    toggleOverview: () => {},
    toggleSelectDeck: () => {},
    onInfoClick: () => {}
  }

  render () {
    const {
      theme,
      stickToUser,
      showOverviewButton,
      showDeckButton,
      showInfoButton,
      zoom,
      isNavigation,
      centerToUser,
      toggleOverview,
      toggleSelectDeck,
      onInfoClick
    } = this.props
    return (
      <div className={`deck-buttons deck-buttons--theme-${theme}`}>
        {showDeckButton &&
          <DeckButton
            theme={theme}
            className="deck-buttons__btn deck-buttons__btn--select"
            onClick={toggleSelectDeck}
          >
            {decksSvg}
          </DeckButton>
        }

        {showInfoButton &&
          <DeckButton
            theme={theme}
            className="deck-buttons__btn deck-buttons__btn--info"
            onClick={() => {
              window.provideInfoToShell('wayfinder.INFO_BUTTON_CLICKED', {})
              return onInfoClick()
            }}
          >
            {questionSvg}
          </DeckButton>
        }

        {showOverviewButton &&
          <DeckButton
            theme={theme}
            className="deck-buttons__btn deck-buttons__btn--overview"
            onClick={toggleOverview}
          >
            {zoom !== 1 ? zoomMinusSvg : zoomPlusSvg}
          </DeckButton>
        }

        {!stickToUser &&
          <DeckButton
            theme={theme}
            className={cn(
              'deck-buttons__btn',
              {
                'deck-buttons__btn--resenter': !isNavigation,
                'deck-buttons__btn--navigation': isNavigation
              }
            )}
            onClick={centerToUser}
          >
            {recenterSvg}
          </DeckButton>
        }
      </div>
    )
  }
}
