import React, { Component } from 'react'
import Path from '../elements/Path'


export default class Paths extends Component {
    static defaultProps = {
        instances: []
    }

    shouldComponentUpdate(nextProps) {
        return this.props.instances !== nextProps.instances
    }

    render() {
        return (
            <g className="paths-container">
                {this.props.instances.map((path, i) => <Path
                    key={path.id}
                    instance={path}
                    displayVertexes={true}
                />)}
            </g>
        )
    }
}