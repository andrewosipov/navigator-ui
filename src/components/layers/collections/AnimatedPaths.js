import React, { Component } from 'react'
import Helmet from 'react-helmet'
import { toInt, isSafari } from '../../../helpers/misc'
import { COLORS, HEIGHT, SECTOR_ID_TO_DECKS } from '../../../constants'

export default class AnimatedPaths extends Component {
  static defaultProps = {
    instances: [],
    animationTime: 1.5,
    animation: false
  }

  shouldComponentUpdate (nextProps, nextState) {
    let should = (
      this.state !== nextState ||
      this.props.instances !== nextProps.instances ||
      this.props.deck !== nextProps.deck
    )
    if (isSafari()) {
      should = should || this.props.zoom !== nextProps.zoom
    }
    return should
  }

  componentDidUpdate (prevProps) {
    if (isSafari() && this.props.zoom !== prevProps.zoom) {
      this.setState({ ready: false })
      setTimeout(() => this.setState({ ready: true }), 50)
    }
  }

  constructor (props) {
    super(props)
    this.state = { pathLengths: {}, ready: false }
  }

  componentDidMount () {
    this.setState({ ready: true })
  }

  getPaths () {
    let paths = []
    let current = null

    let added = 0
    let { instances, deck } = this.props

    if (!this.state.ready) {
      return paths
    }

    instances.forEach((instance, i) => {
      if (!instance.distance) {
        return
      }

      if (toInt(SECTOR_ID_TO_DECKS[instance.vertexes.begin.sectorId]) !== toInt(deck)) {
        if (!i || instance.vertexes.begin.sectorId !== instances[i - 1].vertexes.begin.sectorId) {
          paths.push(current)
          current = null
        }
        return
      }

      let anotherDeckPrevious = i && toInt(instances[i - 1].vertexes.begin.sectorId) !== toInt(instance.vertexes.begin.sectorId)
      if (anotherDeckPrevious && added) {
        paths.push(current)
        current = null
      }
      if (!current) {
        current = `M `
      }
      current += `${instance.vertexes.begin.x},${HEIGHT - instance.vertexes.begin.y}`
      let anotherDeckNext = (
        instances[i + 1] && (
          toInt(SECTOR_ID_TO_DECKS[instances[i + 1].vertexes.begin.sectorId]) !== toInt(deck) ||
          (instances[i + 1].distance === 0 && toInt(SECTOR_ID_TO_DECKS[instances[i + 1].vertexes.end.sectorId]) !== toInt(deck))
        )
      )
      if (!instances[i + 1] || anotherDeckNext) {
        current += ` ${instance.vertexes.end.x},${HEIGHT - instance.vertexes.end.y}`
      }
      current += ` `
      added += 1
    })

    if (current) {
      paths.push(current)
    }

    if (!added) {
      return []
    }

    return paths
  }

  getHelmet () {

  }

  render () {
    let { strokeWidth } = this.props

    return (
      <g className="animated-paths-container" style={{ opacity: !this.state.ready ? 0 : 1 }}>
        {this.props.instances.length &&
        <rect
          x={this.props.instances[0].vertexes.begin.x - 500}
          y={HEIGHT - this.props.instances[0].vertexes.begin.y - 500}
          width={2000}
          height={2000}
        />
        }

        {this.getPaths().map((path, index) => {
          if (!path || path === `M `) {
            return null
          }
          return (
            <path
              key={index}
              id={`animated-path-${index}`}
              d={path}
              stroke={COLORS.animatedPath}
              strokeWidth={strokeWidth}
              style={this.props.animation ? {
                strokeDasharray: this.state.pathLengths[index],
                strokeDashoffset: this.state.pathLengths[index],
                animationDelay: `${index * this.props.animationTime}s`
              } : {}}
              ref={(el) => {
                if (el && this.state.pathLengths[index] !== el.getTotalLength()) {
                  this.setState({ pathLengths: { ...this.state.pathLengths, [index]: el.getTotalLength() } })
                }
              }}
            >
              { this.props.animation &&
              <Helmet >
                <style type="text/css">{`
                  path#animated-path-${index} {
                    animation: draw ${this.props.animationTime}s 1 linear;
                    animation-fill-mode: forwards;
                    stroke-dasharray: ${this.state.pathLengths[index]};
                    stroke-dashoffset: ${this.state.pathLengths[index]};
                  }
                `}</style>
              </Helmet>
              }
            </path>
          )
        })}

      </g>
    )
  }
}
