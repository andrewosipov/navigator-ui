import React, { PureComponent } from 'react'
import Venue from 'src/containers/layers/elements/Venue'

export default class Venues extends PureComponent {
    static defaultProps = {
      instances: []
    }

    render () {
      return (
        <span>
          {this.props.instances.map(venue =>
            <Venue
              key={venue.id}
              instance={venue}
              categories={this.props.categories}
              rotation={this.props.rotation}
              perspectiveRotation={this.props.perspectiveRotation}
              scale={this.props.scale}
              selected={this.props.selected}
              user={this.props.user}
              coordsFactor={this.props.coordsFactor}
              selectVenue={this.props.selectVenue}
              selectedPaths={this.props.selectedPaths}
              deck={this.props.deck}
              crewOnly={this.props.crewOnly}
              mapMode={this.props.mapMode}
            />
          )}
        </span>
      )
    }
}
