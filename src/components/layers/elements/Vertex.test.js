import React from 'react'
import { shallow } from 'enzyme'

import Vertex from './Vertex'
import data from '../../../helpers/tests/data'

test('Vertex', () => {
    let component = shallow(
        <Vertex
            instance={data.vertex}
        />
    )
    expect(component.is(`#vertex-${data.vertex.id}-container`)).toBe(true)
    expect(component.state('hovered')).toBe(false)
    component.find('.vertex-circle').simulate('mouseover')
    expect(component.state('hovered')).toBe(true)
    component.find('.vertex-circle').simulate('mouseleave')
    expect(component.state('hovered')).toBe(false)
})