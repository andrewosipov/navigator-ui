import React, { Component } from 'react'
import classnames from 'classnames'
import $ from 'jquery'

import { isWideScreen, toInt } from '../../../helpers/misc'
import {
  CREW_MAP_SUB_MODE,
  HEIGHT,
  SAILORS_MAP_SUB_MODE
} from '../../../constants'
import '../../../styles/Player.scss'

export default class Player extends Component {
  constructor (props) {
    super(props)
    this.state = {
      containerWidth: 0,
      containerHeight: 0
    }
  }

  shouldComponentUpdate (nextProps, nextState) {
    return (
      this.props.instance.isCrew !== nextProps.instance.isCrew ||
      this.props.instance.x !== nextProps.instance.x ||
      this.props.instance.y !== nextProps.instance.y ||
      this.props.mapSubModes !== nextProps.mapSubModes ||
      this.props.perspectiveRotation !== nextProps.perspectiveRotation ||
      this.props.rotation !== nextProps.rotation ||
      this.props.deck !== nextProps.deck ||
      (this.props.user && nextProps.user && this.props.user.cabin !== nextProps.user.cabin) ||
      this.props.scale !== nextProps.scale ||
      (this.props.perspectiveRotation && this.props.user !== nextProps.user)
    )
  }

  initContainer (container) {
    if (!this.initialized && container) {
      this.initialized = true
      this.setState({
        containerWidth: $(container).width(),
        containerHeight: $(container).height()
      })
    }
  }

  getTransform () {
    const {
      instance,
      coordsFactor,
      scale
    } = this.props

    let { x, y } = instance

    let fixedScale = Math.max(scale, 3)
    let d = (fixedScale - scale) / 2

    return [
      `translateX(${x / coordsFactor}px)`,
      `translateY(${(HEIGHT / 2 - y) / coordsFactor}px)`,
      `scale(${this.getScale(fixedScale, d)})`
    ].join(' ')
  }

  getScale (fixedScale, d) {
    let scaleX = 1 / fixedScale
    let scaleY = this.props.perspectiveRotation ? 1 / (1 + d) : scaleX
    if (this.props.perspectiveRotation && isWideScreen()) {
      scaleX *= 1.5
      scaleY *= 1.5
    }
    return `${scaleX}, ${scaleY}`
  }

  getBorder (cId) {
    const outlaws = [9, 3, 11, 1, 2]
    if (!outlaws.includes(cId)) {
      return '1px solid rgba(255,255,255,0.8)'
    }
  }

  getBoxShadow (cId) {
    const outlaws = [9, 3, 11, 1, 2]
    if (!outlaws.includes(cId)) {
      return '0 2px 2px rgba(0,0,0,0.6)'
    }
  }

  isSelected () {
    return this.props.selected && toInt(this.props.instance.id) === toInt(this.props.selected.id)
  }

  render () {
    const { instance, mapSubModes } = this.props

    if (!mapSubModes.includes(CREW_MAP_SUB_MODE) && !mapSubModes.includes(SAILORS_MAP_SUB_MODE)) {
      return null
    }

    return (
      <div
        id={`player-${instance.id}`}
        className={classnames(
          'Player',
          { crew: instance.isCrew && mapSubModes.includes(CREW_MAP_SUB_MODE) },
          { sailor: !instance.isCrew && mapSubModes.includes(SAILORS_MAP_SUB_MODE) }
        )}
        style={{ transform: this.getTransform(), transformOrigin: '0 0', opacity: this.isSelected() ? '1' : undefined }}
      />
    )
  }
}
