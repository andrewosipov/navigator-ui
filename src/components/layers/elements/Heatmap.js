import React, { Component } from 'react'

import '../../../styles/Heatmap.scss'
import { toInt } from '../../../helpers/misc'
import {
  HEIGHT,
  OCCUPANCY_MAP_SUB_MODE,
  WIDTH
} from '../../../constants'

export default class Heatmap extends Component {
  getProps () {
    if (this.props.width <= 576) {
      return {
        radius: 5,
        blur: 10
      }
    }

    return {
      radius: 10,
      blur: 15
    }
  }

  shouldComponentUpdate (nextProps, nextState) {
    return (
      this.props.users !== nextProps.users ||
      this.props.mapMode !== nextProps.mapMode ||
      this.props.mapSubModes !== nextProps.mapSubModes ||
      this.props.coordsFactor !== nextProps.coordsFactor ||
      this.props.deck !== nextProps.deck ||
      this.props.width !== nextProps.width ||
      this.props.height !== nextProps.height
    )
  }

  componentDidMount () {
    this.canvasContext = this.canvas.getContext('2d')
    this.renderHeatmap()
  }

  componentDidUpdate () {
    if (this.canvas && this.canvas.getContext) {
      this.canvasContext = this.canvas.getContext('2d')
      this.reRenderHeatmap()
    }
  }

  reRenderHeatmap () {
    this.canvasContext.clearRect(0, 0, this.getWidth(), this.getHeight())
    this.renderHeatmap()
  }

  renderCircle () {
    const { radius, blur } = this.getProps()
    const circle = document.createElement('canvas')
    const circleContext = circle.getContext('2d')
    const radius2 = radius + blur

    circle.width = circle.height = radius2 * 2

    circleContext.shadowOffsetX = circleContext.shadowOffsetY = radius2 * 2
    circleContext.shadowBlur = blur
    circleContext.shadowColor = 'black'

    circleContext.beginPath()
    circleContext.arc(-radius2, -radius2, radius, 0, Math.PI * 2, true)
    circleContext.closePath()
    circleContext.fill()

    return circle
  }

  renderGradient () {
    const defaultGradient = {
      0.4: 'blue',
      0.6: 'cyan',
      0.7: 'lime',
      0.8: 'yellow',
      1.0: 'red'
    }

    const canvas = document.createElement('canvas')
    const gradientContext = canvas.getContext('2d')
    const gradient = gradientContext.createLinearGradient(0, 0, 0, 256)

    canvas.width = 1
    canvas.height = 256

    for (let i in defaultGradient) {
      gradient.addColorStop(+i, defaultGradient[i])
    }

    gradientContext.fillStyle = gradient
    gradientContext.fillRect(0, 0, 1, 256)

    return gradientContext.getImageData(0, 0, 1, 256).data
  }

  renderHeatmap () {
    const { coordsFactor, alpha = 0.5 } = this.props
    const { radius, blur } = this.getProps()
    const users = this.props.users.filter(user => toInt(user.deck) === toInt(this.props.deck))

    // render gray scale circles with opacity and blur through x y
    users.forEach((s, index) => {
      const _x = s.x / coordsFactor
      const _y = (HEIGHT / 2 - s.y + HEIGHT / 2) / coordsFactor
      this.canvasContext.globalAlpha = alpha
      this.canvasContext.drawImage(this.renderCircle(), _x - radius - blur, _y - radius - blur)
    })

    // render pixels over gray scale circles with gradient colors
    const colored = this.canvasContext.getImageData(0, 0, this.getWidth(), this.getHeight())
    const gradient = this.renderGradient()
    for (let i = 0, len = colored.data.length, j; i < len; i += 4) {
      j = colored.data[i + 3] * 4 // get gradient color from opacity value
      if (j) {
        colored.data[i] = gradient[j]
        colored.data[i + 1] = gradient[j + 1]
        colored.data[i + 2] = gradient[j + 2]
      }
    }

    this.canvasContext.putImageData(colored, 0, 0)
  }

  getTransform (x = 0, y = 0) {
    const { coordsFactor } = this.props
    return [
      `translateX(${x / coordsFactor}px)`,
      `translateY(${(HEIGHT / 2 - y) / coordsFactor}px)`
    ].join(' ')
  }

  getWidth () {
    return WIDTH / this.props.coordsFactor
  }

  getHeight () {
    return HEIGHT / this.props.coordsFactor
  }

  render () {
    const { mapSubModes } = this.props

    if (!mapSubModes.includes(OCCUPANCY_MAP_SUB_MODE)) {
      return null
    }

    return (
      <canvas
        className="Heatmap"
        style={{ marginTop: -this.getHeight() / 2 }}
        ref={ref => { this.canvas = ref }}
        width={this.getWidth()}
        height={this.getHeight()}
      />
    )
  }
}
