import React from 'react'
import { shallow } from 'enzyme'

import Venue from './Venue'
import data from '../../../helpers/tests/data'
import { resizeTo } from '../../../helpers/tests/utils'
import { NORMAL_MAP_MODE } from 'src/constants'

test('Venue', () => {
  let selected = {}
  let component = shallow(
    <Venue
      instance={data.venues[0]}
      categories={data.categories}
      scale={2}
      coordsFactor={10}
      user={data.user}
      perspectiveRotation={60}
      selectVenue={(instance) => selected = instance}
      mapMode={NORMAL_MAP_MODE}
      tags={[]}
    />
  )
  expect(component.hasClass('Venue')).toBe(true)
  resizeTo(1000)
  component.update()
  component.simulate('click')
  expect(selected.id).toBe(data.venues[0].id)
})
