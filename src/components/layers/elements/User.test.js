import React from 'react'
import { shallow } from 'enzyme'

import User from './User'
import data from '../../../helpers/tests/data'

test('User', () => {
    let component = shallow(
        <User
            instance={data.user}
            zoom={1}
            heading={35}
        />
    )
    expect(component.hasClass('user-container')).toBe(true)
    expect(component.hasClass('party')).toBe(false)

    component = shallow(
        <User
            instance={data.user}
            zoom={1}
            heading={35}
            party
        />
    )
    expect(component.hasClass('party')).toBe(true)

})