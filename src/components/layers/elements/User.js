import React from 'react'
import styled from 'styled-components'
import { HEIGHT, CONNECTION_TYPES, THEMES } from 'src/constants'
import cn from 'classnames'

const User = ({ instance, zoom, heading, party = false, connectionType, theme }) => (
  <g
    className={cn('user-container', { party })}
    style={{
      transform: `translate(${instance.x}px, ${HEIGHT - instance.y}px) scale(${4.2 - Math.min(zoom, 2.5)})`
    }}
  >
    { theme !== THEMES.BLISS &&
      <g className="heading">
        <polygon
          points="-50,-50 0,1500 1500,0"
          style={{ transform: `rotate(${heading}deg) rotate(-45deg)` }}
        />
      </g>
    }
    <circle
      className="user-background"
      cx="0"
      cy="0"
      r={theme === THEMES.BLISS ? 60 : 82}
    />
    <circle
      className="user"
      cx="0"
      cy="0"
      r={theme === THEMES.BLISS ? 80 : 100}
    />
    { theme === THEMES.BLISS &&
      <UserHighlight
        noSignal={connectionType === CONNECTION_TYPES.NONE || connectionType === CONNECTION_TYPES.UNKNOWN}
        cx="0"
        cy="0"
        r={200}
      />
    }
    { theme !== THEMES.BLISS &&
      <g
        style={{ transform: `rotate(${heading}deg) rotate(90deg) translate(-48px, -70px)` }}
        id="blue-dot"
        fill="#4285F4"
        fillRule="nonzero"
        stroke="#FFFFFF"
        strokeWidth="12.1999998"
      >
        <polygon
          id="icon/maps/navigation_24px"
          points="50.1704479 16.0546547 7.3357551 119.139725 11.3907727 123.141387 50.1704479 106.232955 88.9501231 123.141387 93.0051407 119.139725"
        ></polygon>
      </g>
    }
  </g>
)

export default User

const UserHighlight = styled.circle`
  stroke: ${props => props.noSignal ? '#464646' : '#4285F4'};
  stroke-width: 4;
  fill: ${props => props.noSignal ? 'rgba(70, 70, 70, 0.2)' : 'rgba(66, 133, 244, 0.2)'};
`
