import React, { Component } from 'react'
import { HEIGHT, PATH_STROKE_WIDTH, TEXT_SIZE, COLORS } from '../../../constants'
import classNames from 'classnames'

class Vertex extends Component {
    static defaultProps = {
        dx: 0,
        dy: 0,
    }

    constructor(props) {
        super(props)
        this.state = {
            hovered: false,
        }
    }

    render() {
        const instance = this.props.instance
        return (
            <g id={`vertex-${instance.id}-container`}>
                <line
                    stroke={COLORS.vertex}
                    strokeWidth={PATH_STROKE_WIDTH}
                    x1={instance.x - this.props.dx}
                    y1={HEIGHT - instance.y - this.props.dy}
                    x2={instance.x + this.props.dx}
                    y2={HEIGHT - instance.y + this.props.dy}
                />

                <text
                    fontSize={TEXT_SIZE}
                    x={instance.x + PATH_STROKE_WIDTH}
                    y={HEIGHT - instance.y - PATH_STROKE_WIDTH}
                    fill={COLORS.vertex}
                    filter="url(#white-background)"
                >
                    {instance.sectorId}-{instance.id} {this.state.hovered && `(id: ${instance.id}, isPortal: ${instance.isPortal}, exitVenueId: ${instance.exitVenueId}, crewOnly: ${instance.crewOnly})`}
                </text>

                <circle
                    strokeWidth={PATH_STROKE_WIDTH}
                    fill="transparent"
                    cx={instance.x}
                    cy={HEIGHT - instance.y}
                    r="100"
                    className="vertex-circle"
                    onMouseOver={() => {
                        this.setState({
                            hovered: true,
                        });
                    }}
                    onMouseLeave={() => {
                        this.setState({
                            hovered: false,
                        });
                    }}
                />
            </g>
        )
    }
}

export default Vertex