import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

import 'src/styles/Venue.scss'
import {
  HEIGHT,
  SECTOR_ID_TO_DECKS,
  DECK_TO_SECTORS_ID,
  NORMAL_MAP_MODE,
  THEMES,
  PAGES
} from 'src/constants'
import { toInt, isWideScreen } from 'src/helpers/misc'
import DynamicFormattedMessage from 'src/components/common/DynamicFormattedMessage'
import Elevator from 'src/components/common/Elevator'
import Stair from 'src/components/common/Stair'
import MusterStationBtn from 'src/components/common/MusterStationBtn'

import SVG from 'src/components/common/SVG'

export default class Venue extends Component {
  static propTypes = {
    theme: PropTypes.string.isRequired,
    page: PropTypes.string
  }

  shouldComponentUpdate (nextProps, nextState) { // TODO: PureComponent?
    return (
      this.state !== nextState ||
      this.props.theme !== nextProps.theme ||
      this.props.visitedPages !== nextProps.visitedPages ||
      this.props.instance.name !== nextProps.instance.name ||
      this.props.mapMode !== nextProps.mapMode ||
      this.props.crewOnly !== nextProps.crewOnly ||
      this.props.coordsFactor !== nextProps.coordsFactor ||
      this.props.perspectiveRotation !== nextProps.perspectiveRotation ||
      this.props.scale !== nextProps.scale ||
      this.props.rotation !== nextProps.rotation ||
      this.props.deck !== nextProps.deck ||
      (this.props.user && nextProps.user && this.props.user.cabin !== nextProps.user.cabin) ||
      this.props.selectedPaths !== nextProps.selectedPaths ||
      ((this.props.selected !== nextProps.selected) && (
        (this.props.selected || {}).id === this.props.instance.id ||
          (nextProps.selected || {}).id === this.props.instance.id
      )) ||
      (this.props.perspectiveRotation && this.props.user !== nextProps.user)
    )
  }

  getTransform () {
    const {
      instance,
      coordsFactor,
      perspectiveRotation,
      scale,
      rotation
    } = this.props

    let { x, y } = instance

    let rotateZ = 0
    if (perspectiveRotation) {
      rotateZ = perspectiveRotation > 45 ? 0 : (6 * Math.cos((rotation + 90) * Math.PI / 180))
    }

    let translateZ = 40

    if (isWideScreen() && perspectiveRotation) {
      translateZ += 35
    }

    if (instance.isService && instance.isService()) {
      translateZ = 10
    }
    if (instance.isCabin && instance.isCabin()) {
      translateZ = -4
    }

    let portalPath = this.getPortalPathInstance()
    if (portalPath) {
      let { begin, end } = portalPath.vertexes
      if (end.sectorId === instance.sectorId) {
        x = end.x
        y = end.y
      } else {
        x = begin.x
        y = begin.y
      }

      translateZ = 20
      if (isWideScreen() && perspectiveRotation) {
        translateZ += 25
      }
    }

    let fixedScale = Math.max(scale, 3)
    let d = (fixedScale - scale) / 2

    let additionalShiftX = 0
    let additionalShiftY = 0

    if (instance.isCabin && instance.isCabin()) {
      additionalShiftY /= 2
      additionalShiftX *= 2.2
    }
    if (perspectiveRotation) {
      let angle = (rotation + 90) * Math.PI / 180
      let ax = additionalShiftX + 4 * additionalShiftX * d * fixedScale / scale
      let ay = additionalShiftY
      additionalShiftX = (ax * Math.cos(angle) + ay * Math.sin(angle))
      additionalShiftY = (-ax * Math.sin(angle) + ay * Math.cos(angle))

      if (rotateZ) {
        additionalShiftX += 3 * Math.cos(angle)
      }
    }

    let venueLabelRows = Math.ceil(this.props.instance.name.replace('Cabin ', '').length / 8)
    let translateZMargin = venueLabelRows >= 3 ? (venueLabelRows - 2) * 30 : 0

    return [
      `translate(-${100 / (2 * fixedScale)}%, -${100 / (2 * fixedScale)}%)`,
      `translateX(${x / coordsFactor}px)`,
      `translateY(${(HEIGHT / 2 - y) / coordsFactor}px)`,
      `translateZ(${translateZ + translateZMargin}px)`,
      `translate(${`${additionalShiftX}px, ${additionalShiftY}px`})`,
      `rotateX(${this.getRotateX()}deg)`,
      `rotateZ(${rotateZ}deg)`,
      `rotateY(${this.getRotateY()}deg)`,
      `scale(${this.getScale(fixedScale, d)})`
    ].join(' ')
  }

  getRotateX () {
    return this.props.perspectiveRotation ? -90 : 0
  }

  getRotateY () {
    return this.props.perspectiveRotation ? this.props.rotation : 0
  }

  getScale (fixedScale, d) {
    let scaleX = 1 / fixedScale
    let scaleY = this.props.perspectiveRotation ? 1 / (1 + d) : scaleX
    if (this.props.perspectiveRotation && isWideScreen()) {
      scaleX *= 1.5
      scaleY *= 1.5
    }
    return `${scaleX}, ${scaleY}`
  }

  getVenueIconContainerTransform () {
    const {
      perspectiveRotation,
      rotation
    } = this.props
    return [
      `rotateZ(${(perspectiveRotation || (this.props.instance.isVerticalCabin && this.props.instance.isVerticalCabin())) ? 0 : -rotation}deg)`
    ].join(' ')
  }

  getVenueIconTransform () {
    const { perspectiveRotation } = this.props
    let transforms = []
    if (perspectiveRotation && !isWideScreen()) {
      transforms.push(`scale(.8)`)
    }
    return transforms.join(' ')
  }

  getBorder (cId) {
    const outlaws = [9, 3, 11, 1, 2]
    if (!outlaws.includes(cId)) {
      return '1px solid rgba(255,255,255,0.8)'
    }
  }

  getBoxShadow (cId) {
    const outlaws = [9, 3, 11, 1, 2]
    if (!outlaws.includes(cId)) {
      return '0 2px 2px rgba(0,0,0,0.6)'
    }
  }

  getPortalPathInstance () {
    if (!this.props.perspectiveRotation || !this.props.instance.isPortal()) {
      return null
    }
    let exitPointPaths = this.props.selectedPaths.filter(p =>
      this.props.instance.hasExitPointPath(p) &&
            p.vertexes.begin.sectorId === DECK_TO_SECTORS_ID[this.props.deck] &&
            p.vertexes.begin.sectorId !== p.vertexes.end.sectorId
    )
    if (!exitPointPaths.length) {
      return null
    }

    return exitPointPaths.find(p => p.vertexes.begin.sectorId !== p.vertexes.end.sectorId)
  }

  getPortal () {
    let portalPath = this.getPortalPathInstance()
    if (!portalPath) {
      return null
    }

    if (this.props.instance.isElevator()) {
      return (
        <Elevator
          down={SECTOR_ID_TO_DECKS[portalPath.vertexes.begin.sectorId] > SECTOR_ID_TO_DECKS[portalPath.vertexes.end.sectorId]}
        />
      )
    }

    if (this.props.instance.isStairs()) {
      return (
        <Stair
          down={SECTOR_ID_TO_DECKS[portalPath.vertexes.begin.sectorId] > SECTOR_ID_TO_DECKS[portalPath.vertexes.end.sectorId]}
        />
      )
    }
  }

  shouldRender () {
    let { instance } = this.props
    if (!instance.isCabin()) {
      return true
    }
    return (
    // (
    //     scale > 2.3 &&
    //     !perspectiveRotation
    // ) ||
      this.isSelected() || (
        this.props.user &&
        (toInt(instance.id) === toInt(this.props.user.cabin))
      )
    )
  }

  isSelected () {
    return this.props.selected && toInt(this.props.instance.id) === toInt(this.props.selected.id)
  }

  getOpacity () {
    if (this.props.perspectiveRotation) {
      let { x, y } = this.props.instance
      let dx = (this.props.user.x - x) * -Math.sin(this.props.rotation)
      let dy = Math.abs(this.props.user.y - y)
      if (dy < 300 && (Math.abs(dx) < 50 || (dx < 1000 && dx > 0))) {
        return 0.45
      }
    }

    return 1
  }

  shouldRenderIcon () {
    if (this.props.theme === THEMES.BLISS || this.props.instance.isMusterStation()) {
      return false
    }
    if (this.props.mapMode === NORMAL_MAP_MODE) {
      return true
    } else {
      if (this.props.instance.isService()) {
        return false
      }
    }
    return false
  }

  showRenderName () {
    const { instance, mapMode } = this.props
    return (!instance.isService() || instance.isCabin()) &&
      !instance.isMusterStation() &&
      mapMode === NORMAL_MAP_MODE
  }

  render () { // TODO make shouldRender for submap modes
    const { instance, categories, perspectiveRotation, crewOnly, theme, page } = this.props

    if (!this.shouldRender()) {
      return null
    }

    let portal = this.getPortal()
    return (
      <div
        id={`venue-${instance.id}`}
        className={classnames(
          'Venue',
          {
            service: instance.isService(),
            elevator: !!portal,
            cabin: instance.isCabin(),
            'vertical-cabin': instance.isVerticalCabin(),
            perspective: !!perspectiveRotation
          }
        )}
        style={{
          transform: this.getTransform(),
          transformOrigin: '0 0',
          opacity: this.isSelected() ? '1' : undefined
        }}
        onClick={(e) => {
          if (e) {
            e.stopPropagation()
          }
          if (this.props.selectVenue && this.props.mapMode === NORMAL_MAP_MODE) {
            this.props.selectVenue(instance)
          }
        }}
      >
        <div
          className="venue-icon-container"
          style={{ transform: this.getVenueIconContainerTransform(), opacity: this.getOpacity() }}
        >
          {this.shouldRenderIcon() &&
            <div className={classnames('venue-icon', { crewonly: instance.crewOnly })} style={{ transform: this.getVenueIconTransform() }}>
              {!portal && instance.categories.map((cat, index) => {
                let category = categories.find(c => c.id === cat.id)
                if (!category) {
                  return null
                }
                return (
                  <SVG
                    key={category.id}
                    className={classnames({ bd: category && !category.isService })}
                  >
                    {category.getIcon(instance.crewOnly)}
                  </SVG>
                )
              })}
            </div>
          }
          {instance.isMusterStation() && page !== PAGES.NAVIGATION &&
            <MusterStationBtn style={{ transform: this.getVenueIconTransform() }} />
          }
          {portal}
          {this.showRenderName() &&
            <div className={classnames('venue-label', { bliss: theme === THEMES.BLISS })}>
              { crewOnly && instance.representations.length
                ? instance.representations.reduce((acc, cur) => acc.concat(cur.name), [])
                : <DynamicFormattedMessage
                  id={`venue.${instance.id}`}
                  defaultMessage={instance.name.replace('Cabin', '')}
                />
              }
            </div>
          }
        </div>
      </div>
    )
  }
}
