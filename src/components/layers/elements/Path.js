import React from 'react'
import classNames from 'classnames'
import { HEIGHT, PATH_STROKE_WIDTH, COLORS, } from '../../../constants'
import Vertex from './Vertex'

export default ({ instance, className, style, displayVertexes = false, stroke = COLORS.path, strokeWidth = PATH_STROKE_WIDTH }) => (
    <g
        key={instance.id}
        className={classNames('path-container', className)}
    >
        <line
            stroke={stroke}
            strokeWidth={strokeWidth}
            x1={instance.vertexes.begin.x}
            y1={HEIGHT - instance.vertexes.begin.y}
            x2={instance.vertexes.end.x}
            y2={HEIGHT - instance.vertexes.end.y}
            style={style}
        />

        {displayVertexes &&
            <g>
                <Vertex
                    instance={instance.vertexes.begin}
                    dx={PATH_STROKE_WIDTH / 2}
                />

                <Vertex
                    instance={instance.vertexes.end}
                    dy={PATH_STROKE_WIDTH / 2}
                />
            </g>
        }
    </g>
)