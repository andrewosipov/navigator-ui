import React from 'react'

import Destination from '../../directions/Destination'
import Venue from './Venue'
import { HEIGHT } from '../../../constants'

export default class extends Venue {
    static propTypes = {}

    getPortalPathInstance () {
      return null
    }

    getTransform () {
      const {
        instance,
        coordsFactor,
        scale,
        perspectiveRotation
      } = this.props

      let { x, y } = instance

      let fixedScale = Math.max(scale, 3)

      let rotateX = 0
      if (perspectiveRotation) {
        rotateX = perspectiveRotation > 45 ? 0 : 5
      }

      return [
        `translateX(${x / coordsFactor + 1}px)`,
        `translateY(${(HEIGHT / 2 - y) / coordsFactor - 1}px)`,
        `translateZ(-20px)`,
        `rotateZ(90deg)`,
        `rotateX(${rotateX}deg)`,
        `scale(${1 / fixedScale})`
      ].join(' ')
    }

    render () {
      return (
        <div
          className="DestinationContainer"
          style={{
            transform: this.getTransform(),
            transformOrigin: '0 0',
            transformStyle: 'preserve-3d'
          }}
        >
          <Destination to={this.props.to} />
        </div>
      )
    }
}
