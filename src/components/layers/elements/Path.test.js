import React from 'react'
import { shallow } from 'enzyme'

import Path from './Path'
import data from '../../../helpers/tests/data'

test('Path', () => {
    let component = shallow(
        <Path
            instance={data.path}
            className="test-path-class"
        />
    )
    expect(component.hasClass('path-container')).toBe(true)
    expect(component.hasClass('test-path-class')).toBe(true)
})