import React, { Component } from 'react'
import classnames from 'classnames'
import $ from 'jquery'

import { isWideScreen } from '../../../helpers/misc'

import DynamicFormattedMessage from '../../common/DynamicFormattedMessage'

import {
  HEIGHT,
  LABELS_MAP_SUB_MODE
} from '../../../constants'

import '../../../styles/VenueInfoBox.scss'

export default class VenueInfoBox extends Component {
  constructor (props) {
    super(props)
    this.state = {
      containerWidth: 0,
      containerHeight: 0
    }
  }

  shouldComponentUpdate (nextProps, nextState) {
    return (
      this.state !== nextState ||
      this.props.mapMode !== nextProps.mapMode ||
      this.props.mapSubModes !== nextProps.mapSubModes ||
      this.props.instance.capacity !== nextProps.instance.capacity ||
      this.props.instance.id !== nextProps.instance.id ||
      this.props.users !== nextProps.users ||
      this.props.crewOnly !== nextProps.crewOnly ||
      this.props.coordsFactor !== nextProps.coordsFactor ||
      this.props.perspectiveRotation !== nextProps.perspectiveRotation ||
      this.props.scale !== nextProps.scale ||
      this.props.rotation !== nextProps.rotation ||
      this.props.deck !== nextProps.deck ||
      (this.props.user && nextProps.user && this.props.user.cabin !== nextProps.user.cabin) ||
      this.props.selectedPaths !== nextProps.selectedPaths ||
      ((this.props.selected !== nextProps.selected) && (
        (this.props.selected || {}).id === this.props.instance.id ||
          (nextProps.selected || {}).id === this.props.instance.id
      )) ||
      (this.props.perspectiveRotation && this.props.user !== nextProps.user)
    )
  }

  initContainer (container) {
    if (!this.initialized && container) {
      this.initialized = true
      this.setState({
        containerWidth: $(container).width(),
        containerHeight: $(container).height()
      })
    }
  }

  getTransform () {
    const {
      instance,
      coordsFactor,
      perspectiveRotation,
      scale,
      rotation
    } = this.props

    let { x, y } = instance

    let rotateZ = 0
    if (perspectiveRotation) {
      rotateZ = perspectiveRotation > 45 ? 0 : (6 * Math.cos((rotation + 90) * Math.PI / 180))
    }

    let translateZ = 40

    if (isWideScreen() && perspectiveRotation) {
      translateZ += 35
    }

    let fixedScale = Math.max(scale, 3)
    let d = (fixedScale - scale) / 2

    let additionalShiftX = 0
    let additionalShiftY = 0
    if (this.state.containerWidth) {
      additionalShiftX = -this.state.containerHeight / (2 * fixedScale)
      additionalShiftY = -this.state.containerWidth / (2 * fixedScale)

      if (perspectiveRotation) {
        let angle = (rotation + 90) * Math.PI / 180
        let ax = additionalShiftX + 4 * additionalShiftX * d * fixedScale / scale
        let ay = additionalShiftY
        additionalShiftX = (ax * Math.cos(angle) + ay * Math.sin(angle))
        additionalShiftY = (-ax * Math.sin(angle) + ay * Math.cos(angle))

        if (rotateZ) {
          additionalShiftX += 3 * Math.cos(angle)
        }
      }
    }

    rotateZ = (perspectiveRotation || (this.props.instance.isVerticalCabin && this.props.instance.isVerticalCabin())) ? 0 : -rotation
    let xdif = -20
    let ydif = 20
    if (this.props.height > this.props.width && this.props.width >= 768) {
      xdif = 0
      ydif = this.props.instance.id === 8 || this.props.instance.id === 6 ? -20 : 0
    }

    return [
      `translateX(${x / coordsFactor + xdif}px)`,
      `translateY(${(HEIGHT / 2 - y) / coordsFactor + ydif}px)`,
      `translateZ(${translateZ}px)`,
      `translate(${`${additionalShiftX}px, ${additionalShiftY}px`})`,
      `rotateX(${this.getRotateX()}deg)`,
      `rotateZ(${rotateZ}deg)`,
      `rotateY(${this.getRotateY()}deg)`,
      `scale(${this.getScale(fixedScale, d)})`
    ].join(' ')
  }

  getRotateX () {
    return this.props.perspectiveRotation ? -90 : 0
  }

  getRotateY () {
    return this.props.perspectiveRotation ? this.props.rotation : 0
  }

  getScale (fixedScale, d) {
    let scaleX = 1 / fixedScale
    let scaleY = this.props.perspectiveRotation ? 1 / (1 + d) : scaleX
    if (this.props.perspectiveRotation && isWideScreen()) {
      scaleX *= 1.5
      scaleY *= 1.5
    }
    return `${scaleX}, ${scaleY}`
  }

  getVenueInfoBoxTransform () {
    const {
      perspectiveRotation,
      rotation
    } = this.props
    return [
      `rotateZ(${(perspectiveRotation || (this.props.instance.isVerticalCabin && this.props.instance.isVerticalCabin())) ? 0 : -rotation}deg)`
    ].join(' ')
  }

  getTotalCount () {
    return this.props.users.reduce((count, user) => {
      return user.venue && this.props.instance.id === user.venue.id ? ++count : count
    }, 0)
  }

  getCrewCount () {
    return this.props.users.reduce((count, user) => {
      return user.isCrew && user.venue && this.props.instance.id === user.venue.id ? ++count : count
    }, 0)
  }

  getSailorCount () {
    return this.getTotalCount() - this.getCrewCount()
  }

  getOccupancy () {
    if (!this.props.instance.capacity) return 0
    return this.getTotalCount() / this.props.instance.capacity * 100
  }

  getIndicators () {
    let indicators = []
    const crewSailorRate = this.props.instance.crewSailorRate !== 1 ? this.props.instance.crewSailorRate / 100 : 0.2
    const currentRate = this.getCrewCount() / this.getTotalCount()
    if (Number.isNaN(currentRate)) return []

    if (2 / 3 * crewSailorRate < currentRate <= crewSailorRate) {
      indicators = [0]
    } else {
      if (1 / 3 * crewSailorRate < currentRate <= 2 / 3 * crewSailorRate) {
        indicators = [0, 1]
      } else {
        if (currentRate <= 1 / 3 * crewSailorRate) {
          indicators = [0, 1, 2]
        }
      }
    }

    return indicators.map((i, index) => <i key={index} />)
  }

  render () {
    const { instance, perspectiveRotation, crewOnly, mapSubModes } = this.props
    if (instance.isService() || instance.isCabin() || instance.crewOnly) {
      return null
    }

    if (!mapSubModes.includes(LABELS_MAP_SUB_MODE)) {
      return null
    }

    return (
      <div
        id={`venue-info-box-${instance.id}`}
        className={classnames(
          'VenueInfoBox',
          {
            perspective: !!perspectiveRotation
          }
        )}
        style={{ transform: this.getTransform(), transformOrigin: '0 0' }}
        onClick={(e) => {
          e && e.stopPropagation()
          return null
        }}
        ref={(el) => this.initContainer(el)}
        data-capacity={this.props.instance.capacity}
      >
        <div
          className={classnames('occupancy',
            { green: this.getOccupancy() <= 20 },
            { yellow: this.getOccupancy() > 20 && this.getOccupancy() <= 50 },
            { orange: this.getOccupancy() > 50 && this.getOccupancy() <= 70 },
            { red: this.getOccupancy() > 70 }
          )
          }
          style={{ width: `${this.getOccupancy()}%` }}
        />

        <div className="title">{
          crewOnly && instance.representations.length
            ? instance.representations.reduce((acc, cur) => acc.concat(cur.name), [])
            : <DynamicFormattedMessage
              id={`venue.${instance.id}`}
              defaultMessage={instance.name.replace('Cabin', '')}
            />
        }</div>

        <div className="info">
          <span className="total-count">{this.getTotalCount()}</span>
          <span className="crew-count">{this.getCrewCount()}</span>
          <span className="indicator">{this.getIndicators()}</span>
        </div>
      </div>
    )
  }
}
