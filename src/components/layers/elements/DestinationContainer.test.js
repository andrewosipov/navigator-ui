import React from 'react'
import { shallow } from 'enzyme'

import DestinationContainer from './DestinationContainer'
import data from '../../../helpers/tests/data'

test('DestinationContainer', () => {
    let component = shallow(
        <DestinationContainer
            instance={data.venues[0]}
            scale={2}
            coordsFactor={10}
            perspectiveRotation={60}
        />
    )
    expect(component.hasClass('DestinationContainer')).toBe(true)
})