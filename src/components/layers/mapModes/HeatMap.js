import React, { Component } from 'react'
import VenueInfoBox from '../elements/VenueInfoBox'
import Player from '../elements/Player'
import { toInt } from '../../../helpers/misc'
import Heatmap from '../elements/Heatmap'

export default class HeatMap extends Component {
  render () {
    return (
      <div>
        <Heatmap
          {...this.props}
          users={this.props.users}
          rotation={this.props.rotation}
          perspectiveRotation={this.props.perspectiveRotation}
          scale={this.props.scale}
          coordsFactor={this.props.coordsFactor}
        />
        {this.props.users
          .filter(user => toInt(user.deck) === toInt(this.props.deck))
          .map((user, index) =>
            <Player
              {...this.props}
              key={index}
              instance={user}
              rotation={this.props.rotation}
              perspectiveRotation={this.props.perspectiveRotation}
              scale={this.props.scale}
              coordsFactor={this.props.coordsFactor}
            />
          )}
        {this.props.venues.map(venue =>
          <VenueInfoBox
            {...this.props}
            key={venue.id}
            instance={venue}
            rotation={this.props.rotation}
            perspectiveRotation={this.props.perspectiveRotation}
            scale={this.props.scale}
            coordsFactor={this.props.coordsFactor}
          />
        )}
      </div>
    )
  }
}
