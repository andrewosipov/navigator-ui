import React from 'react'
import { shallow } from 'enzyme'

import App from './App'


test('App', () => {
  let component = shallow(<App />)
  expect(component.text()).toContain('Home')

  component = shallow(<App page='catalog' />)
  expect(component.text()).toContain('Catalog')

  component = shallow(<App page='position' />)
  expect(component.text()).toContain('Directions')

  component = shallow(<App page='navigation' />)
  expect(component.text()).toContain('Navigation')

  component = shallow(<App page='navigation-overview' />)
  expect(component.text()).toContain('NavigationOverview')

  component = shallow(<App page='search' />)
  expect(component.text()).toContain('Search')
});
