import React from 'react'
import { shallow } from 'enzyme'

import data from '../helpers/tests/data'
import { ORIENTATION_PORTRAIT, NORMAL_MAP_MODE } from '../constants'
import NavBottom from './NavBottom'

test('NavBottom', () => {
  let page = ''
  const changePage = (p) => {
    page = p
  }

  let activeModal = ''
  const showModal = (m) => {
    activeModal = m
  }

  let target = {}
  const selectTarget = (t) => {
    target = t
  }

  let embarkationFound = false
  const findEmbarkation = () => {
    embarkationFound = true
  }

  const component = shallow(
    <NavBottom
      user={data.user}
      changePage={changePage}
      venues={data.venues}
      selectTarget={selectTarget}
      orientation={ORIENTATION_PORTRAIT}
      showModal={showModal}
      crewOnly={false}
      theme="light"
      findEmbarkation={findEmbarkation}
      mapSubModes={[]}
      users={[]}
      mapModel={NORMAL_MAP_MODE}
      showNavBottom
    />
  )
  expect(component.text()).toContain('My Party')
  expect(component.text()).toContain('My Cabin')

  component.find('#search-link').simulate('click')
  expect(page).toBe('search')

  component.find('#nav-bottom__my-cabin-button').simulate('click')
  expect(page).toBe('position')
  expect(target.id).toBe(data.user.cabin)

  component.find('#nav-bottom__disembarkations-button').simulate('click')
  expect(embarkationFound).toBe(true)

  component.find('#nav-bottom__wc-button').simulate('click')
  expect(activeModal).toBe('RESTROOMS')

  component.find('#nav-bottom__my-party-button').simulate('click')
  expect(activeModal).toBe('MY_PARTY')
})
