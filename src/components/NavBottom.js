import React, { Component } from 'react'
import { CSSTransition } from 'react-transition-group'
import '../styles/NavBottom.scss'
import classnames from 'classnames'
import { toInt } from '../helpers/misc'
import { FormattedMessage } from 'react-intl'

import {
  ORIENTATION_LANDSCAPE,
  NORMAL_MAP_MODE,
  HEATMAP_MAP_MODE,
  LOST_KIDS_MAP_MODE,
  OCCUPANCY_MAP_SUB_MODE,
  LABELS_MAP_SUB_MODE,
  CREW_MAP_SUB_MODE,
  SAILORS_MAP_SUB_MODE,
  PAGES
} from 'src/constants'

import SVG from './common/SVG'

import settingsSvg from '../../public/images/settings.svg'
import closeSvg from '../../public/images/close.svg'

import navCabinSvg from '../../public/images/nav-cabin.svg'
import navPartySvg from '../../public/images/nav-party.svg'
import searchSvg from '../../public/images/search.svg'
import acceptSvg from '../../public/images/accept.svg'
import { icons } from '../helpers/categorySvgSet'

export default class NavBottom extends Component {
    state = {
      isMapSettingsOpen: false
    }

    mapSettingsHandler = () => {
      this.setState({
        isMapSettingsOpen: !this.state.isMapSettingsOpen
      })
    }

    modeHandler = mode => ev => {
      ev.preventDefault()
      this.mapSettingsHandler()
      this.setState({
        isOccupancy: true,
        isLabels: true,
        isSailors: false,
        isCrew: false
      })
      this.props.changeMapMode(mode)
    }

    getNormalNav () {
      const { changePage, user, venues, selectTarget, showModal, crewOnly, theme, findEmbarkation } = this.props
      return (
        <div className='buttons'>
          <button className='search' onClick={() => changePage(PAGES.SEARCH)} id="search-link">
            <SVG>{searchSvg}</SVG>
            <span>
              <FormattedMessage
                id="Search"
                defaultMessage="Search"
              />
            </span>
          </button>
          <button
            onClick={() => {
              let venue = venues.find(v => v.globalVenueId === user.cabin)
              if (venue) {
                changePage(PAGES.POSITION)
                selectTarget(venue)
              }
            }}
            id="nav-bottom__my-cabin-button"
          >
            <SVG>{navCabinSvg}</SVG>
            <FormattedMessage
              id="My-cabin"
              defaultMessage="My Cabin"
            />
          </button>
          <button onClick={() => showModal('MY_PARTY')} id="nav-bottom__my-party-button">
            <SVG>{navPartySvg}</SVG>
            {crewOnly
              ? <FormattedMessage
                id="My-contacts"
                defaultMessage="My Contacts"
              />
              : <FormattedMessage
                id="My-party"
                defaultMessage="My Party"
              />
            }
          </button>
          <button
            className='tb'
            onClick={() => showModal('RESTROOMS', { selectTarget, changePage, user, crewOnly, theme })}
            id="nav-bottom__wc-button"
          >
            <SVG>{icons[11].public}</SVG>
            <FormattedMessage
              id="Restrooms"
              defaultMessage="Restrooms"
            />
          </button>
          <button className='tb' onClick={() => findEmbarkation()} id="nav-bottom__disembarkations-button">
            <SVG>{icons[3].public}</SVG>
            <FormattedMessage
              id="Embarkations"
              defaultMessage="Embarkations"
            />
          </button>
        </div>
      )
    }

    getHeatmapNav () {
      const isOccupancy = this.props.mapSubModes.includes(OCCUPANCY_MAP_SUB_MODE)
      const isLabels = this.props.mapSubModes.includes(LABELS_MAP_SUB_MODE)
      const isSailors = this.props.mapSubModes.includes(SAILORS_MAP_SUB_MODE)
      const isCrew = this.props.mapSubModes.includes(CREW_MAP_SUB_MODE)
      const users = this.props.users.filter(user => toInt(user.deck) === toInt(this.props.deck.deck))

      return (
        <div className='buttons'>
          <button
            className={classnames({ active: isOccupancy })}
            onClick={() => this.props.changeMapSubMode(OCCUPANCY_MAP_SUB_MODE)}
          >
            <SVG className='icon-container'>{acceptSvg}</SVG>
            <FormattedMessage
              id="Occupancy"
              defaultMessage="Occupancy"
            />
          </button>
          <button
            className={classnames({ active: isLabels })}
            onClick={() => this.props.changeMapSubMode(LABELS_MAP_SUB_MODE)}
          >
            <SVG className='icon-container'>{acceptSvg}</SVG>
            <FormattedMessage
              id="Labels"
              defaultMessage="Labels"
            />
          </button>
          <button
            className={classnames('sailors', { active: isSailors })}
            onClick={() => this.props.changeMapSubMode(SAILORS_MAP_SUB_MODE)}
          >
            <SVG className='icon-container'>{acceptSvg}</SVG>
            <FormattedMessage
              id="Sailors"
              defaultMessage="Sailors"
            />
            <span className='count'>{users.filter(u => !u.isCrew).length}</span>
          </button>
          <button
            className={classnames('crew', { active: isCrew })}
            onClick={() => this.props.changeMapSubMode(CREW_MAP_SUB_MODE)}
          >
            <SVG className='icon-container'>{acceptSvg}</SVG>
            <FormattedMessage
              id="Crew"
              defaultMessage="Crew"
            />
            <span className='count'>{users.reduce((acc, user) => user.isCrew ? ++acc : acc, 0)}</span>
          </button>
        </div>
      )
    }

    getMapSettings () {
      const { mapMode } = this.props
      return (
        <div className='buttons'>
          <button className={classnames({ active: mapMode === NORMAL_MAP_MODE })} onClick={this.modeHandler(NORMAL_MAP_MODE)}>
            <FormattedMessage
              id="Normal"
              defaultMessage="Normal"
            />
          </button>
          <button className={classnames({ active: mapMode === LOST_KIDS_MAP_MODE })} >
            <FormattedMessage
              id="Lost-kids"
              defaultMessage="Lost kids"
            />
          </button>
          <button className={classnames({ active: mapMode === HEATMAP_MAP_MODE })} onClick={this.modeHandler(HEATMAP_MAP_MODE)}>
            <FormattedMessage
              id="Heat-Map"
              defaultMessage="Heat Map"
            />
          </button>
        </div>
      )
    }

    render () {
      const { mapMode, orientation, showNavBottom } = this.props
      const { isMapSettingsOpen } = this.state

      let body = this.getHeatmapNav()
      if (isMapSettingsOpen) {
        body = this.getMapSettings()
      } else {
        if (mapMode === NORMAL_MAP_MODE) {
          body = this.getNormalNav()
        }
        if (mapMode === HEATMAP_MAP_MODE) {
          body = this.getHeatmapNav()
        }
      }

      return (
        <CSSTransition
          in={showNavBottom}
          timeout={350}
          classNames='fade'
          unmountOnExit
        >
          <div
            className={
              classnames(
                'NavBottom',
                mapMode,
                {
                  landscape: orientation === ORIENTATION_LANDSCAPE,
                  'map-settings-open': this.state.isMapSettingsOpen,
                  'map-mode-selected': mapMode !== NORMAL_MAP_MODE
                }
              )
            }
          >
            {this.props.crewOnly &&
              <button className="map-settings-toggler" onClick={this.mapSettingsHandler}>
                <SVG>{isMapSettingsOpen ? closeSvg : settingsSvg}</SVG>
              </button>}
            {body}
          </div>

        </CSSTransition>
      )
    }
}
