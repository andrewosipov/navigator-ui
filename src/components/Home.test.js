import React from 'react'
import { shallow } from 'enzyme'

import Home from './Home'
import { resizeTo } from '../helpers/tests/utils'


test('Home', () => {
    let activeModal = null
    const showModal = (m) => {
        activeModal = m
    }

    const hideModal = () => {
        activeModal = null
    }

    let selectedVenue = null
    const deselectVenue = () => {
        selectedVenue = null
    }

    let component = shallow(
        <Home
            deck={6}
            showModal={showModal}
            deselectVenue={deselectVenue}
            hideModal={hideModal}
            modalType="VENUE_INFO"
        />
    )
    expect(component.find('.deck-container-wrapper').exists()).toBe(true)

    component.find('.share').simulate('click')
    expect(activeModal).toBe('MY_PARTY')

    selectedVenue = 'test'
    component.find('#home__deck-selector').simulate('click')
    expect(activeModal).toBe('SELECT_DECK')
    expect(selectedVenue).toBe(null)

    component.setProps({ directionTo: { id: 15 } })
    expect(activeModal).toBe('VENUE_INFO')

    component.setProps({ directionTo: null })
    expect(activeModal).toBe(null)
})


test('Home screen orientation', () => {
    resizeTo(100, 200)
    let component = shallow(
        <Home />
    )
    expect(component.find('.Home').hasClass('landscape')).toBe(false)

    resizeTo(200, 100)
    component = shallow(
        <Home />
    )
    expect(component.find('.Home').hasClass('landscape')).toBe(true)
})
