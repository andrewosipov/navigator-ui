import Sector28 from './Sector28'
import Sector29 from './Sector29'
import Sector30 from './Sector30'
import Sector382 from './Sector382'
import Sector383 from './Sector383'
import Sector384 from './Sector384'
import Sector385 from './Sector385'
import Sector386 from './Sector386'
import Sector387 from './Sector387'
import Sector388 from './Sector388'
import Sector389 from './Sector389'
import Sector390 from './Sector390'
import Sector391 from './Sector391'
import Sector392 from './Sector392'
import Sector393 from './Sector393'
import Sector394 from './Sector394'
import Sector396 from './Sector396'
import Sector397 from './Sector397'
import Sector398 from './Sector398'
import Sector399 from './Sector399'


export default {
28: Sector28,
29: Sector29,
30: Sector30,
382: Sector382,
383: Sector383,
384: Sector384,
385: Sector385,
386: Sector386,
387: Sector387,
388: Sector388,
389: Sector389,
390: Sector390,
391: Sector391,
392: Sector392,
393: Sector393,
394: Sector394,
396: Sector396,
397: Sector397,
398: Sector398,
399: Sector399,

}
