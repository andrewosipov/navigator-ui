import Sector1 from './Sector1'
import Sector106 from './Sector106'
import Sector15 from './Sector15'
import Sector16 from './Sector16'
import Sector17 from './Sector17'
import Sector18 from './Sector18'
import Sector19 from './Sector19'
import Sector2 from './Sector2'
import Sector20 from './Sector20'
import Sector21 from './Sector21'
import Sector22 from './Sector22'
import Sector23 from './Sector23'
import Sector24 from './Sector24'
import Sector25 from './Sector25'
import Sector3 from './Sector3'
import Sector4 from './Sector4'
import Sector5 from './Sector5'
import Sector7 from './Sector7'


export default {
1: Sector1,
106: Sector106,
15: Sector15,
16: Sector16,
17: Sector17,
18: Sector18,
19: Sector19,
2: Sector2,
20: Sector20,
21: Sector21,
22: Sector22,
23: Sector23,
24: Sector24,
25: Sector25,
3: Sector3,
4: Sector4,
5: Sector5,
7: Sector7,

}
