import { PureComponent } from 'react'
import $ from 'jquery'
import { THEMES } from 'src/constants'

export default class BaseSector extends PureComponent {
  componentDidMount () {
    this.init()
  }

  componentDidUpdate () {
    this.init()
  }

  init () {
    const venueOpacity = this.props.hideVenues || this.props.theme === THEMES.BLISS ? 0 : 0.1
    let s = `#svg-sector-${this.props.sectorId}`
    $(`${s} [id*="venue-area-"]`).attr({ opacity: venueOpacity })
    $(`${s} [id*=-cabins-] *`).css({ fill: '', opacity: '', 'stroke-width': '3px', stroke: '#aaa' })
    this.selectVenue(this.props.selected)
    this.selectVenue(this.props.endVenue)
    if (this.props.user) {
      $(`${s} [id*=-cabins-] #venue-area-${this.props.user.cabin}`).css({
        fill: '#476292',
        opacity: 1
      })
    }

    let stroke = $(`${s} [id*=allowedAreaStroke-1]`)
    let fill = $(`${s} [id*=allowedAreaFill-1]`)
    let cabins = $(`${s} [id*=cabins-1]`)

    if ($(`${s} [id*=allowedAreaFill-2]`).length) {
      if (this.props.user.isCrew) {
        $(`${s} [id*=allowedAreaFill-1]`).hide()
        $(`${s} [id*=allowedAreaStroke-1]`).hide()
        stroke = $(`${s} [id*=allowedAreaStroke-2]`)
        fill = $(`${s} [id*=allowedAreaFill-2]`)
        cabins = $(`${s} [id*=cabins-2]`)
      } else {
        $(`${s} [id*=allowedAreaFill-2]`).hide()
        $(`${s} [id*=allowedAreaStroke-2]`).hide()
        $(`${s} [id*=cabins-2]`).hide()
        $(`${s} .venues-map[id*=venues-2]`).hide()
        stroke = $(`${s} [id*=allowedAreaStroke-1]`)
        fill = $(`${s} [id*=allowedAreaFill-1]`)
        cabins = $(`${s} [id*=cabins-1]`)
      }
    }
    fill.attr('fill', '#57585C').show()
    stroke.attr('stroke-width', 14).attr('stroke', '#979797').show()
    cabins.show()
  }

  getSelectedVenueStyle () {
    const style = { opacity: 1 }

    if (this.props.theme === THEMES.BLISS) {
      style.fill = '#BFF4FF'
    }

    return style
  }

  selectVenue (selected) {
    let s = `#svg-sector-${this.props.sectorId}`
    if (selected) {
      $(`${s} #venue-area-${selected.id}`).attr(this.getSelectedVenueStyle())

      $(`${s} [id*=-cabins-] #venue-area-${selected.id}`).css({
        fill: '#E51930',
        opacity: 1
      })
    }
  }
}
