import React, { Component } from 'react'
import PropTypes from 'prop-types'
import $ from 'jquery'
import classnames from 'classnames'

import Deck from '../Deck'
import Layer3d from './Layer3d'

export default class DirectionsDeck extends Component {
  static propTypes = {
    portals: PropTypes.array
  }

  constructor (props) {
    super(props)
    this.state = {
      svgWidth: 0,
      svgHeight: 0
    }

    this.delayedUpdate = () => {
      // dirty hack to recalculate user and destination position
      this.calcSvgDimensions()
      this.activeUpdateTimer = setTimeout(() => this.calcSvgDimensions(), 250)
      this.activeUpdateTimer = setTimeout(() => this.calcSvgDimensions(), 500)
    }

    if ('onorientationchange' in window) {
      window.addEventListener('orientationchange', this.delayedUpdate, false)
    }

    window.onresize = this.delayedUpdate
  }

  componentWillUnmount () {
    clearTimeout(this.activeUpdateTimer)
    window.onresize = () => {}
    window.removeEventListener('orientationchange', this.delayedUpdate)
  }

  calcSvgDimensions = () => this.setState({
    svgWidth: $(this.svg).width(),
    svgHeight: $(this.svg).height()
  })

  render () {
    return (
      <div className={classnames('directions-deck-container', this.props.className)} style={{ transform: this.props.transform }}>
        {this.state.svgWidth &&
        <Layer3d
          {...this.props}
          svgWidth={this.state.svgWidth}
          svgHeight={this.state.svgHeight}
        />
        }

        <div className="deck">
          <Deck
            className="deck-img"
            onSvgReady={(el) => {
              if (!this.state.svgWidth) {
                this.svg = el
                this.calcSvgDimensions()
              }
            }}
            width={Math.min(this.props.windowSize.width, 660)}
            height={Math.min(this.props.windowSize.height, 850)}
            deck={this.props.deckId}

            user={this.props.user}
            hideUser={!!this.props.from}
            theme={this.props.theme}
            endVenue={this.props.to}
            selectedVenue={this.props.from}
            selectedPaths={this.props.selectedPaths}
            pathStrokeWidth={this.props.pathStrokeWidth || 200}
            pathBorderWidth={this.props.pathBorderWidth || 50}

            getDeckElement={this.getDeckElement}

            pathsAnimation={this.props.pathsAnimation}
            hideVenues={true}
          />
        </div>

        {this.props.children}
      </div>
    )
  }
}
