import React from 'react'
import { mount } from 'enzyme'

import Layer3d from './Layer3d'
import data from '../../helpers/tests/data'

test('Layer3d', () => {
  let deckId = 6
  let component = mount(
    <Layer3d
      svgWidth={667}
      svgHeight={1000}
      user={data.user}
      rotateZ={38}
      deckId={deckId}
      venues={data.venues}
      selectedPaths={data.selectedPaths}
      pathsAnimation={false}
      latestPath={data.selectedPaths[data.selectedPaths.length - 1]}
      from={data.directions.from}
      to={data.directions.to}
      portals={[{ isStair: false, x: 5, y: 5 }]}

      heading={30}
      distanceBetweenDecks={300}
    />
  )

  expect(component.find('.deck-number').text()).toContain(deckId)
  expect(component.find('.Destination').exists()).toBe(true)
  expect(component.find('.lift-here').exists()).toBe(true)
  expect(component.find('.user-cont').exists()).toBe(true)
})
