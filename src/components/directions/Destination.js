import React from 'react'
import cn from 'classnames'
import { get } from 'lodash'
import User from 'src/models/User'
import Venue from 'src/models/Venue'
import 'src/styles/Destination.scss'
import DynamicFormattedMessage from '../common/DynamicFormattedMessage'

const Destination = ({ x, y, children, to, instance, theme, vertex }) => {
  return (
    <div
      className="Destination"
      style={{
        left: `${x - 5}px`,
        top: `${y - 3}px`
      }}
    >
      <div
        className="dest-circle"
        data-test-selector="dest-exit-point"
        data-vertex-id={get(vertex, 'id')}
      />
      <img src="public/images/directions/destination.svg" className="dest-img" />
      {to instanceof User && <img src={to.iconUrl} className="dest-avatar" />}
      <div className={cn('dest-name', `dest-name--theme-${theme}`)}>
        {instance instanceof Venue
          ? <DynamicFormattedMessage id={`venue.${instance.id}`} defaultMessage={children} />
          : children
        }
      </div>
    </div>
  )
}

export default Destination
