import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { FormattedMessage } from 'react-intl'
import { CONNECTION_TYPES, PAGES } from 'src/constants'
import { meterToFoot } from 'src/helpers/misc'

import 'src/styles/Directions.scss'
import Decks from './Decks'
import Target from './Target'
import Venue from 'src/models/Venue'
import User from 'src/models/User'

import SVG from 'src/components/common/SVG'
import backSvg from 'public/images/back.svg'
import locationSvg from 'public/images/location.svg'
import recenterSvg from 'public/images/recenter.svg'
import threeDotsSvg from 'public/images/three-dots.svg'

import Button from 'src/components/common/Button'
import Notify from 'src/components/common/Notify'
import Loader from 'src/components/common/Loader'
import TryingToConnect from 'src/components/common/TryingToConnect'
import NoConnection from 'src/components/common/NoConnection'
import CloseBtn from 'src/components/common/CloseBtn'

class Directions extends Component {
  static propTypes = {
    to: PropTypes.oneOfType([
      PropTypes.instanceOf(Venue),
      PropTypes.instanceOf(User)
    ]),
    avoidStairs: PropTypes.bool.isRequired,
    useImperialSystem: PropTypes.bool.isRequired
  }

  constructor (props) {
    super(props)

    this.state = {
      avoidStairs: props.avoidStairs,
      insideOnly: false,
      showFriendNotify: props.to instanceof User
    }

    this.delayedUpdate = () => {
      // dirty hack to recalculate elevators position
      this.forceUpdate()
      this.activeUpdateTimer = setTimeout(() => this.forceUpdate(), 500)
      this.activeUpdateTimer = setTimeout(() => this.forceUpdate(), 1000)
    }

    if ('onorientationchange' in window) {
      window.addEventListener('orientationchange', this.delayedUpdate, false)
    }

    window.onresize = this.delayedUpdate
  }

  componentWillUnmount () {
    clearTimeout(this.activeUpdateTimer)
    clearTimeout(this.friendNotifyTimer)
    window.onresize = () => {}
    window.removeEventListener('orientationchange', this.delayedUpdate)
  }

  componentDidMount () {
    this.fetchPaths()

    this.friendNotifyTimer = setTimeout(() => this.setState({ showFriendNotify: false }), 3000)
  }

  componentDidUpdate (prevProps) {
    if (prevProps.from !== this.props.from || prevProps.to !== this.props.to) {
      this.fetchPaths()
    }
  }

  toggleStairs = () => {
    this.setState(prevState => ({ avoidStairs: !prevState.avoidStairs }), this.fetchPaths)
  }

  toggleInside = () => {
    this.setState(prevState => ({ insideOnly: !prevState.insideOnly }), this.fetchPaths)
  }

  hideFriendNotify = () => {
    this.setState({ showFriendNotify: false })
  }

  fetchPaths () {
    const crewOnly = this.props.crewOnly || this.props.user.isCrew
    const { avoidStairs, insideOnly } = this.state

    if ((this.props.to instanceof Venue) && !this.props.from) {
      this.props.fetchUserToVenuePath(this.props.user, this.props.to, crewOnly, avoidStairs, insideOnly)
    }

    if ((this.props.to instanceof Venue) && (this.props.from instanceof Venue)) {
      this.props.fetchVenueToVenuePath(this.props.from, this.props.to, crewOnly, avoidStairs, insideOnly)
    }

    if ((this.props.to instanceof User) && !this.props.from) {
      this.props.fetchUserToUserPath(this.props.user, this.props.to, crewOnly, avoidStairs, insideOnly)
    }
  }

  changeFrom () {
    this.props.changeTargetType('from')
    this.props.changePage(PAGES.SEARCH)
  }

  changeTo () {
    this.props.changeTargetType('to')
    this.props.changePage(PAGES.SEARCH)
  }

  removeTarget () {
    this.props.changeTargetType('from')
    this.props.selectTarget(null)
  }

  render () {
    const { selectedPaths, isSelectedPathFetching, connectionType, useImperialSystem } = this.props
    const { avoidStairs, insideOnly, showFriendNotify } = this.state
    const hasPaths = !!selectedPaths.length
    const distanceM = this.props.selectedPaths.reduce((acc, v) => acc + v.distance || 0, 0) / 100
    const distanceFt = meterToFoot(distanceM)

    return (
      <div className={classnames('Directions', { landscape: this.props.windowSize.width > this.props.windowSize.height })}>
        <button
          className='back'
          onClick={() => {
            this.props.eraseSelectedDirections()
            this.props.goBack()
          }}
        >
          <SVG>{backSvg}</SVG>
        </button>
        <div className={classnames('position', { 'v-to-v': this.props.from })}>
          <div className="left">
            {this.props.from
              ? <SVG>{locationSvg}</SVG>
              : <SVG>{recenterSvg}</SVG>
            }
            <SVG className='dots'>{threeDotsSvg}</SVG>
            <SVG>{locationSvg}</SVG>
          </div>
          <div className="right">
            <div className="target-from" onClick={() => this.changeFrom()}>
              <Target
                instance={this.props.from || { name: <FormattedMessage id="my-position" defaultMessage="My position" /> }}
                btn={this.props.from && <div className="remove-target" onClick={(e) => {
                  if (e) {
                    e.stopPropagation()
                  }
                  this.removeTarget()
                }}></div>}
                theme={this.props.theme}
              />
            </div>

            <div className="target-to" onClick={() => this.changeTo()}>
              <Target instance={this.props.to || {}} theme={this.props.theme} />
            </div>
          </div>

          <div className="nav-options">
            <Button isActive={avoidStairs} onClick={this.toggleStairs}>
              <FormattedMessage id="Avoid-stairs" defaultMessage="Avoid stairs" />
            </Button>
            <Button isActive={insideOnly} onClick={this.toggleInside}>
              <FormattedMessage id="Inside-only" defaultMessage="Inside Only" />
            </Button>
          </div>
        </div>

        <div className="nav-bottom">
          {/* <div className="rotate">
                        <SVG>{rotateSvg}</SVG>
                    </div> */}

          <div className="info">
            <div className="time">
              <FormattedMessage
                id="time-min"
                defaultMessage="{time} min."
                values={{ time: Math.round(selectedPaths.reduce((acc, v) => acc + v.time || 0, 0)) }}
              />
            </div>
            <div className="distance">
              { !useImperialSystem &&
                <FormattedMessage
                  id="distance-m"
                  defaultMessage="{distance} m"
                  values={{ distance: Math.round(distanceM) }}
                />
              }
              { useImperialSystem &&
                <FormattedMessage
                  id="distance-ft"
                  defaultMessage="{distance} ft"
                  values={{ distance: Math.round(distanceFt) }}
                />
              }
            </div>
          </div>

          {!this.props.from && hasPaths &&
            <button className="start-navigation-button" onClick={() => this.props.changePage(PAGES.NAVIGATION)}>
              <img className='imgr' src="/public/images/start-nav.svg" alt=""/>
              <FormattedMessage id="start-navigation" defaultMessage="start navigation" />
            </button>
          }
        </div>

        <Decks
          venues={this.props.venues}
          users={this.props.users}
          selectedPaths={selectedPaths}
          from={this.props.from}
          to={this.props.to}
          user={this.props.user}
          theme={this.props.theme}
          heading={this.props.heading}
          pathsAnimation
          windowSize={this.props.windowSize}
        />

        {isSelectedPathFetching && <Loader withBackground theme={this.props.theme} />}

        {!isSelectedPathFetching && !hasPaths &&
          <Notify withBackground isFullScreen theme={this.props.theme}>
            <FormattedMessage id="No-available-routes" defaultMessage="No routes available with selected filters" />
          </Notify>
        }

        {showFriendNotify &&
          <Notify className="nav-friend-notify" withBackground theme={this.props.theme}>
            <FormattedMessage id="Routing-to-Friend" defaultMessage="You are routing to a friend in motion" />
            <CloseBtn
              className="nav-friend-notify__close-btn"
              onClick={this.hideFriendNotify}
              theme={this.props.theme}
            />
          </Notify>
        }

        {connectionType === CONNECTION_TYPES.UNKNOWN &&
          <TryingToConnect
            isAbsolute
            withBackground
            isFullScreen
            theme={this.props.theme}
          />
        }
        {connectionType === CONNECTION_TYPES.NONE &&
          <NoConnection
            isAbsolute
            withBackground
            isFullScreen
            theme={this.props.theme}
          />
        }
      </div>
    )
  }
}

export default Directions
