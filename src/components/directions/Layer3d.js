import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import Elevator from './Elevator'
import Stair from './Stair'
import Destination from './Destination'
import User from 'src/containers/directions/User'

import { SECTOR_ID_TO_DECKS, WIDTH, HEIGHT } from '../../constants'
import { toInt } from '../../helpers/misc'

export default class Layer3d extends PureComponent {
  static propTypes = {
    portals: PropTypes.array
  }

  transformCoordinates (x, y) {
    let svgWidth = this.props.svgWidth
    let svgHeight = this.props.svgHeight
    let factor = WIDTH / svgWidth
    let cLeft = svgWidth / 2
    let cTop = svgHeight / 2
    let newX = cLeft + 1.5 * (HEIGHT / 2 - y) / factor
    let newY = cTop + 1 * (WIDTH / 2 - x) / factor
    return { x: newX, y: newY }
  }

  getDeckNumberMargin () {
    const rotateZ = this.props.rotateZ
    let deckNumberMargin = 0
    if (rotateZ >= -15 && rotateZ < 2) {
      deckNumberMargin = -40
    }
    if (rotateZ >= -2 && rotateZ < 4) {
      deckNumberMargin = -60
    }
    if (rotateZ >= 4 && rotateZ < 17) {
      deckNumberMargin = 45
    }
    if (rotateZ >= 17 && rotateZ <= 30) {
      deckNumberMargin = 20
    }
    return deckNumberMargin
  }

  render () {
    let userComponent = null
    if (!this.props.from && toInt(this.props.user.deck) === toInt(this.props.deckId)) {
      userComponent = <User theme={this.props.theme} heading={this.props.heading} {...this.transformCoordinates(this.props.user.x, this.props.user.y)} />
    }

    return (
      <div className="layer3d">
        <span className="deck-number" style={{ marginLeft: this.getDeckNumberMargin(), top: toInt((this.props.svgHeight - this.props.svgWidth) / 2) }}>
          <span className="deck-number-value">{toInt(this.props.deckId)}</span>
        </span>
        {this.props.portals.map((data, index) => (
          data.deckId === this.props.deckId ? (
            <span key={index}>
              {data.isStair
                ? <Stair {...data} {...this.transformCoordinates(data.x, data.y)} height={this.props.distanceBetweenDecks} />
                : <Elevator {...data} {...this.transformCoordinates(data.x, data.y)} height={this.props.distanceBetweenDecks} />
              }
            </span>
          ) : null
        ))}

        {toInt(SECTOR_ID_TO_DECKS[this.props.to.sectorId]) === toInt(this.props.deckId) && this.props.latestPath &&
          <Destination
            {...this.transformCoordinates(this.props.latestPath.vertexes.end.x, this.props.latestPath.vertexes.end.y)}
            to={this.props.to}
            instance={this.props.to}
            theme={this.props.theme}
            vertex={this.props.latestPath.vertexes.end}
          >
            {this.props.to.name}
          </Destination>
        }

        {this.props.from && this.props.selectedPaths[0] && toInt(SECTOR_ID_TO_DECKS[this.props.from.sectorId]) === toInt(this.props.deckId) &&
          <Destination
            {...this.transformCoordinates(this.props.selectedPaths[0].vertexes.begin.x, this.props.selectedPaths[0].vertexes.begin.y)}
            to={this.props.to}
            instance={this.props.from}
            theme={this.props.theme}
            vertex={this.props.selectedPaths[0].vertexes.begin}
          >
            {this.props.from.name}
          </Destination>
        }

        {userComponent}
      </div>
    )
  }
}
