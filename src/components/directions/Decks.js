import React, { Component } from 'react'
import Hammer from 'hammerjs'
import { uniqBy, get } from 'lodash'

import Deck from './Deck'

import {
  SECTOR_ID_TO_DECKS,
  DEFAULT_PERSPECTIVE_ROTATION
} from '../../constants'
import { toInt } from '../../helpers/misc'

export default class Decks extends Component {
  constructor (props) {
    super(props)
    this.state = {
      rotation: -36
    }
    this.timeouts = []
  }

  getDeckIds () {
    let deckIds = []
    let pathsCount = {}
    this.props.selectedPaths.filter(({ distance }) => distance).forEach((p, i) => {
      let deckId = toInt(SECTOR_ID_TO_DECKS[p.vertexes.begin.sectorId])
      let endDeckId = toInt(SECTOR_ID_TO_DECKS[p.vertexes.end.sectorId])
      let isLatestPath = i === this.props.selectedPaths.length - 1
      if (!pathsCount[deckId]) {
        pathsCount[deckId] = 0
      }
      pathsCount[deckId] += 1
      if ((
        deckIds.length === 0 ||
                pathsCount[deckId] > 1 ||
                isLatestPath
      ) && !deckIds.includes(deckId)) {
        deckIds.push(deckId)
      }

      if (isLatestPath && !deckIds.includes(endDeckId)) {
        deckIds.push(endDeckId)
      }
    })
    deckIds.sort((a, b) => b - a)

    if (deckIds.length > 2) {
      deckIds = [deckIds[0], deckIds[1], deckIds[deckIds.length - 1]]
    }

    if (!deckIds.length) {
      deckIds = [toInt(SECTOR_ID_TO_DECKS[this.props.to.sectorId])]
    }

    return deckIds
  }

  rotate (e) {
    this.setState({
      rotation: Math.max(Math.min(this.state.rotation - e.deltaX / 30, 35), -35)
    })
  }

  addEventListeners (el) {
    if (el && !this.hammertime) {
      this.hammertime = new Hammer(el)
      this.hammertime.on('panmove', (e) => this.rotate(e))
    }
  }

  componentWillUnmount () {
    this.hammertime.destroy()
  }

  getScreenHeightFactor () {
    return Math.min(Math.max(this.props.windowSize.height - 700, 0), 200)
  }

  getPortals = () => {
    const { selectedPaths, venues } = this.props
    const vertexList = selectedPaths.reduce(
      (vertexList, path) => {
        vertexList.push(path.vertexes.begin, path.vertexes.end)
        return vertexList
      },
      []
    )
    const borderVertexList = vertexList.filter((vertex, index, array) => {
      const nextVertex = array[index + 1]
      const prevVertex = array[index - 1]
      return (!!nextVertex && (vertex.sectorId !== nextVertex.sectorId)) ||
        (!!prevVertex && (vertex.sectorId !== prevVertex.sectorId))
    })
    const uniqBorderVertexList = uniqBy(borderVertexList, vertex => vertex.id)

    const portals = uniqBorderVertexList.map((vertex, index, array) => {
      const currentDeckId = parseInt(SECTOR_ID_TO_DECKS[vertex.sectorId])
      const currentVenue = venues.find(venue => venue.exitPoints.includes(vertex.id))
      const nextVertex = array[index + 1]
      const prevVertex = array[index - 1]
      const nextDeckId = parseInt(SECTOR_ID_TO_DECKS[get(nextVertex, 'sectorId')])
      const prevDeckId = parseInt(SECTOR_ID_TO_DECKS[get(prevVertex, 'sectorId')])
      let down = false
      let withArrows = false

      if (nextVertex && nextDeckId !== currentDeckId) {
        down = nextDeckId < currentDeckId
        withArrows = nextDeckId > currentDeckId
      } else if (prevVertex && prevDeckId !== currentDeckId) {
        down = prevDeckId > currentDeckId
        withArrows = prevDeckId > currentDeckId
      }

      return {
        x: vertex.x,
        y: vertex.y,
        deckId: currentDeckId,
        showText: false,
        isStair: currentVenue ? currentVenue.isStairs() : false,
        down,
        withoutArrows: !withArrows
      }
    })

    return portals
  }

  getDeckTransform (index, decksCount) {
    let screenHeightFactor = this.getScreenHeightFactor()
    let margin = decksCount === 3 ? 30 : 50
    if (screenHeightFactor >= 200) {
      margin = 0
    }
    let z = (
      40 * (decksCount - 1) -
            index * (190 + screenHeightFactor) / decksCount +
            (this.props.windowSize.width > this.props.windowSize.height ? 40 : 0) +
            (decksCount > 1 ? screenHeightFactor / 4 + screenHeightFactor / (decksCount * 8) : 0) -
            margin
    )
    let rotateX = DEFAULT_PERSPECTIVE_ROTATION
    if (screenHeightFactor) {
      if (decksCount === 2) {
        rotateX -= 5 * (decksCount - index - 1)
      } else if (decksCount === 3 && index === 0) {
        rotateX -= 5
      }
    }
    if (index === 2 && decksCount === 3) {
      rotateX += 4
    }

    return [
      `rotateX(${rotateX}deg)`,
      `rotateY(0deg)`,
      `rotateZ(${this.state.rotation}deg)`,
      `translate3d(0px, -20px, ${z}px)`
    ].join(' ')
  }

  render () {
    let deckIds = this.getDeckIds()
    let distanceBetweenDecks = (190 + this.getScreenHeightFactor()) / deckIds.length
    return (
      <div
        className="_3d-cont"
        style={{ width: `${this.props.windowSize.width}px`, height: `${this.props.windowSize.height}px` }}
      >
        <div
          className={`decks-view-cont decks-count-${deckIds.length}`}
          ref={(el) => this.addEventListeners(el)}
        >
          {deckIds.map((deckId, index) => (
            <Deck
              key={deckId}
              className={`deck-${Math.max(deckIds.length, 2) - index - 1}`}
              transform={this.getDeckTransform(index, deckIds.length)}
              rotateZ={this.state.rotation}
              deckId={deckId}
              venues={this.props.venues}
              selectedPaths={this.props.selectedPaths.filter(p => deckIds.includes(toInt(SECTOR_ID_TO_DECKS[p.vertexes.begin.sectorId])))}
              pathsAnimation={this.props.pathsAnimation}
              latestPath={this.props.selectedPaths[this.props.selectedPaths.length - 1]}
              from={this.props.from}
              to={this.props.to}
              user={this.props.user}
              theme={this.props.theme}
              heading={this.props.heading}
              distanceBetweenDecks={distanceBetweenDecks}
              windowSize={this.props.windowSize}
              pathStrokeWidth={this.props.pathStrokeWidth}
              pathBorderWidth={this.props.pathStrokeWidth}
              portals={this.getPortals()}
            >
              {deckIds.length === index &&
                                <div className="lift-here-flip">
                                  <div className="lift-tunnel rot90 flipped opacitylow"></div>
                                  <div className="lift-tunnel opacitylow2"></div>
                                </div>
              }
            </Deck>
          ))}
        </div>
      </div>
    )
  }
}
