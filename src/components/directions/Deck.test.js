import React from 'react'
import { shallow } from 'enzyme'

import Deck from './Deck'
import Layer3d from './Layer3d'
import StaticDeck from '../Deck'
import data from '../../helpers/tests/data'

test('Deck', () => {
  let deckId = 6
  let component = shallow(
    <Deck
      user={data.user}
      venues={data.venues}
      selectedPaths={data.selectedPaths}
      from={data.directions.from}
      to={data.directions.to}
      heading={30}

      rotateZ={38}
      deckId={deckId}
      pathsAnimation={false}
      latestPath={data.selectedPaths[data.selectedPaths.length - 1]}

      distanceBetweenDecks={300}
      windowSize={{ width: 400, height: 400 }}
    />
  )
  expect(component.find(StaticDeck).exists()).toBe(true)
  expect(component.find(Layer3d).exists()).toBe(false)

  component.setState({ svgWidth: 300, svgHeight: 300 })
  expect(component.find(Layer3d).exists()).toBe(true)
})
