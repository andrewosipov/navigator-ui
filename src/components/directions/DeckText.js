import React from 'react'

export default ({x, y, children, textAlign = 'left'}) => (
    <div
        style={{
            opacity: '1',
            left: `${x}px`,
            top: `${y}px`,
            textAlign
        }}
        className="deck-text"
    >{children}</div>
)