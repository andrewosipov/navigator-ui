import React from 'react'
import { mount } from 'enzyme'

import Stair from './Stair'
import data from '../../helpers/tests/data'

test('Stair', () => {
    let component = mount(
        <Stair
            x={10}
            y={0}
        />
    )

    expect(component.find('.lift-here').exists()).toBe(true)
    expect(component.find('.Stair').exists()).toBe(true)
    expect(component.find('.down').exists()).toBe(false)
    expect(component.find('.triangles').exists()).toBe(true)

    component = mount(
        <Stair
            x={10}
            y={0}
            down
            withoutArrows
        />
    )

    expect(component.find('.lift-here').exists()).toBe(true)
    expect(component.find('.Stair').exists()).toBe(true)
    expect(component.find('.down').exists()).toBe(true)
    expect(component.find('.triangles').exists()).toBe(false)
})