import React, { Component } from 'react'
import classnames from 'classnames'
import Elevator from '../common/Elevator'


export default ({ x, y, down = false, withoutArrows = false, height = undefined }) => (
    <div
        className="lift-here"
        style={{
            top: `${y - 7}px`,
            left: `${x - 12.08}px`,
        }}
    >
        <Elevator down={down} withoutArrows={withoutArrows} height={withoutArrows ? undefined : height} />
    </div>
)