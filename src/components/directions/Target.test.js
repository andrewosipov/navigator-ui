/* eslint-env jest */
import React from 'react'
import { mount } from 'enzyme'

import Target from './Target'
import data from 'src/helpers/tests/data'

test('Target', () => {
  let component = mount(
    <Target
      instance={data.venues[0]}
      btn={null}
    />
  )
  console.log(component.debug())
  expect(component.find('.descr-wrapper').exists()).toBe(true)
  expect(component.find('.name').html()).toContain(data.venues[0].name)

  let btnTitle = 'TEST BUTTON 123'
  component = mount(
    <Target
      instance={data.venues[0]}
      btn={<button className="test-button">{btnTitle}</button>}
    />
  )
  expect(component.find('.test-button').text()).toContain(btnTitle)
})
