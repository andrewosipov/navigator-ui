import React from 'react'
import { shallow } from 'enzyme'

import Destination from './Destination'
import data from '../../helpers/tests/data'

test('Destination', () => {
    let c = "TEST TEXT 123"
    let component = shallow(
        <Destination
            x={10}
            y={0}
        >{c}</Destination>
    )
    expect(component.find('.dest-name').text()).toBe(c)
    expect(component.hasClass('Destination')).toBe(true)
})