import React from 'react'
import { shallow } from 'enzyme'
import { Provider } from 'react-redux'

import Directions from './Directions'
import Decks from './Decks'
import Target from './Target'
import data from '../../helpers/tests/data'
import { store } from '../../store'

test('Directions', () => {
  let status = {}
  let component = shallow(
    <Provider store={store}>
      <Directions
        user={data.user}
        venues={data.venues}
        selectedPaths={data.selectedPaths}
        from={data.directions.from}
        to={data.directions.to}
        heading={30}
        windowSize={{ width: 200, height: 400 }}

        changePage={(page) => status.page = page}
        goBack={() => status.back = true}
        changeTargetType={(targetType) => status.targetType = targetType}
        selectTarget={(target) => status.target = target}
        eraseSelectedDirections={() => status.target = null}
        fetchUserToVenuePath={() => status.fetchedUtoV = true}
        fetchVenueToVenuePath={() => status.fetchedVtoV = true}
      />
    </Provider>
  ).find(Directions).dive()
  expect(component.hasClass('Directions')).toBe(true)
  expect(component.hasClass('landscape')).toBe(false)

  component.update()
  component.setProps({ windowSize: { width: 400, height: 200 } })
  expect(component.hasClass('landscape')).toBe(true)

  expect(status.fetchedUtoV).toBe(true)
  component.find('.back').simulate('click')
  expect(status.back).toBe(true)

  component.find('.target-to').simulate('click')
  expect(status.page).toBe('search')
  expect(status.targetType).toBe('to')

  status.page = null
  component.find('.target-from').simulate('click')
  expect(status.page).toBe('search')
  expect(status.targetType).toBe('from')

  expect(component.find(Decks).exists()).toBe(true)

  component.find('.start-navigation-button').simulate('click')
  expect(status.page).toBe('navigation')

  status.fetchedUtoV = false
  status.fetchedVtoV = false
  component.setProps({ to: data.venues[0] })
  expect(status.fetchedUtoV).toBe(true)
  expect(status.fetchedVtoV).toBe(false)

  status.fetchedUtoV = false
  status.fetchedVtoV = false
  component.setProps({ to: data.venues[0], from: data.venues[1] })
  expect(status.fetchedUtoV).toBe(false)
  expect(status.fetchedVtoV).toBe(true)

  component.find('.target-from').find(Target).dive().find('.remove-target').simulate('click')
  expect(status.target).toBe(null)
})
