import React from 'react'
import { shallow } from 'enzyme'

import Decks from './Decks'
import Deck from './Deck'
import data from '../../helpers/tests/data'

test('Decks', () => {
  let component = shallow(
    <Decks
      user={data.user}
      venues={data.venues}
      selectedPaths={data.selectedPaths}
      from={data.directions.from}
      to={data.directions.to}
      heading={30}
      windowSize={{ width: 400, height: 400 }}
    />
  )
  expect(component.find(Deck).exists()).toBe(true)
  expect(component.find(Deck)).toHaveLength(2)
})
