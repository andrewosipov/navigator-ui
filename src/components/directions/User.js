import React from 'react'
import styled from 'styled-components'
import { CONNECTION_TYPES, THEMES } from 'src/constants'

const User = ({ x, y, heading, connectionType, theme }) => (
  <div
    className="user-cont"
    style={{
      left: `${x - 10.04}px`,
      top: `${y - 10.04}px`
    }}
  >
    {theme !== THEMES.BLISS &&
      <img style={{ transform: `rotate(${heading}deg) rotate(-45deg)` }} src="/public/images/user-white-arrow.svg" alt=""/>
    }
    {theme === THEMES.BLISS &&
      <UserDot noSignal={connectionType === CONNECTION_TYPES.NONE || connectionType === CONNECTION_TYPES.UNKNOWN} />
    }
  </div>
)

export default User

const UserDot = styled.div`
  box-sizing: border-box;
  position: relative;
  width: 10px;
  height: 10px;
  margin: 2px;
  border-radius: 50%;
  border: 1px solid white;
  background-color: #4285F4;

  &:before {
    box-sizing: border-box;
    content: '';
    position: absolute;
    top: -7px;
    left: -7px;
    bottom: -7px;
    right: -7px;
    border-radius: 50%;
    background-color: ${props => props.noSignal ? 'rgba(70, 70, 70, 0.3)' : 'rgba(66, 133, 244, 0.3)'};
  }
`
