import React from 'react'
import { FormattedMessage, injectIntl } from 'react-intl'
import { SECTOR_ID_TO_DECKS, THEMES } from 'src/constants'
import Venue from 'src/models/Venue'
import DynamicFormattedMessage from 'src/components/common/DynamicFormattedMessage'

const Target = ({ instance, btn, theme, intl }) => (
  <div className="descr-wrapper">
    <div className="descr">
      <div className="name es">{
        instance instanceof Venue
          ? <DynamicFormattedMessage
            id={`venue.${instance.id}`}
            defaultMessage={instance.name}
          />
          : instance.name
      }</div>
      {instance.sectorId &&
        <div className="info">
          <div className='ttu'>
            <FormattedMessage
              id="Deck-N"
              defaultMessage="Deck {number}"
              values={{ number: SECTOR_ID_TO_DECKS[instance.sectorId] }}
            />
          </div>
          {theme !== THEMES.BLISS && !instance.isService() && !instance.isCabin() &&
            <span className='middot'>&middot;</span>
          }
          {theme !== THEMES.BLISS && !instance.isService() && !instance.isCabin() &&
            <div>
              <FormattedMessage
                id="closes-at"
                defaultMessage="Closes at: {time}"
                values={{ time: intl && intl.formatTime((new Date()).setHours(18, 0, 0)) }}
              />
            </div>
          }
        </div>
      }
    </div>
    {btn}
  </div>
)

export default injectIntl(Target)
