import React from 'react'
import { mount } from 'enzyme'

import Elevator from './Elevator'
import data from '../../helpers/tests/data'

test('Elevator', () => {
    let component = mount(
        <Elevator
            x={10}
            y={0}
        />
    )

    expect(component.find('.lift-here').exists()).toBe(true)
    expect(component.find('.Elevator').exists()).toBe(true)
    expect(component.find('.down').exists()).toBe(false)
    expect(component.find('.triangles').exists()).toBe(true)

    component = mount(
        <Elevator
            x={10}
            y={0}
            down
            withoutArrows
        />
    )

    expect(component.find('.lift-here').exists()).toBe(true)
    expect(component.find('.Elevator').exists()).toBe(true)
    expect(component.find('.down').exists()).toBe(true)
    expect(component.find('.triangles').exists()).toBe(false)
})