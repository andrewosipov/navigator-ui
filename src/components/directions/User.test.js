import React from 'react'
import { shallow } from 'enzyme'

import User from './User'
import data from '../../helpers/tests/data'

test('User', () => {
    let component = shallow(
        <User
            x={10}
            y={0}
            heading={30}
        />
    )
    expect(component.hasClass('user-cont')).toBe(true)
    expect(component.find('img').exists()).toBe(true)
})