import React from 'react'
import { shallow } from 'enzyme'

import DeckText from './DeckText'

test('DeckText', () => {
    let c = "TEST TEXT 12312"
    let component = shallow(
        <DeckText
            x={10}
            y={0}
        >{c}</DeckText>
    )

    expect(component.text()).toBe(c)
    expect(component.hasClass('deck-text')).toBe(true)
})