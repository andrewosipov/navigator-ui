/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'

import SelectDeck from './default'
import data from 'src/helpers/tests/data'

test('SelectDeck', () => {
  let deckChanged = false
  let modalHidden = false
  let component = shallow(
    <SelectDeck
      deck={6}
      categories={data.categories}
      venues={data.venues}
      user={data.user}
      changeDeck={() => deckChanged = true} // eslint-disable-line
      hideModal={() => modalHidden = true} // eslint-disable-line
    />
  )
  expect(component.find('SelectDeck').exists()).toBe(true)
  expect(deckChanged).toBe(false)
  expect(modalHidden).toBe(false)
  component.find('#select-deck-7').simulate('click')
  expect(deckChanged).toBe(true)
  expect(modalHidden).toBe(true)
})
