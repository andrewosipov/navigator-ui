import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { FormattedMessage } from 'react-intl'
import SVG from 'src/components/common/SVG'
import CLoseBtn from 'src/components/common/CloseBtn'

import './2.scss'

import decksSvg from 'public/images/decks-icon.svg'

const SelectDeck = ({ currentDeck, decks, theme, onClose, onSelect }) => (
  <div className="select-deck">
    <div className="select-deck__header">
      <SVG className="select-deck__header__deck-icon">{decksSvg}</SVG>
      <FormattedMessage id="Decks" defaultMessage="Decks" />
      <CLoseBtn
        className="select-deck__header__close-btn"
        onClick={onClose}
        theme={theme}
      />
    </div>
    <div className="select-deck__body">
      {decks.map(deck => (
        <button
          className={cn('select-deck__body__deck', { current: deck === currentDeck })}
          onClick={() => onSelect(deck)}
          key={deck}
        >
          <FormattedMessage id="Deck" defaultMessage="Deck" /> <span className="number">{deck}</span>
        </button>
      ))}
    </div>
  </div>
)

SelectDeck.propTypes = {
  currentDeck: PropTypes.number.isRequired,
  decks: PropTypes.arrayOf(PropTypes.number).isRequired,
  theme: PropTypes.string,
  onClose: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired
}

export default SelectDeck
