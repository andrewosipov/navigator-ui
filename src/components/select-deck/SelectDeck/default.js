import $ from 'jquery'
import React, { Component } from 'react'
import { FormattedMessage, injectIntl } from 'react-intl'
import classnames from 'classnames'

import 'src/styles/SelectDeck.scss'
import { DECK_TO_SECTORS_ID } from 'src/constants'
import { toInt } from 'src/helpers/misc'
import Category from 'src/models/Category'
import SVG from 'src/components/common/SVG'
import searchNearMeSvg from 'public/images/search-near-me.svg'
import closeSvg from 'public/images/close.svg'
import { tabs } from 'src/helpers/categorySvgSet'

class SelectDeck extends Component {
  componentDidMount () {
    const sd = $('.SelectDeck .content')
    let offsetTop = 0
    if ($(`#select-deck-${this.props.deck}`).offset()) {
      offsetTop = $(`#select-deck-${this.props.deck}`).offset().top
    }
    if (sd.offset()) {
      offsetTop -= sd.offset().top
    }
    sd.animate({
      scrollTop: sd.scrollTop() + offsetTop
    })
  }

  getVenuesForDeck (deckId) {
    let sectorId = toInt(DECK_TO_SECTORS_ID[deckId])
    let serviceCategories = this.props.categories.filter(c => c.isService).map(c => c.id)
    let venues = this.props.venues.filter(
      v => toInt(v.sectorId) === sectorId &&
            !serviceCategories.includes(v.getDefaultCategoryId())
    )

    if (venues.length < 5) {
      let cabins = this.getCabins(deckId).sort((a, b) => toInt(a.name.replace('Cabin ', '')) - toInt(b.name.replace('Cabin ', '')))

      if (cabins.length > 1) {
        venues.push({
          name: `${this.props.intl.formatMessage({
            id: 'Cabins',
            defaultMessage: 'Cabins'
          })} ${cabins[0].name.replace('Cabin ', '')} - ${cabins[cabins.length - 1].name.replace('Cabin ', '')}`,
          categories: [{ id: Category.CABIN }]
        })
      }
    }

    return venues
  }

  getCabins (deckId) {
    let sectorId = toInt(DECK_TO_SECTORS_ID[deckId])
    return this.props.venues.filter(
      v => v.isCabin() && toInt(v.sectorId) === sectorId && toInt(v.name.replace('Cabin ', ''))
    )
  }

  getCategoriesForDeck (deckId) {
    let venues = this.getVenuesForDeck(deckId)
    let categories = {}
    venues.forEach(v => {
      v.categories.forEach(c => {
        if (!categories[c.id]) {
          categories[c.id] = 0
        }
        categories[c.id] += 1
      })
    })
    return Object.keys(categories).sort((a, b) => categories[b.id] - categories[a.id])
  }

  render () {
    let deckCategories = {}
    Object.keys(DECK_TO_SECTORS_ID).forEach(deck => {
      deckCategories[deck] = this.getCategoriesForDeck(deck).filter(
        c => tabs[c] && (tabs[c].public || tabs[c].crew)
      )
    })
    return (
      <div className='SelectDeck'>
        <div className="head">
          <SVG className='near'>{searchNearMeSvg}</SVG>
          <FormattedMessage id="You-are-on-deck-N" defaultMessage="You are on Deck {number}" values={{ number: this.props.user.deck }} />
          <button className='close' onClick={() => this.props.hideModal()}>
            <SVG>{closeSvg}</SVG>
          </button>
        </div>
        <div className="content">
          {Object.keys(DECK_TO_SECTORS_ID).filter(
            d => deckCategories[d] && deckCategories[d].length
          ).sort((a, b) => b - a).map((deck, index) =>
            <div
              key={index}
              id={`select-deck-${deck}`}
              className={classnames('item', { active: toInt(deck) === toInt(this.props.deck) })}
              onClick={() => {
                this.props.changeDeck(deck, this.props.debug)
                this.props.hideModal()
              }}
            >
              <div className="active-icon">
                {toInt(deck) === toInt(this.props.user.deck) &&
                                    <SVG>{searchNearMeSvg}</SVG>
                }
              </div>
              <div className="number">
                {deck}
              </div>
              <div className="right">
                <div className="venues">
                  {this.getVenuesForDeck(deck).map(v => this.props.intl.formatMessage({ id: `venue.${v.id}`, defaultMessage: v.name })).slice(0, 4).join(', ')}
                </div>
                <div className="icons">
                  {deckCategories[deck].map(c =>
                    <SVG key={c}>{
                      (this.props.crewOnly && ((this.props.categories.find(cat => cat.id === c && cat.crewOnly) && tabs[c].crew) || tabs[c].public)) ||
                                            tabs[c].public
                    }</SVG>
                  ).slice(0, 4)}
                </div>
              </div>

            </div>
          )}

        </div>
      </div>
    )
  }
}

export default injectIntl(SelectDeck)
