/* globals APP_MODE */
import React, { Component } from 'react'
import classNames from 'classnames'

import shellInitialize from '../shell/shell_integration'

import 'src/styles/App.scss'

import { PAGES } from 'src/constants'
import Home from 'src/containers/Home'
import HomeTV from 'src/containers/tv/Home'
import Directions from 'src/containers/Directions'
import Catalog from 'src/containers/Catalog'
import Navigation from 'src/containers/Navigation'
import NavigationOverview from 'src/containers/NavigationOverview'
import Search from 'src/containers/Search'
import Loader from 'src/components/common/Loader'

import { parseParams } from 'src/helpers/misc'

export default class App extends Component {
  constructor (props) {
    super(props)

    try {
      if (window.location.href.split('?')[1]) {
        const query = parseParams(window.location.href.split('?')[1])
        if (query && query.mode && query.mode === 'framework') {
          shellInitialize()
        }
      }
    } catch (e) {
      console.log(e)
    }
  }

  componentWillUnmount () {
    try {
      window.provideInfoToShell('stopSendingCompassHeading')
    } catch (e) {
      console.log(e)
    }
  }

  render () {
    const { page, theme, ready } = this.props
    let component
    /* eslint-disable */
    if (APP_MODE !== 'tv' && !ready) {
      return <Loader withBackground theme={theme} />
    }

    switch (page) {
      case PAGES.POSITION:
        component = <Directions />
        break
      case PAGES.CATALOG:
        component = <Catalog />
        break
      case PAGES.NAVIGATION:
        component = <Navigation />
        break
      case PAGES.NAVIGATION_OVERVIEW:
        component = <NavigationOverview />
        break
      case PAGES.SEARCH:
        component = <Search />
        break
      case PAGES.TV:
        component = <HomeTV />
        break
      default:
        component = <Home />
    }
    /* eslint-disable */
    if (APP_MODE === 'tv') {
      component = <HomeTV/>
    }
    /* eslint-enable */
    return <div className={classNames('App', theme, { tv: APP_MODE === 'tv' })}>{component}</div>
  }
}
