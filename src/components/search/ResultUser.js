import React from 'react'
import { SECTOR_ID_TO_DECKS, PAGES } from 'src/constants'
import classNames from 'classnames'

import SVG from 'src/components/common/SVG'
import nextArrowSvg from 'public/images/next-arrow.svg'
import partyIcon from 'public/images/nav-party.svg'

const highlightResult = (text, value) => {
  const regexp = new RegExp(value, 'gi')
  return text.replace(regexp, '<span class="match">$&</span>')
}

const ResultUser = ({ instance, selectTarget, changePage, searchValue, crewOnly, resultInstance, addToParty }) => {
  let { name, subrole } = instance

  if (searchValue) {
    searchValue.match(/([A-Za-z0-9]+)/g).forEach(value => {
      name = highlightResult(name, value)
      subrole = subrole && highlightResult(subrole.toUpperCase(), value)
      // categories.forEach(c => c.name = highlightResult(c.name, value))
      // representations.forEach(r => r.name = highlightResult(r.name, value))
    })
  }

  return (
    <a
      className={classNames('item', 'item-user', { disable: instance.optOut })}
      id={`result-user-item__${instance.id}`}
      onClick={() => {
        if (instance.optOut) return null

        selectTarget(instance)
        changePage(PAGES.POSITION)
      }}
    >
      {/* 1 if user avatar and 0 if place */}
      <div style={{ borderWidth: 0 }} className="avatar">
        <img src={instance.iconUrl} />
      </div>
      <div className="descr">
        <div className={classNames('name', { 'crew-only': crewOnly ? instance.isCrew : false })} dangerouslySetInnerHTML={{ __html: name }} />
        { !instance.optOut &&
          <div className="info">
            {<div className='cat ttu' dangerouslySetInnerHTML={{ __html: subrole }} />}
            {subrole && (<span className='middot'>&middot;</span>)}

            {instance.sectorId &&
                <div className='ttu'>
                    deck {SECTOR_ID_TO_DECKS[instance.sectorId]}
                </div>
            }
            {/* hide if user and add class "error" if closed */}
            {instance.time &&
              <span>
                <span className='middot'>&middot;</span>
                <div>
                  {Math.round(instance.time)} min.
                </div>
              </span>
            }

          </div>
        }
        {crewOnly && !instance.isParty && instance.isCrew &&
          <span
            className='add-user'
            onClick={(ev) => { addToParty(instance); ev.stopPropagation() } }
          >
            +
          </span>
        }
        {crewOnly && instance.isParty && <SVG className="is-party">{partyIcon}</SVG>}
        {!instance.optOut && <SVG className='next-arrow'>{nextArrowSvg}</SVG>}
      </div>
    </a>
  )
}

export default ResultUser
