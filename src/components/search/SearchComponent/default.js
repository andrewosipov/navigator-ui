import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import 'src/styles/Search.scss'
import { CSSTransition } from 'react-transition-group'
import { BounceLoader } from 'halogenium'
import classnames from 'classnames'

import Categories from 'src/components/search/Categories'
import Results from 'src/components/search/Results'
import Modal from 'src/containers/Modal'
import {
  ORIENTATION_LANDSCAPE,
  CONNECTION_TYPES
} from 'src/constants'

import SVG from 'src/components/common/SVG'
import TryingToConnect from 'src/components/common/TryingToConnect'
import NoConnection from 'src/components/common/NoConnection'
import searchBackSvg from 'public/images/search-back.svg'
import closeSvg from 'public/images/close.svg'

export default class SearchComponent extends PureComponent {
  static propTypes = {
    orientation: PropTypes.string.isRequired,
    goBack: PropTypes.func.isRequired,
    placeholder: PropTypes.string.isRequired,
    query: PropTypes.string.isRequired,
    onSearchChange: PropTypes.func.isRequired,
    onSearchEnter: PropTypes.func.isRequired,
    onReset: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isSearching: PropTypes.bool.isRequired,
    crewOnly: PropTypes.bool.isRequired,
    venues: PropTypes.array.isRequired,
    allVenues: PropTypes.array.isRequired,
    users: PropTypes.array.isRequired,
    selectTarget: PropTypes.func.isRequired,
    selectVenue: PropTypes.func.isRequired,
    changeDeck: PropTypes.func.isRequired,
    changePage: PropTypes.func.isRequired,
    roles: PropTypes.array.isRequired,
    party: PropTypes.array.isRequired,
    addToParty: PropTypes.func.isRequired,
    selectCategory: PropTypes.func.isRequired,
    showNearMe: PropTypes.func.isRequired,
    categories: PropTypes.array.isRequired,
    findEmbarkation: PropTypes.func.isRequired,
    showModal: PropTypes.func.isRequired,
    theme: PropTypes.string.isRequired,
    user: PropTypes.object.isRequired,
    connectionType: PropTypes.string.isRequired,
    visitedPages: PropTypes.array.isRequired
  }

  componentDidMount () {
    let [last] = this.props.visitedPages.slice(-1)
    if (last === 'catalog') {
      this.searchInputRef.focus()
    }
  }

  render () {
    return (
      <div className={classnames('Search', { landscape: this.props.orientation === ORIENTATION_LANDSCAPE })}>
        <div className="search-inner">
          <div className="input-wrap">
            <a
              className='back'
              onClick={this.props.goBack}
              id="search__back-button"
            >
              <SVG>{searchBackSvg}</SVG>
            </a>
            <input
              placeholder={this.props.placeholder}
              type="text"
              value={this.props.query}
              id="search__query-field"
              autoComplete="off"
              ref={e => { this.searchInputRef = e }}
              onChange={this.props.onSearchChange}
              onKeyUp={this.props.onSearchEnter}
            />
            {!this.props.isLoading && this.props.query !== '' &&
                            <button className='reset' onClick={this.props.onReset} id="search__reset-button">
                              <SVG>{closeSvg}</SVG>
                            </button>
            }
            <BounceLoader
              className='loader'
              loading={this.props.isLoading}
              color={this.props.theme === 'dark' ? 'rgba(255,255,255,0.6)' : 'rgba(30,31,37,0.87)'}
              size="18px"
            />
          </div>
          {this.props.connectionType === CONNECTION_TYPES.UNKNOWN &&
            <TryingToConnect
              className="search-notify"
              theme={this.props.theme}
            />
          }
          {this.props.connectionType === CONNECTION_TYPES.NONE &&
            <NoConnection
              className="search-notify"
              theme={this.props.theme}
            />
          }
          <div className="content">
            <CSSTransition
              in={!!this.props.query && this.props.isSearching}
              timeout={350}
              classNames="appear"
              unmountOnExit
            >
              <Results
                searchValue={this.props.query === 'Near me' ? '' : this.props.query}
                crewOnly={this.props.crewOnly}
                venues={this.props.venues}
                users={this.props.users}
                selectTarget={this.props.selectTarget}
                selectVenue={this.props.selectVenue}
                changeDeck={this.props.changeDeck}
                changePage={this.props.changePage}
                loader={this.props.isLoading}
                roles={this.props.roles}
                party={this.props.party}
                addToParty={this.props.addToParty}
                theme={this.props.theme}
              />
            </CSSTransition>

            <CSSTransition
              in={!this.props.query || !this.props.isSearching}
              timeout={350}
              classNames="appear"
              unmountOnExit
            >
              <Categories
                selectCategory={this.props.selectCategory}
                showNearMe={this.props.showNearMe}
                categories={this.props.categories}
                crewOnly={this.props.crewOnly}
                venues={this.props.allVenues}
                findEmbarkation={this.props.findEmbarkation}
                showModal={this.props.showModal}
                theme={this.props.theme}
                changePage={this.props.changePage}
                selectTarget={this.props.selectTarget}
                user={this.props.user}
                party={this.props.party}
              />
            </CSSTransition>

            <Modal />

          </div>
        </div>
      </div>
    )
  }
}
