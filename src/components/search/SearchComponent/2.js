import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { get } from 'lodash'
import Search from 'src/components/common/Search'
import CloseBtn from 'src/components/common/CloseBtn'
import Modal from 'src/containers/Modal'
import Categories from 'src/containers/search/bliss/Categories'
import Results from 'src/components/search/Results'

import './2.scss'

export default class SearchComponent extends PureComponent {
  constructor (props) {
    super(props)

    this.searchRef = React.createRef()
  }
  static propTypes = {
    query: PropTypes.string.isRequired,
    isSearching: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    placeholder: PropTypes.string,
    crewOnly: PropTypes.bool.isRequired,
    venues: PropTypes.array.isRequired,
    users: PropTypes.array.isRequired,
    roles: PropTypes.array.isRequired,
    party: PropTypes.array.isRequired,
    selectTarget: PropTypes.func.isRequired,
    selectVenue: PropTypes.func.isRequired,
    changeDeck: PropTypes.func.isRequired,
    changePage: PropTypes.func.isRequired,
    addToParty: PropTypes.func.isRequired,
    onSearchChange: PropTypes.func.isRequired,
    onSearchEnter: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
    theme: PropTypes.string.isRequired
  }

  handleSearchEnter = e => {
    if (e.keyCode === 13) {
      const elem = get(this.searchRef, 'current')
      elem && elem.blur()
      this.props.onSearchEnter(e)
    }
  }

  render () {
    const {
      query,
      isSearching,
      isLoading,
      placeholder,
      crewOnly,
      venues,
      users,
      roles,
      party,
      goBack,
      selectTarget,
      selectVenue,
      changeDeck,
      changePage,
      addToParty,
      onSearchChange,
      theme
    } = this.props
    return (
      <div className="search-component">
        <div className="search-component__header">
          <CloseBtn
            className="search-component__header__del-btn"
            onClick={goBack}
            theme={theme}
          />
          <Search
            value={query}
            onChange={onSearchChange}
            onKeyUp={this.handleSearchEnter}
            placeholder={placeholder}
            ref={this.searchRef}
            autoComplete="off"
            autoFocus
          />
        </div>

        { !query && !isSearching &&
          <Categories />
        }
        <div className="search-component__result">
          { !!query && isSearching &&
            <Results
              searchValue={query === 'Near me' ? '' : query}
              crewOnly={crewOnly}
              venues={venues}
              users={users}
              selectTarget={selectTarget}
              selectVenue={selectVenue}
              changeDeck={changeDeck}
              changePage={changePage}
              loader={isLoading}
              roles={roles}
              party={party}
              addToParty={addToParty}
              theme={theme}
            />
          }
        </div>

        <div className="search-component__modal-fix-wrapper">
          <Modal />
        </div>
      </div>
    )
  }
}
