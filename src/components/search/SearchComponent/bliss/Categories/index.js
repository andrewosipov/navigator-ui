import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { last } from 'lodash'
import { FormattedMessage } from 'react-intl'
import { PAGES, SECTOR_ID_TO_DECKS } from 'src/constants'
import SVG from 'src/components/common/SVG'
import cabinSvg from 'public/images/bliss/cabin.svg'
import mStationSvg from 'public/images/bliss/m-station.svg'
import restroomsSvg from 'public/images/bliss/restrooms.svg'
import './index.scss'

export default class Categories extends PureComponent {
  static propTypes = {
    user: PropTypes.object.isRequired,
    crewOnly: PropTypes.bool.isRequired,
    theme: PropTypes.string.isRequired,
    allVenues: PropTypes.array.isRequired,
    visitedPages: PropTypes.arrayOf(PropTypes.string).isRequired,
    showModal: PropTypes.func.isRequired,
    selectTarget: PropTypes.func.isRequired,
    changeDeck: PropTypes.func.isRequired,
    selectVenue: PropTypes.func.isRequired,
    changePage: PropTypes.func.isRequired
  }

  handleMyCabinClick = () => {
    const { allVenues, user, selectTarget, changePage } = this.props
    const venue = allVenues.find(v => v.globalVenueId === user.cabin)
    if (venue) {
      selectTarget(venue)
      changePage(PAGES.POSITION)
    }
  }

  handleMusteringClick = () => {
    const { allVenues, visitedPages, changeDeck, selectVenue, selectTarget, changePage } = this.props
    const musterStation = allVenues.find(venue => venue.isMusterStation())

    if (!musterStation) return

    if (last(visitedPages) !== PAGES.POSITION) {
      changeDeck(SECTOR_ID_TO_DECKS[musterStation.sectorId])
      selectVenue(musterStation)
      changePage(PAGES.HOME)
    } else {
      selectTarget(musterStation)
      changePage(PAGES.POSITION)
    }
  }

  handleRestroomClick = () => {
    const { showModal, selectTarget, changePage, user, crewOnly, theme } = this.props

    showModal('RESTROOMS', { selectTarget, changePage, user, crewOnly, theme })
  }

  handleCabinsClick = () => {
    const { showModal, selectTarget, changePage, user, allVenues, crewOnly, theme } = this.props

    showModal('CABINS', { selectTarget, changePage, user, venues: allVenues, crewOnly, theme })
  }

  render () {
    return (
      <div className="bliss-categories">
        <button
          className="bliss-categories__category"
          onClick={this.handleMyCabinClick}
        >
          <SVG className="bliss-categories__category__icon">{cabinSvg}</SVG>
          <FormattedMessage id="My-Cabin" defaultMessage="My Cabin" />
        </button>
        <button
          className="bliss-categories__category"
          onClick={this.handleMusteringClick}
        >
          <SVG className="bliss-categories__category__icon">{mStationSvg}</SVG>
          <FormattedMessage id="My-Mustering-Station" defaultMessage="My Mustering Station" />
        </button>
        <button
          className="bliss-categories__category"
          onClick={this.handleRestroomClick}
        >
          <SVG className="bliss-categories__category__icon">{restroomsSvg}</SVG>
          <FormattedMessage id="Restrooms" defaultMessage="Restroom" />
        </button>
        <button
          className="bliss-categories__category"
          onClick={this.handleCabinsClick}
        >
          <SVG className="bliss-categories__category__icon">{cabinSvg}</SVG>
          <FormattedMessage id="Cabins" defaultMessage="Cabins" />
        </button>
      </div>
    )
  }
}
