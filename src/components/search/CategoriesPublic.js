import React from 'react'
import { FormattedMessage } from 'react-intl'
import CategoryItem from './CategoryItem'
import SVG from '../common/SVG'
import searchNearMeSvg from '../../../public/images/search-near-me.svg'
import searchWCSvg from '../../../public/images/search-wc.svg'
import searchCabinsSvg from '../../../public/images/search-cabins.svg'
import searchEMBSvg from '../../../public/images/search-emb.svg'
import Contacts from './Contacts'

const CategoriesPublic = ({
  categories,
  selectCategory,
  theme,
  user,
  party,
  venues,
  showNearMe,
  showModal,
  crewOnly,
  findEmbarkation,
  selectTarget,
  changePage
}) => {
  // it needs a more beauty solution
  categories.forEach(category => {
    category.crewVenuesCount = 0
    category.sailorVenuesCount = 0

    venues.forEach(venue => {
      if (venue.categories.find(cat => cat.id === category.id)) {
        venue.crewOnly && category.crewVenuesCount++
        !venue.crewOnly && category.sailorVenuesCount++
      }
    })
  })

  return (
    <div className='categories'>
      <div className="head">
        <button onClick={() => showNearMe()}>
          <SVG>{searchNearMeSvg}</SVG>
          <FormattedMessage id="Near-me" defaultMessage="Near me" />
        </button>
        <button
          id="search__wc-button"
          onClick={() => showModal('RESTROOMS', { selectTarget, changePage, user, crewOnly, theme })}
        >
          <SVG>{searchWCSvg}</SVG>
          <FormattedMessage id="Restrooms" defaultMessage="Restrooms" />
        </button>
        <button
          id="search__cabin-button"
          onClick={() => showModal('CABINS', { selectTarget, changePage, user, venues, crewOnly, theme })}
        >
          <SVG>{searchCabinsSvg}</SVG>
          <FormattedMessage id="Cabin" defaultMessage="Cabin" />
        </button>
        <button id="search__embarkation-button" onClick={() => findEmbarkation()}>
          <SVG>{searchEMBSvg}</SVG>
          <FormattedMessage id="Embarkation" defaultMessage="Embarkation" />
        </button>
      </div>

      <div className="container">
        <h3 className="title"><FormattedMessage id="My-party" defaultMessage="My Party" /></h3>
        <Contacts
          party={party}
          changePage={changePage}
          selectTarget={selectTarget}
        />
      </div>

      <div className="container">
        {categories
          .filter(category => !category.isService && category.venuesCount > 0)
          .map(category => <CategoryItem selectCategory={selectCategory} key={category.id} instance={category} />)
        }
      </div>

    </div>
  )
}

export default CategoriesPublic
