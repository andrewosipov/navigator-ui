/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import UserItem from './UserItem'

test('UserItem', () => {
  const component = shallow(
    <UserItem user={{ iconUrl: '/', name: 'username' }} />
  )

  expect(component.find('.avatar img').html()).toEqual('<img src="/"/>')

  component.setProps({ user: { iconUrl: '/', name: 'username', cabin: 'test-cabin' }, type: 'cabin' })
  expect(component.find('.name').text()).toEqual('username')
  expect(component.find('.cabin').text()).toEqual('test-cabin')

  component.setProps({ user: { iconUrl: '/', vanueName: 'vanuename', deck: 'deckname' }, type: 'party' })
  expect(component.find('span').text()).toEqual('deck deckname, vanuename')
})
