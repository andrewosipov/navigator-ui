import React, { Component } from 'react'

import CategoriesPublic from './CategoriesPublic'
import CategoriesCrew from './CategoriesCrew'

export default (props) => props.crewOnly ? <CategoriesCrew {...props} /> : <CategoriesPublic {...props} />