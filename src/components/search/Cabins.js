import React, { Component } from 'react'
import classnames from 'classnames'
import { BounceLoader } from 'halogenium'
import { FormattedMessage, injectIntl } from 'react-intl'

import 'src/styles/Cabins.scss'
import Category from 'src/models/Category'
import Venue from 'src/models/Venue'
import SVG from 'src/components/common/SVG'
import { PAGES } from 'src/constants'

import { icons } from 'src/helpers/categorySvgSet'
import nextArrowSvg from 'public/images/next-arrow.svg'

class Cabins extends Component {
    state = {
      q: '',
      processing: false,
      notFound: false
    }

    findUserCabin () {
      let venue = this.props.modalProps.venues.find(v => v.globalVenueId === this.props.modalProps.user.cabin)
      if (venue) {
        this.props.modalProps.selectTarget(venue)
        this.props.modalProps.showModalOnSubmit ? this.props.showModal('DIRECTIONS', { }) : this.props.modalProps.changePage(PAGES.POSITION)
      }
    }

    async findCabin (cabinNumber) {
      if (this.state.notFound) {
        return
      }

      this.setState({ processing: true })
      let venues = await Venue.search(cabinNumber, Category.CABIN, this.props.modalProps.crewOnly, this.props.modalProps.user)
      this.setState({ processing: false })
      if (venues.length) {
        let venue = venues.find(v => v.name === `${cabinNumber}`)
        if (venue) {
          this.props.modalProps.selectTarget(venue)
          this.props.modalProps.showModalOnSubmit ? this.props.showModal('DIRECTIONS', { }) : this.props.modalProps.changePage(PAGES.POSITION)
        } else {
          this.setState({ notFound: true })
        }
      } else {
        this.setState({ notFound: true })
      }
    }

    render () {
      return (
        <div className='Cabins modal' onClick={(ev) => { ev.stopPropagation() }}>
          <div className="title"><FormattedMessage id="Cabins" defaultMessage="Cabins" /></div>
          <div className="content">
            <div className="buttons">
              {!this.props.modalProps.hideMyCabinButton && <button className='cabin-btn' onClick={(ev) => { ev.stopPropagation(); this.findUserCabin() }}>
                <SVG>{icons[9].public}</SVG>
                <FormattedMessage id="My-cabin" defaultMessage="my cabin" />
              </button>}
              <div className="search-cabin">
                <input
                  className={classnames({ onfocus: this.state.focus })}
                  placeholder={this.props.intl.formatMessage({ id: 'Cabin-number', defaultMessage: 'Cabin Number' })}
                  type="text"
                  value={this.state.q}
                  onChange={e => this.setState({ q: e.target.value, notFound: false })}
                />
                {!this.state.q &&
                                <button onClick={() => this.findUserCabin()}>
                                  <SVG className="next-arrow">{nextArrowSvg}</SVG>
                                </button>
                }
                {this.state.q && !this.state.processing &&
                                <button onClick={() => this.findCabin(this.state.q)}>
                                  <SVG className={classnames('next-arrow', { disabled: this.state.notFound })}>{nextArrowSvg}</SVG>
                                </button>
                }
                <BounceLoader
                  className='loader'
                  loading={this.state.processing}
                  color={this.props.modalProps.theme === 'dark' ? 'rgba(255,255,255,0.6)' : 'rgba(30,31,37,0.87)'}
                  size="18px"
                />
              </div>
            </div>
            {this.state.notFound
              ? <div className="error"><FormattedMessage id="Cabin-not-found" defaultMessage="Cabin not found" /></div>
              : <div className="help"><FormattedMessage id="Type-in-cabin-and-hit-arrow" defaultMessage="Type in Cabin and hit arrow" /></div>
            }
            {/* <div className="cabins-wrapper">
                        {range(20).map(user => <UserItem key={user} type='cabin' user={userPlaceholder} />)}
                    </div> */}
          </div>
          <div className="footer">
            <button onClick={() => this.props.hideModal()}><FormattedMessage id="cancel" defaultMessage="cancel" /></button>
          </div>
        </div>
      )
    }
}

export default injectIntl(Cabins)
