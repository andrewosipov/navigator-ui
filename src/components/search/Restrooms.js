import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import 'src/styles/Restrooms.scss'
import classnames from 'classnames'
import { BounceLoader } from 'halogenium'
import { PAGES } from 'src/constants'
import Venue from 'src/models/Venue'
import SVG from 'src/components/common/SVG'
import WCManSvg from 'public/images/wc-man.svg'
import WCWomenSvg from 'public/images/wc-women.svg'
import invalidSvg from 'public/images/invalid.svg'

class Restrooms extends Component {
    state = {
      activeWC: 'M',
      activeInvalid: false,
      loader: false
    }

    changeWC (activeWC) {
      this.setState({ activeWC })
    }

    toggleInvalid = (ev) => {
      ev.stopPropagation()
      this.setState({
        activeInvalid: !this.state.activeInvalid
      })
    }

    async findRestroom () {
      let restrooms = await Venue.findRestroom(this.state.activeWC, this.state.activeInvalid, this.props.modalProps.crewOnly, this.props.modalProps.user)
      if (restrooms.length) {
        this.props.modalProps.selectTarget(restrooms[0])
        this.props.modalProps.showModalOnSubmit ? this.props.showModal('DIRECTIONS', { }) : this.props.modalProps.changePage(PAGES.POSITION)
      }
    }

    render () {
      const { activeWC, activeInvalid } = this.state
      return (
        <div className="modal Restrooms">
          <div className="title"><FormattedMessage id="Restrooms" defaultMessage="Restrooms" /></div>
          <div className="content">
            <div className="buttons">
              <button
                onClick={(ev) => { ev.stopPropagation(); this.changeWC('M') }}
                className={classnames('big man', { active: activeWC === 'M' })}
              >
                <SVG>{WCManSvg}</SVG>
              </button>
              <button
                onClick={(ev) => { ev.stopPropagation(); this.changeWC('W') }}
                className={classnames('big women', { active: activeWC === 'W' })}
              >
                <SVG>{WCWomenSvg}</SVG>
              </button>
              <button
                onClick={this.toggleInvalid}
                className={classnames('dis', { active: activeInvalid })}>
                <SVG>{invalidSvg}</SVG>
              </button>
            </div>
          </div>
          <div className="footer">
            <button onClick={() => this.props.hideModal()}><FormattedMessage id='cancel' defaultMessage='cancel' /></button>
            <button className='find-restroom' onClick={() => {
              this.findRestroom()
              this.setState({ loader: true })
              this.props.hideModal()
            }}>
              {!this.state.loader && <FormattedMessage id='find-restroom' defaultMessage='find restroom' />}
              <BounceLoader
                className='loader'
                loading={this.state.loader}
                color={this.props.theme === 'dark' ? 'rgba(255,255,255,0.6)' : 'rgba(30,31,37,0.87)'}
                size="16px"
              />
            </button>

          </div>
        </div>
      )
    }
}

export default Restrooms
