import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import Venue from 'src/models/Venue'
import User from 'src/models/User'
import ResultItem from 'src/containers/search/ResultItem'
import ResultUser from './ResultUser'
import nextArrowSvg from 'public/images/next-arrow.svg'
import SVG from 'src/components/common/SVG'

class ResultsContent extends Component {
    state = {
      showCount: 3
    }

    getResult = () => {
      const { resultInstance = 'all', instances, searchValue, selectVenue, selectTarget, changeDeck, changePage, crewOnly = false, theme } = this.props

      let result = instances.filter(instance =>
        instance instanceof User ||
            (instance instanceof Venue && ((resultInstance === 'crew' && instance.crewOnly) || (resultInstance === 'sailor' && !instance.crewOnly)))
      )

      this.resultCount = result.length <= 10 ? result.length : 10

      if (result.length === 0) {
        return (
          <div className='content'>
            <div className='text'><FormattedMessage id="No-results" defaultMessage="No results" /></div>
          </div>
        )
      }

      if (resultInstance === 'people') {
        return result
          .filter(instance => instance instanceof User)
          .slice(0, (this.state.showCount === 'all' && this.resultCount) || this.state.showCount)
          .map((instance) => {
            instance.isParty = !!this.props.party.find(u => u.id === instance.id)
            return <ResultUser
              key={instance.id}
              instance={instance}
              selectTarget={selectTarget}
              selectVenue={selectVenue}
              changePage={changePage}
              changeDeck={changeDeck}
              crewOnly={crewOnly}
              searchValue={searchValue}
              resultInstance={resultInstance}
              addToParty={this.props.addToParty}
              theme={theme}
            />
          })
      }

      return instances
        .filter(instance => (
          instance instanceof Venue && ((resultInstance === 'crew' && instance.crewOnly) || (resultInstance === 'sailor' && !instance.crewOnly))))
        .slice(0, (this.state.showCount === 'all' && this.resultCount) || this.state.showCount)
        .map((instance) =>
          <ResultItem
            key={instance.id}
            instance={instance}
            searchValue={searchValue}
            resultInstance={resultInstance}
          />
        )
    }

    clickHandler = () => {
      this.setState({
        showCount: this.state.showCount === 'all' ? 3 : 'all'
      })
    }

    render () {
      const { instances } = this.props

      return (
        <div className='content'>
          {this.getResult()}
          {!!instances.length && this.state.showCount !== 'all' && this.resultCount > this.state.showCount && <button className='show-all' onClick={this.clickHandler}>
            <FormattedMessage id="Show-all-N-results" defaultMessage="Show all {count} results" values={{ count: this.resultCount }} />
            <SVG className='next-arrow'>{nextArrowSvg}</SVG>
          </button>}
        </div>
      )
    }
}

export default ResultsContent
