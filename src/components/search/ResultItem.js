/* globals SIM_ID */
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { last } from 'lodash'
import { SECTOR_ID_TO_DECKS, THEMES, PAGES } from 'src/constants'
import classNames from 'classnames'
import { FormattedMessage, injectIntl } from 'react-intl'

import SVG from 'src/components/common/SVG'
import nextArrowSvg from 'public/images/next-arrow.svg'

const highlightResult = (text, value) => {
  const regexp = new RegExp(value, 'gi')
  return text.replace(regexp, '<span class="match">$&</span>')
}

class ResultItem extends PureComponent {
  static propTypes = {
    instance: PropTypes.object.isRequired,
    searchValue: PropTypes.string.isRequired,
    crewOnly: PropTypes.bool.isRequired,
    resultInstance: PropTypes.string.isRequired,
    theme: PropTypes.string.isRequired,
    visitedPages: PropTypes.arrayOf(PropTypes.string).isRequired,
    intl: PropTypes.object.isRequired,
    selectTarget: PropTypes.func.isRequired,
    selectVenue: PropTypes.func.isRequired,
    changePage: PropTypes.func.isRequired,
    changeDeck: PropTypes.func.isRequired
  }

  handleVenueSelect = () => {
    const { instance, changeDeck, selectVenue, changePage } = this.props
    changeDeck(SECTOR_ID_TO_DECKS[instance.sectorId])
    selectVenue(instance)
    changePage(PAGES.HOME)
  }

  handleTargetSelect = () => {
    const { instance, selectTarget, changePage } = this.props
    selectTarget(instance)
    changePage(PAGES.POSITION)
  }

  handleClick = () => {
    if (SIM_ID === 2 && last(this.props.visitedPages) !== PAGES.POSITION) {
      this.handleVenueSelect()
    } else {
      this.handleTargetSelect()
    }
  }

  render () {
    const {
      instance,
      searchValue,
      crewOnly,
      resultInstance,
      theme,
      intl
    } = this.props
    let { name, categories, representations } = instance

    if (searchValue) {
      searchValue.match(/([A-Za-z0-9]+)/g).forEach(value => {
        name = highlightResult(name, value)
        categories.forEach(c => { c.name = highlightResult(c.name, value) })
        representations.forEach(r => { r.name = highlightResult(r.name, value) })
      })
    }

    return (
      <a
        className="item"
        id={`result-venue-item__${instance.id}`}
        onClick={this.handleClick}
      >
        {/* 1 if user avatar and 0 if place */}
        <div style={{ borderWidth: 0 }} className="avatar">
          { theme !== THEMES.BLISS &&
            <SVG>{instance.getIcon(resultInstance === 'crew')}</SVG>
          }
        </div>
        <div className="descr">
          {crewOnly &&
            representations.map((r, index) =>
              <div
                key={index}
                className="name"
                dangerouslySetInnerHTML={{ __html: r.name }}
              />
            )
          }
          <div className={classNames('name', { 'crew-only': crewOnly })} dangerouslySetInnerHTML={{ __html: intl.formatMessage({ id: `venue.${instance.id}`, defaultMessage: name }) }} />
          <div className="info">
            {!!categories.length && categories
              .sort((a, b) => b.isDefaultCategory - a.isDefaultCategory)
              .map(c => <div key={c.id} className='cat ttu' dangerouslySetInnerHTML={{ __html: intl.formatMessage({ id: `category.${c.id}`, defaultMessage: c.name }) }} />)
              .reduce((prev, curr) => [prev, ', ', curr])
            }
            <span className='middot'>&middot;</span>
            <div className='ttu'>
              <FormattedMessage id="Deck-N" defaultMessage="deck {number}" values={{ number: SECTOR_ID_TO_DECKS[instance.sectorId] }} />
            </div>
            {/* hide if user and add class "error" if closed */}
            <span className='middot'>&middot;</span>
            <div>
              <FormattedMessage id="time-min" defaultMessage="{time} min." values={{ time: Math.round(instance.time) }} />
            </div>
          </div>
          <SVG className='next-arrow'>{nextArrowSvg}</SVG>
        </div>
      </a>
    )
  }
}

export default injectIntl(ResultItem)
