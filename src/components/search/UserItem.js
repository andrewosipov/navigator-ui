import React from 'react'
import { FormattedMessage } from 'react-intl'
import cn from 'classnames'
import { PAGES } from 'src/constants'
import SVG from 'src/components/common/SVG'
import 'src/styles/UserItem.scss'
import nextArrowSvg from 'public/images/next-arrow.svg'

const UserItem = ({ user, type, selectTarget, changePage, hideModal }) => (
  <div
    className={cn('UserItem', { disable: user.optOut })}
    onClick={() => {
      if (user.optOut) return null

      hideModal()
      selectTarget(user)
      changePage(PAGES.POSITION, false)
    }}
  >
    <div className="avatar">
      <img src={user.iconUrl} />
    </div>
    <div className="info">
      <div className="descr">
        <div className="name">{user.name}</div>
        {type === 'cabin' && !user.optOut && <div className="cabin">{user.cabin}</div>}
        {type === 'party' && !user.optOut && <span><FormattedMessage id="Deck-N" defaultMessage="deck {number}" values={{ number: user.deck }} /></span>}
      </div>
      {!user.optOut && <SVG className='next-arrow'>{nextArrowSvg}</SVG>}
    </div>
  </div>
)

export default UserItem
