import React from 'react'
import ResultsContainer from './ResultsContainer'
import { FormattedMessage } from 'react-intl'

export default (props) => {
  const { users, venues, searchValue, loader } = props

  if ((users.length || venues.length) && !loader) {
    return <ResultsContainer {...props} />
  } else {
    return (
      <div className='results__placeholder'>
        {loader && <FormattedMessage id="Searching" defaultMessage='Searching...' />}
        {!loader && <FormattedMessage id="No-results-for" defaultMessage="No results for '{value}'" values={{ value: searchValue }} />}
      </div>
    )
  }
}
