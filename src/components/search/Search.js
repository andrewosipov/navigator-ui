import React, { Component } from 'react'
import { injectIntl } from 'react-intl'
import 'src/styles/Search.scss'
import SearchComponent from './SearchComponent'

import Venue from 'src/models/Venue'
import User from 'src/models/User'
import { toInt } from 'src/helpers/misc'
import {
  ORIENTATION_LANDSCAPE,
  ORIENTATION_PORTRAIT,
  PAGES
} from 'src/constants'

class Search extends Component {
  constructor (props) {
    super(props)
    this.state = {
      venues: [],
      users: [],
      q: '',
      loader: false,
      orientation: this.getOrientation()
    }

    if (this.state.q) {
      this.search(this.state.q)
    }

    this.addEventListeners()
  }

  addEventListeners () {
    const updateDeckContainerSizeAndOrientation = () => {
      this.setState({ orientation: this.getOrientation() })
    }

    this.onOrientationChange = () => {
      return setTimeout(() => {
        updateDeckContainerSizeAndOrientation()
      }, 500)
    }

    if ('onorientationchange' in window) {
      window.addEventListener('orientationchange', this.onOrientationChange, false)
    }

    window.onresize = updateDeckContainerSizeAndOrientation
  }

  componentWillUnmount () {
    window.onresize = () => {}
    window.removeEventListener('orientationchange', this.onOrientationChange)
  }

  goBack = () => setTimeout(this.props.goBack, 350)

  onSearchChange = (e) => {
    let q = e.target.value
    this.setState({ q })
    this.clear()
    if (q.length > 2) {
      this.search(q)
    }
  }

  onSearchEnter = (e) => {
    if (e.keyCode === 13) {
      this.search(this.state.q)
    }
  }

  clear () {
    this.setState({ venues: [], users: [], loader: false })
  }

  selectCategory = (categoryId, crewOnly) => {
    this.props.selectCategory(categoryId, crewOnly)
    this.props.changePage(PAGES.CATALOG)
  }

  showModal = (name, payload) => this.props.showModal(name, payload)

  async search (q, categoryId = null, filter = null, onFound = null) {
    this.clear()
    this.setState({ loader: true })
    clearTimeout(this.searchDelay)
    this.searchDelay = setTimeout(async () => {
      let venues = await Venue.search(
        q,
        categoryId,
        this.props.crewOnly,
        this.props.user,
        Object.values(this.props.nonServiceVenues)
      )
      venues.sort((a, b) => a.time - b.time)
      let users
      try {
        users = await User.search(q, this.props.crewOnly, this.props.accessToken, this.props.roles)
      } catch (e) {
        users = []
      }

      this.setState({
        venues: venues,
        users: users,
        loader: false
      })
      if (onFound) {
        onFound(venues.concat(users))
      }
    }, 1000)
  }

  findEmbarkation = () => {
    let venue = this.props.allVenues.find(v => toInt(v.id) === 3486)
    if (venue) {
      this.props.selectTarget(venue)
      this.props.changePage(PAGES.POSITION)
    }
  }

  getOrientation () {
    return window.innerHeight > window.innerWidth ? ORIENTATION_PORTRAIT : ORIENTATION_LANDSCAPE
  }

  showNearMe = () => {
    this.setState({ q: 'Near me' })
    this.search('', null, v => v instanceof Venue && !v.isRestroom() && !v.isEmbarkation())
  }

  reset = () => {
    this.clear()
    this.setState({
      q: ''
    })
  }

  getFieldPlaceholder = () => {
    if (!this.props.destinationDirection) {
      return this.props.intl.formatMessage({ id: 'Search-places', defaultMessage: 'Search places' })
    }

    if (this.props.destinationDirection === 'to') {
      return this.props.intl.formatMessage({ id: 'Place-of-arrival', defaultMessage: 'Place of arrival' })
    }
    return this.props.intl.formatMessage({ id: 'Place-of-departure', defaultMessage: 'Place of departure' })
  }

  isSearching () {
    return this.state.loader || this.state.q.length > 2 || !!this.state.venues.length || !!this.state.users.length
  }

  render () {
    return (
      <SearchComponent
        orientation={this.state.orientation}
        goBack={this.goBack}
        placeholder={this.getFieldPlaceholder()}
        query={this.state.q}
        onSearchChange={this.onSearchChange}
        onSearchEnter={this.onSearchEnter}
        onReset={this.reset}
        isLoading={this.state.loader}
        isSearching={this.isSearching()}
        crewOnly={this.props.crewOnly}
        venues={this.state.venues}
        allVenues={this.props.allVenues}
        users={this.state.users}
        selectTarget={this.props.selectTarget}
        selectVenue={this.props.selectVenue}
        changeDeck={this.props.changeDeck}
        changePage={this.props.changePage}
        roles={this.props.roles}
        party={this.props.party}
        addToParty={this.props.addToParty}
        selectCategory={this.selectCategory}
        showNearMe={this.showNearMe}
        categories={this.props.categories}
        findEmbarkation={this.findEmbarkation}
        showModal={this.showModal}
        theme={this.props.theme}
        user={this.props.user}
        connectionType={this.props.connectionType}
        visitedPages={this.props.visitedPages}
      />
    )
  }
}

export default injectIntl(Search)
