import React, { Component } from 'react'
import ResultsContent from './ResultsContent'
import { FormattedMessage } from 'react-intl'

class ResultsContainer extends Component {
    state = {
      filter: 'all',
      activeCategory: null
    }

    filterHandler = (filter) => {
      this.setState({
        filter
      })
    }

    getFilterClassName = (filter = 'all') => {
      return this.state.filter === filter ? 'active' : ''
    }

    render () {
      const { crewOnly = false } = this.props
      const { filter } = this.state

      if (crewOnly) {
        return (
          <div className='results'>
            <div className='fast-filter'>
              <button className={`filter-tag ${this.getFilterClassName('all')}`} onClick={() => this.filterHandler('all')}><FormattedMessage id="All" defaultMessage="All" /></button>
              <button className={`filter-tag ${this.getFilterClassName('people')}`} onClick={() => this.filterHandler('people')}><FormattedMessage id="People" defaultMessage="People" /></button>
              <button className={`filter-tag ${this.getFilterClassName('sailor')}`} onClick={() => this.filterHandler('sailor')}><FormattedMessage id="Public-venues" defaultMessage="Public Venues" /></button>
              <button className={`filter-tag ${this.getFilterClassName('crew')}`} onClick={() => this.filterHandler('crew')}><FormattedMessage id="Service-venues" defaultMessage="Service Venues" /></button>
            </div>
            {(filter === 'all' || filter === 'people') &&
                        <div className='category'>
                          <h3 className='title'><FormattedMessage id="People" defaultMessage="People" /></h3>
                          <ResultsContent resultInstance='people' title='people' {...this.props} instances={this.props.users}/>
                        </div>
            }
            {(filter === 'all' || filter === 'sailor') &&
                        <div className='category'>
                          <h3 className='title'><FormattedMessage id="Sailor-venues" defaultMessage="Sailor venues" /></h3>
                          <ResultsContent resultInstance='sailor' title='sailor venues' {...this.props} instances={this.props.venues}/>
                        </div>
            }
            {(filter === 'all' || filter === 'crew') &&
                        <div className='category'>
                          <h3 className='title'><FormattedMessage id="Crew-venues" defaultMessage="Crew venues" /></h3>
                          <ResultsContent resultInstance='crew' title='crew venues' {...this.props} instances={this.props.venues}/>
                        </div>
            }
          </div>
        )
      }

      return (
        <div className='results'>
          <div className='fast-filter'>
            <button className={`filter-tag ${this.getFilterClassName('all')}`} onClick={() => this.filterHandler('all')}><FormattedMessage id="All" defaultMessage="All" /></button>
            <button className={`filter-tag ${this.getFilterClassName('people')}`} onClick={() => this.filterHandler('people')}><FormattedMessage id="People" defaultMessage="People" /></button>
            <button className={`filter-tag ${this.getFilterClassName('sailor')}`} onClick={() => this.filterHandler('sailor')}><FormattedMessage id="Venues" defaultMessage="Venues" /></button>
          </div>
          {(filter === 'all' || filter === 'people') &&
                <div className='category'>
                  <h3 className='title'><FormattedMessage id="People" defaultMessage="People" /></h3>
                  <ResultsContent resultInstance='people' title='people' {...this.props} instances={this.props.users} />
                </div>
          }
          {(filter === 'all' || filter === 'sailor') &&
                <div className='category'>
                  <h3 className='title'><FormattedMessage id="Venues" defaultMessage="Venues" /></h3>
                  <ResultsContent resultInstance='sailor' title='venues' {...this.props} instances={this.props.venues} />
                </div>
          }
        </div>
      )
    }
}

export default ResultsContainer
