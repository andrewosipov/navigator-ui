import React from 'react'
import classnames from 'classnames'
import SVG from '../common/SVG'
import DynamicFormattedMessage from '../common/DynamicFormattedMessage'

const CategoryItem = ({ instance, selectCategory, crewOnly = false }) => (
  <button
    className='cat'
    id={`category-item__${instance.id}`}
    onClick={() => {
      selectCategory(instance.id, crewOnly)
    }}
  >
    <div className={`img-wrap ${instance.id === 1 || instance.id === 2 ? 'empty' : ''}`}>
      <SVG className={classnames('imgr', { crewonly: crewOnly })}>{instance.getFullIcon(crewOnly)}</SVG>
      <span className="count">{ crewOnly ? instance.crewVenuesCount : instance.sailorVenuesCount}</span>
    </div>
    <div className="name">
      <DynamicFormattedMessage
        id={`category.${instance.id}`}
        defaultMessage={instance.name}
      />
    </div>
  </button>
)

export default CategoryItem
