import React from 'react'
import { FormattedMessage } from 'react-intl'
import '../../styles/MyParty.scss'
import '../../styles/HomeHint.scss'
import UserItem from './UserItem'
import SVG from '../common/SVG'
import closeSvg from '../../../public/images/close.svg'

const MyParty = ({ hideModal, party = [], crewOnly = false, selectTarget, changePage }) => (
  <div className='MyParty HomeHint'>

    <div className="head">
      <div className="name">{crewOnly
        ? <FormattedMessage id="My-contacts" defaultMessage="My Contacts" />
        : <FormattedMessage id='My-party' defaultMessage='My Party' />
      }</div>

      <button onClick={() => hideModal()} id="close__my__party-button">
        <SVG>{closeSvg}</SVG>
      </button>
    </div>

    <div className="content">
      {party.map(user =>
        <UserItem
          key={user.id}
          type='party'
          user={user}
          selectTarget={selectTarget}
          changePage={changePage}
          hideModal={hideModal}
        />
      )}
      {!party.length &&
        <div className="no-friend-overlay">
          <FormattedMessage id="You-have-no-friends" defaultMessage="You have no friends" />
        </div>
      }
    </div>

  </div>
)

export default MyParty
