import React, { Component } from 'react'
import cn from 'classnames'
import { PAGES } from 'src/constants'

class Contacts extends Component {
  render () {
    return (
      <div className="contacts">
        {this.props.party.map(user => (
          <button
            key={user.id}
            className={cn('item', { disable: user.optOut })}
            onClick={() => {
              if (user.optOut) return

              this.props.selectTarget(user)
              this.props.changePage(PAGES.POSITION, false)
            }}
          >
            <div className="img" style={{ backgroundImage: user.iconUrl ? `url(${user.iconUrl}&size=low-res)` : '' }} />
            <span className="name">{user.name}</span>
          </button>
        ))}
      </div>
    )
  }
}

export default Contacts
