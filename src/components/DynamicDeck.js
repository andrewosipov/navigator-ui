import $ from 'jquery'
import React, { Component } from 'react'
import classNames from 'classnames'
import { Matrix } from 'transformation-matrix-js'

import Deck from './Deck'

import { WIDTH, HEIGHT, ORIENTATION_LANDSCAPE, WIDE_PERSPECTIVE_ROTATION, HEATMAP_MAP_MODE } from 'src/constants'
import { inRange, toInt, isWideScreen, isSafari } from 'src/helpers/misc'
import DeckButtons from 'src/components/DeckButtons'

export default class DynamicDeck extends Component {
    static defaultProps = {
      rotationEnabled: true,
      zoomEnabled: true,
      panEnabled: true,
      showSelectedPaths: false,
      minZoom: 1,
      maxZoom: 10,
      displayOnlyNearestVenues: false,
      stickToUser: false,
      showOverviewButton: false,
      showDeckButton: false,
      showDestinationPoint: false
    }

    constructor (props) {
      super(props)
      this.state = {
        inMove: false,
        stickToUser: props.stickToUser || toInt(props.deck) === toInt(props.user.deck) || !this.props.deck,
        initialized: false
      }
    }

    componentDidMount () {
      this.addEventListeners()
      if (this.props.selectedVenue) {
        let selectedVenue = this.props.selectedVenue
        setTimeout(() => {
          this.zoomToPoint(selectedVenue.x, selectedVenue.y, 'venue')
          setTimeout(() => this.setState({ initialized: true }), 150)
        }, 500)
      } else if (this.state.stickToUser) {
        this.centerToUserTimeout = setTimeout(() => {
          this.centerToUser()
          setTimeout(() => this.setState({ initialized: true }), 150)
        }, 500)
      } else {
        setTimeout(() => this.setState({ initialized: true }), 150)
      }
    }

    componentDidUpdate (prevProps) {
      if (this.state.stickToUser) {
        if (
          (prevProps.directionTo && !this.props.directionTo && toInt(this.props.deck) === toInt(this.props.user.deck)) ||
          (prevProps.width !== this.props.width || prevProps.height !== this.props.height) ||
          (this.props.user.x !== prevProps.user.x || this.props.user.y !== prevProps.user.y)
        ) {
          this.centerToUser()
        }
      }

      if (toInt(this.props.deck) !== toInt(this.props.user.deck) && this.state.stickToUser) {
        if (prevProps.deck === this.props.deck) {
          this.props.changeDeck(this.props.user.deck, this.props.debug)
        } else {
          this.setState({ stickToUser: false })
        }
      }
    }

    componentWillUnmount () {
      if (this.onMapDragStart) {
        this.dynamicDeckContainer.removeEventListener(this.interactions.down, this.onMapDragStart)
      }
      clearTimeout(this.centerToUserTimeout)
    }

    addEventListeners () {
      if (this.props.panEnabled) {
        this.addMapDragEventListener()
      }
    }

    addMapDragEventListener () {
      const isTouch = ('ontouchstart' in document.documentElement)
      this.interactions = {
        click: !isTouch ? 'click' : 'touchstart',
        move: !isTouch ? 'mousemove' : 'touchmove',
        down: !isTouch ? 'mousedown' : 'touchstart',
        up: !isTouch ? 'mouseup' : 'touchend'
      }

      let getPageXY = (e) => {
        let x = e.pageX
        let y = e.pageY
        if (x === undefined) {
          x = e.touches[0].pageX
          y = e.touches[0].pageY
        }
        if (!this.isHorizontalOrientation()) {
          let t = x
          x = -y
          y = t
        }
        return { x, y }
      }

      let onMapDrag = (e) => {
        e.preventDefault()
        let newPos = getPageXY(e)
        let factor = Math.max(WIDTH / this.props.width, HEIGHT / this.props.height) / this.getZoom()
        factor *= isSafari() ? 1.7 : 1.3
        if (this.props.perspectiveRotation && isSafari()) {
          factor *= 3
        }

        let diff = {
          x: -(this.intPos.x - newPos.x) * factor,
          y: -(this.intPos.y - newPos.y) * factor
        }

        if (Math.abs(diff.x) > 2000) {
          diff.x *= 1.5
        }

        let angle = this.props.rotation * Math.PI / 180
        this.setState({ stickToUser: false })
        this.changeOffsetX(parseInt(this.props.offsetX, 10) + diff.x * Math.cos(angle) + diff.y * Math.sin(angle))
        this.intPos = newPos
      }

      this.onMapDragStart = (e) => {
        this.intPos = getPageXY(e)
        this.setState({ inMove: true })
        let isLong = false

        setTimeout(() => {
          isLong = true
        }, 150)

        this.dynamicDeckContainer.addEventListener(this.interactions.move, onMapDrag)

        let onMapDragEnd = (e) => {
          this.dynamicDeckContainer.removeEventListener(this.interactions.move, onMapDrag)
          this.dynamicDeckContainer.removeEventListener(this.interactions.up, onMapDragEnd)
          if (isLong) {
            setTimeout(() => {
              this.setState({ inMove: false })
            }, 50)
          } else {
            this.setState({ inMove: false })
          }
        }

        this.dynamicDeckContainer.addEventListener(this.interactions.up, onMapDragEnd)
      }

      this.dynamicDeckContainer.addEventListener(this.interactions.down, this.onMapDragStart)
    }

    transformClientCoordinates (clientX, clientY) {
      const matrix = Matrix.from(this.deckGroup.getScreenCTM().inverse())
      return matrix.applyToPoint(clientX, clientY)
    }

    changeOffsetX (offsetX, clean = true) {
      if (clean) {
        offsetX = this.cleanOffsetX(offsetX)
      }
      this.props.changeOffsetX(offsetX)
    }

    cleanOffsetX (offsetX) {
      let maxOffset = WIDTH / 2
      let minOffset = -maxOffset
      if (!this.props.directionTo || this.props.perspectiveRotation) {
        maxOffset = maxOffset - WIDTH / (this.getZoom() * 2) + 500 * +!this.isHorizontalOrientation()
        if (this.deckContainer) {
          let box = this.deckContainer.getBBox()
          if (box.width) {
            minOffset = box.width + box.x - WIDTH / 2 - WIDTH / (this.getZoom() * 2) + 500 * +!this.isHorizontalOrientation()
            minOffset *= -1
          }
        } else {
          minOffset = -maxOffset
        }

        if (!this.props.perspectiveRotation) {
          maxOffset += 3000 / this.getZoom()
          minOffset -= 3000 / this.getZoom()
        } else {
          if (this.props.perspectiveRotation > WIDE_PERSPECTIVE_ROTATION) {
            if (this.props.rotation === 180) {
              maxOffset += 2000
            } else {
              minOffset -= 2000
            }
          }
        }
      }
      return inRange(toInt(offsetX || 0), minOffset, maxOffset - 1000 * +this.isHorizontalOrientation())
    }

    changeRotation (rotation) {
      this.props.changeRotation(this.cleanRotation(rotation))
    }

    cleanRotation (rotation) {
      return inRange(toInt(rotation || 0), -360, 360)
    }

    getZoom = () => {
      if (this.props.localState) {
        return this.props.perspectiveZoom
      }

      return this.props.zoom
    }

    changeZoom (zoom) {
      this.props.changeZoom(this.cleanZoom(zoom), !!this.props.perspectiveRotation)
    }

    cleanZoom (zoom) {
      return inRange(parseFloat(zoom || this.props.minZoom), this.props.minZoom, this.props.maxZoom)
    }

    cleanPerspectiveRotation (val) {
      return inRange(toInt(val || 0), 0, 60)
    }

    centerToUser = () => {
      if (toInt(this.props.user.deck) !== toInt(this.props.deck)) {
        this.props.changeDeck(this.props.user.deck, this.props.debug)
      }
      let { x, y } = this.props.user
      if (this.props.perspectiveRotation) {
        let factor = WIDTH / (this.props.height * this.getOptimalZoom())
        if (this.props.perspectiveRotation > 40) {
          factor *= 3
        }
        x += ($(window).height() * factor) / 4 * Math.cos(this.props.rotation * Math.PI / 180)
      }
      this.zoomToPoint(x, y)
      this.setState({
        stickToUser: true
      })
    }

    getZoomFactor () {
      if (!this.deckGroup) {
        return 1
      }

      let a, b
      if (this.props.width < this.props.height || !this.isHorizontalOrientation()) {
        a = this.props.width
        b = this.deckGroup.getBoundingClientRect().width
      } else {
        a = this.props.height
        b = this.deckGroup.getBoundingClientRect().height
      }

      if (this.isHorizontalOrientation()) {
        a -= 50
      }

      return a / b
    }

    zoomToPoint (x, y, target) {
      this.setOptimalZoom()
      let clean = !this.props.perspectiveRotation
      let cx = WIDTH / 2
      if (target === 'venue') {
        let k = Math.max(WIDTH / this.props.width, HEIGHT / this.props.height) / this.getZoom()
        cx += $('.VenueInfo').height() * k / 4
      }
      this.changeOffsetX(cx - x, clean)
    }

    getOptimalZoom () {
      let zoom = this.getZoom()
      if (zoom === 1 || !zoom) {
        zoom = this.getZoomFactor()
        if (isWideScreen()) {
          zoom = Math.min(zoom, this.props.perspectiveRotation ? 5 : 2.6)
        }
      }
      return zoom
    }

    setOptimalZoom () {
      let zoom = this.getOptimalZoom()
      if (zoom !== this.props.zoom) {
        this.changeZoom(zoom, !!this.props.perspectiveRotation)
      }
    }

    setUserPosition (e) {
      if (this.props.perspectiveRotation) {
        return
      }
      let { x, y } = this.transformClientCoordinates(e.clientX, e.clientY)
      return this.props.setUserPosition(x, HEIGHT - y, this.props.deck)
    }

    selectVenue (venue) {
      if (this.props.selectVenue && !this.state.inMove) {
        setTimeout(() => {
          this.zoomToPoint(venue.x, venue.y, 'venue')
        }, 200)
        return this.props.selectVenue(venue)
      }
    }

    selectEndVenue (venue) {
      if (this.props.perspectiveRotation) {
        return
      }
      return this.props.selectEndVenue(venue)
    }

    isHorizontalOrientation () {
      return this.props.orientation === ORIENTATION_LANDSCAPE
    }

    onClick (e) {
      if (this.state.inMove) {
        return
      }

      if (e.shiftKey) {
        this.setUserPosition(e)
      } else {
        if (this.props.deselectVenue) {
          if (!e.target.id.startsWith('venue-area')) {
            this.props.deselectVenue()
          }
        }
      }
    }

    getVenues () {
      let venues = this.props.venues
      if (this.props.displayOnlyNearestVenues) {
        venues = venues.filter(v => Math.hypot(
          this.props.user.x - v.x,
          this.props.user.y - v.y
        ) < (5000 + 3000 * (this.props.maxZoom - this.getZoom())))
      }
      return venues
    }

    toggleOverview = () => {
      if (this.getZoom() !== 1) {
        this.changeZoom(1)
      } else {
        this.setOptimalZoom()
      }
    }

    toggleSelectDeck = (e) => {
      if (e) {
        e.preventDefault()
      }
      this.props.deselectVenue()
      this.props.showModal('SELECT_DECK')
    }

    render () {
      return (
        <div
          className={classNames('DynamicDeck', this.props.className)}
          id={this.props.componentId || 'DeckContainer'}
          onClick={(e) => this.onClick(e)}
          ref={el => { this.dynamicDeckContainer = el }}
        >

          <DeckButtons
            theme={this.props.theme}
            stickToUser={this.state.stickToUser}
            showOverviewButton={this.props.showOverviewButton}
            showDeckButton={this.props.showDeckButton}
            showInfoButton={this.props.showInfoButton}
            zoom={this.getZoom()}
            centerToUser={this.centerToUser}
            toggleOverview={this.toggleOverview}
            toggleSelectDeck={this.toggleSelectDeck}
            isNavigation={this.props.isNavigation}
          />

          <Deck
            perspectiveRotation={this.props.perspectiveRotation}
            rotation={this.props.rotation}
            zoom={this.getZoom()}
            offsetX={this.cleanOffsetX(this.props.offsetX)}
            offsetY={this.props.offsetY}
            inMove={this.state.inMove}
            isHorizontalOrientation={this.isHorizontalOrientation()}
            showDestinationPoint={this.props.showDestinationPoint}
            initialized={this.state.initialized}
            crewOnly={this.props.crewOnly}

            dynamicDeck
            hideVenues={this.props.mapMode === HEATMAP_MAP_MODE }

            mapMode={this.props.mapMode}
            mapSubModes={this.props.mapSubModes}

            deck={this.props.deck}
            width={this.props.width}
            height={this.props.height}

            selectedVenue={this.props.selectedVenue}
            endVenue={this.props.endVenue}
            selectedPaths={this.props.showSelectedPaths ? this.props.selectedPaths : undefined}

            venues={this.getVenues()}
            paths={this.props.debug ? this.props.paths : undefined}
            user={this.props.user}
            users={this.props.users}
            party={this.props.party}

            categories={this.props.categories}
            page={this.props.page}
            theme={this.props.theme}

            onSvgReady={el => { this.svg = el }}
            onDeckGroupReady={el => { this.deckGroup = el }}
            onDeckContainerReady={el => { this.deckContainer = el }}
            selectVenue={venue => this.selectVenue(venue)}
            selectEndVenue={venue => this.selectEndVenue(venue)}
          />
        </div>
      )
    }
}
