import React, { Fragment } from 'react'
import { FormattedMessage, injectIntl } from 'react-intl'
import { get } from 'lodash'
import './2.scss'
import { SECTOR_ID_TO_DECKS } from 'src/constants'
import SVG from 'src/components/common/SVG'
import decksSvg from 'public/images/decks-icon.svg'
import mStationSvg from 'public/images/bliss/m-station.svg'
import CloseBtn from 'src/components/common/CloseBtn'

const VenueInfo = ({ intl, venue, user, theme, deselectVenue, changePage }) => {
  const showTime = false // wait for venue available time feature
  const isMuster = venue.isMusterStation()
  if (!venue) {
    return null
  }

  return (
    <div className="venue-info">
      <div className="venue-info__header">
        <div className="venue-info__header__row">
          <div className="venue-info__header__row__cell venue-info__header__row__cell--title">
            {!isMuster &&
              <Fragment>
                {intl.formatMessage({ id: `venue.${venue.id}`, defaultMessage: venue.name })}
                {user.cabin === venue.id && ` - ${intl.formatMessage({ id: 'My-cabin', defaultMessage: 'My cabin' })}`}
              </Fragment>
            }
            {isMuster &&
              <Fragment>
                <SVG className="venue-info__header__muster-icon">{mStationSvg}</SVG>
                {intl.formatMessage({ id: `My-Mustering-Station`, defaultMessage: 'My Mustering Station' })}
              </Fragment>
            }
          </div>
          <div className="venue-info__header__row__cell venue-info__header__row__cell--open">
            {showTime && `${intl.formatTime((new Date()).setHours(9, 0, 0))} - ${intl.formatTime((new Date()).setHours(20, 30, 0))}`}
          </div>
        </div>
        <div className="venue-info__header__row">
          <div className="venue-info__header__row__cell">
            <SVG className="venue-info__header__deck-icon">{decksSvg}</SVG>
            <FormattedMessage
              id="Deck-N"
              defaultMessage="Deck {number}"
              values={{ number: SECTOR_ID_TO_DECKS[venue.sectorId] }}
            />
          </div>
          <div className="venue-info__header__row__cell">
            {!!venue.categories && !isMuster &&
              get(venue, 'categories', [])
                .map(category =>
                  intl.formatMessage({ id: `category.${category.id}`, defaultMessage: category.name })
                )
                .join(', ')
            }
            {isMuster &&
              intl.formatMessage({ id: `venue.${venue.id}`, defaultMessage: venue.name })
            }
          </div>
        </div>
        <CloseBtn
          className="venue-info__header__close-btn"
          onClick={deselectVenue}
          theme={theme}
        />
      </div>
      <div className="venue-info__body">
        {(!venue.isService() && !venue.crewOnly) &&
          <div className="venue-info__body__block">
            <img className="venue-info__img" src={venue.getThumbnail()} alt={venue.name} />
          </div>
        }
        <div className="venue-info__body__block">
          <button className="venue-info__direction-btn" onClick={() => changePage(`position`)}>
            <div className="venue-info__direction-btn__title">
              <FormattedMessage id="directions" defaultMessage="directions" />
            </div>
            <div className="venue-info__direction-btn__time">
              {venue.time &&
                <FormattedMessage
                  id="time-min-Walk"
                  defaultMessage="{time} min. Walk"
                  values={{ time: Math.round(venue.time) }}
                />
              }
            </div>
          </button>

          <button
            className="venue-info__details-btn"
            onClick={() => window.provideInfoToShell('wayfinder.VIEW_VENUE_DETAILS', { venue: venue.toObject() })}
          >
            <FormattedMessage id="View-details" defaultMessage="View details" />
          </button>
        </div>
      </div>
    </div>
  )
}

export default injectIntl(VenueInfo)
