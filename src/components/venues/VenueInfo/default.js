import React from 'react'
import { FormattedMessage, injectIntl } from 'react-intl'
import './index.scss'
import 'src/styles/HomeHint.scss'
import { ORIENTATION_LANDSCAPE, SECTOR_ID_TO_DECKS } from 'src/constants'
import classnames from 'classnames'

import SVG from 'src/components/common/SVG'
import DynamicFormattedMessage from 'src/components/common/DynamicFormattedMessage'
import closeSvg from 'public/images/close.svg'
import decksIconSvg from 'public/images/decks-icon.svg'
import timeIconSvg from 'public/images/time-icon.svg'
import searchNearMeSvg from 'public/images/search-near-me.svg'

const VenueInfo = ({ modalProps, intl }) => {
  const { venue, deselectVenue, changePage, orientation, user } = modalProps
  if (!venue) {
    return null
  }

  return (
    <div className={classnames('VenueInfo HomeHint', { landscape: orientation === ORIENTATION_LANDSCAPE })}>
      <div className="head">
        <SVG>{venue.getDefaultCategoryIcon()}</SVG>
        <div className='name es'><DynamicFormattedMessage id={`venue.${venue.id}`} defaultMessage={venue.name} />{user.cabin === venue.id && ` - ${intl.formatMessage({ id: 'My-cabin', defaultMessage: 'My cabin' })}`}</div>
        <button onClick={() => deselectVenue()}>
          <SVG>{closeSvg}</SVG>
        </button>
      </div>
      <div className="content">
        <div className="info-wrap">
          <div className="info">
            <div className='deck'>
              <SVG>{decksIconSvg}</SVG>
              <FormattedMessage id="Deck-N" defaultMessage="Deck {number}" values={{ number: SECTOR_ID_TO_DECKS[venue.sectorId] }} />
            </div>
            {(!venue.isService() && !venue.isCabin()) &&
                            <div className="work">
                              <SVG>{timeIconSvg}</SVG>
                              <FormattedMessage
                                id="closes-at"
                                defaultMessage="Closes at: {time}"
                                values={{ time: intl.formatTime((new Date()).setHours(18, 0, 0)) }}
                              />
                            </div>
            }
          </div>
          {true &&
                        <button
                          onClick={(e) => {
                            e.preventDefault()
                            changePage(`position`)
                          }}
                        >
                          <span><FormattedMessage id="directions" defaultMessage="directions" /></span>
                          <FormattedMessage id="distance-m" defaultMessage="{distance} m" values={{ distance: 5 }} />
                        </button>
          }
          {false &&
                        <button
                          className='here'
                        >
                          <SVG>{searchNearMeSvg}</SVG>
                            You are here
                        </button>
          }
        </div>
        {(!venue.isService() && !venue.crewOnly) &&
                    <div className="images-wrap">
                      <div className="images-innner">
                        <img src={`/public/images/venues/${venue.id}_1.jpg`} alt=""/>
                        <img src={`/public/images/venues/${venue.id}_2.jpg`} alt=""/>
                        <img src={`/public/images/venues/${venue.id}_3.jpg`} alt=""/>
                      </div>
                    </div>
        }
      </div>
    </div>
  )
}

export default injectIntl(VenueInfo)
