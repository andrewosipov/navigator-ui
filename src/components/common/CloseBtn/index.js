import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { THEMES } from 'src/constants'
import SVG from 'src/components/common/SVG'
import closeSvg from 'public/images/close-2.svg'
import './index.scss'

const CloseBtn = ({ className, theme, ...props }) => (
  <button className={cn('close-btn', `close-btn--theme-${theme}`, className)} {...props}>
    <SVG className="close-btn__icon">{closeSvg}</SVG>
  </button>
)

CloseBtn.propTypes = {
  className: PropTypes.string,
  theme: PropTypes.string
}

CloseBtn.defaultProps = {
  theme: THEMES.DARK
}

export default CloseBtn
