/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'

import Button from './index'

describe('Button', () => {
  it('should render self', () => {
    const component = shallow(
      <Button>Test</Button>
    )

    expect(component.hasClass('active')).toBe(false)
    expect(component.text()).toBe('Test')
  })

  it('should render active button', () => {
    const component = shallow(
      <Button isActive>Test</Button>
    )
    expect(component.hasClass('active')).toBe(true)
    expect(component.find('svg').exists()).toBe(true)
  })
})
