import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import './Button.scss'

import SVG from '../SVG'
import acceptSvg from '../../../../public/images/accept.svg'

const Button = ({ children, isActive, ...props }) => (
  <button
    className={cn('custom-btn', { active: isActive })}
    {...props}
  >
    { isActive && <SVG className="accept-icon">{acceptSvg}</SVG> }
    {children}
  </button>
)

Button.propTypes = {
  children: PropTypes.node,
  isActive: PropTypes.bool
}

Button.defaultProps = {
  isActive: false
}

export default Button
