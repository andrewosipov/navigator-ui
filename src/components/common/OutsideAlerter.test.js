import React from 'react'
import { mount } from 'enzyme'

import OutsideAlerter from './OutsideAlerter'

test('OutsideAlerter', () => {
    let events = []
    let handlers = []
    let outside = false
    let el = <div>Some</div>
    let origAddEventListener = document.addEventListener
    document.addEventListener = function(evt, handler) {
        events.push(evt)
        handlers.push(handler)
    }

    let origRemoveEventListener = document.removeEventListener
    document.removeEventListener = function(evt, handler) {
        events = events.filter(e => e !== evt)
        handlers = handlers.filter(h => h !== handler)
    }

    let component = mount(
        <OutsideAlerter className="test-outisde" outside={() => outside = true}>
            {el}
        </OutsideAlerter>
    )
    expect(component.hasClass('test-outisde')).toBe(true)

    expect(events).toContain('mousedown')
    expect(events).toContain('touchend')
    expect(handlers.length).toBeGreaterThan(0)

    expect(outside).toBe(false)
    handlers[0]({})
    expect(outside).toBe(true)

    component.unmount()
    expect(handlers.length).toBe(0)
    expect(events.length).toBe(0)

    document.addEventListener = origAddEventListener
    document.removeEventListener = origRemoveEventListener
})