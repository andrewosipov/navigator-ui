import React from 'react'
import { FormattedMessage } from 'react-intl'
import cn from 'classnames'
import Notify from 'src/components/common/Notify'
import SVG from 'src/components/common/SVG'
import noConnectionIcon from './no-connection.svg'
import './NoConnection.scss'

const NoConnection = ({ theme, ...props }) => (
  <Notify {...props} theme={theme}>
    <SVG className={cn('no-connection-icon', `no-connection-icon--theme-${theme}`)}>
      {noConnectionIcon}
    </SVG>
    <FormattedMessage id="Connection-problems" defaultMessage="Connection problems" />
  </Notify>
)

export default NoConnection
