import React from 'react'

class OutsideAlerter extends React.Component {

  componentDidMount() {
    ['mousedown', 'touchend'].forEach(evt => document.addEventListener(evt, this.handleClickOutside, false))
  }

  componentWillUnmount() {
    ['mousedown', 'touchend'].forEach(evt => document.removeEventListener(evt, this.handleClickOutside))
  }

  handleClickOutside = (event) => {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.props.outside()
    }
  }

  render() {
    return (
      <div className={this.props.className} onClick={() => this.props.onClick && this.props.onClick()} ref={ref => this.wrapperRef = ref}>{this.props.children}</div>
    )
  }
}

export default OutsideAlerter
