import React, { Component } from 'react'
import classnames from 'classnames'

import '../../styles/Elevator.scss'


export default ({ down = false, withoutArrows = false, height = undefined }) => (
    <div className={classnames('Elevator Stair', { down: down, 'without-arrows': withoutArrows })}>
        <div className="lift-tunnel" style={{ height }}></div>
        <img className='lift-icon' src={`/public/images/directions/stair-${down ? 'down' : 'up'}.svg`} alt=""/>
        <div className="stair-rectangle-bottom"></div>
        <div className="lift-circle-dark"></div>
        {!withoutArrows &&
            <div className="triangles">
                <div className="triangles-images-container">
                    <img src="public/images/directions/triangle.svg" className="triangle-try2" />
                    <img src="public/images/directions/triangle.svg" className="triangle-try2" />
                    <img src="public/images/directions/triangle.svg" className="triangle-try2" />
                </div>
            </div>
        }
    </div>
)