import React, { Component } from 'react'
import { CSSTransition } from 'react-transition-group'
import classnames from 'classnames'
import OutsideAlerter from './OutsideAlerter'
import Restrooms from 'src/components/search/Restrooms'
import Cabins from 'src/components/search/Cabins'
import VenueInfo from 'src/containers/VenueInfo'
import SelectDeck from 'src/containers/SelectDeck'
import MyParty from 'src/containers/MyParty'
import Directions from 'src/containers/tv/Directions'

const MODAL_COMPONENTS = {
  'RESTROOMS': Restrooms,
  'CABINS': Cabins,
  'VENUE_INFO': VenueInfo,
  'SELECT_DECK': SelectDeck,
  'MY_PARTY': MyParty,
  'DIRECTIONS': Directions
}

class Modal extends Component {
    hideModal = () => {
      if (this.props.modalType !== 'VENUE_INFO') {
        this.props.hideModal()
      }
    }

    getNotBg () {
      return !this.props.modalType || (this.props.modalType === 'VENUE_INFO')
    }

    isModal () {
      if (this.props.modalType === 'VENUE_INFO') {
        return this.props.page === 'home'
      }
      return !!this.props.modalType
    }

    render () {
      const SpecificModal = MODAL_COMPONENTS[this.props.modalType]
      return (
        <CSSTransition
          in={this.isModal()}
          timeout={350}
          classNames='fade'
          unmountOnExit
        >
          <div className={classnames('overlay', { notbg: this.getNotBg() })}>
            <OutsideAlerter className='outside-alerter' outside={this.hideModal}>
              {!!this.props.modalType && <SpecificModal {...this.props} />}
            </OutsideAlerter>
          </div>
        </CSSTransition>
      )
    }
}

export default Modal
