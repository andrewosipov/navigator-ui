import React from 'react'
import { FormattedMessage } from 'react-intl'
import Notify from '../Notify'
import Spinner from '../Spinner'

const TryingToConnect = ({ theme, ...props }) => (
  <Notify {...props} theme={theme}>
    <Spinner size="sm" theme={theme} />
    <FormattedMessage id="Trying-to-connect" defaultMessage="Trying to connect…" />
  </Notify>
)

export default TryingToConnect
