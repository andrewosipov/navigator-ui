import React from 'react'
import cn from 'classnames'
import SVG from 'src/components/common/SVG'
import magnifier from './magnifier.svg'
import './Search.scss'

const Search = ({ className, ...props }, ref) => (
  <div className={cn('search-wrapper', className)}>
    <SVG className="icon">{magnifier}</SVG>
    <input {...props} ref={ref} />
  </div>
)

export default React.forwardRef(Search)
