import React from 'react'
import { shallow } from 'enzyme'

import Stair from './Stair'

test('Stair', () => {
    let component = shallow(
        <Stair />
    )
    expect(component.hasClass('Elevator')).toBe(true)
    expect(component.find('.triangles').exists()).toBe(true)
    expect(component.hasClass('down')).toBe(false)

    component = shallow(
        <Stair down />
    )
    expect(component.hasClass('down')).toBe(true)

    component = shallow(
        <Stair withoutArrows />
    )
    expect(component.find('.triangles').exists()).toBe(false)
})