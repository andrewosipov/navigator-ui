import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { THEMES } from 'src/constants'
import './Spinner.scss'
import SVG from '../SVG'
import spinnerIcon from './spinner.svg'

const Spinner = ({ className, size, theme, ...props }) => (
  <SVG className={cn('spinner', `spinner--theme-${theme}`, size, className)} {...props}>
    {spinnerIcon}
  </SVG>
)

Spinner.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(['sm', 'md']),
  theme: PropTypes.string
}

Spinner.defaultProps = {
  size: 'md',
  theme: THEMES.DARK
}

export default Spinner
