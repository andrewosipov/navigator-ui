import React from 'react'
import SVG from 'src/components/common/SVG'
import mStationSvg from 'public/images/bliss/m-station.svg'

import './index.scss'

const MusterStationBtn = props => (
  <button className="muster-station-btn" {...props}>
    <SVG className="muster-station-btn__icon">{mStationSvg}</SVG>
  </button>
)

export default MusterStationBtn
