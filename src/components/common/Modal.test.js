/* eslint-env jest */
import React from 'react'
import { mount } from 'enzyme'
import Modal from './Modal'
import data from 'src/helpers/tests/data'

test('Modal', () => {
  let component = mount(
    <Modal
      modalType="VENUE_INFO"
      page="home"
      modalProps={{ venue: data.venues[0], user: data.user }}
    />
  )
  expect(component.find('.overlay').exists()).toBe(true)

  component = mount(
    <Modal
      modalType="RESTROOMS"
      page="home"
    />
  )
  expect(component.find('.overlay').exists()).toBe(true)
})
