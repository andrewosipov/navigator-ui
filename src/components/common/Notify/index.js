import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { THEMES } from 'src/constants'
import './Notify.scss'

const Notify = ({ className, withBackground, inCenter, isAbsolute, isFullScreen, theme, ...props }) => (
  <div
    className={cn('notify', `notify--theme-${theme}`, className, { withBackground, inCenter, isAbsolute, isFullScreen })}
    {...props}
  />
)

Notify.propTypes = {
  className: PropTypes.string,
  withBackground: PropTypes.bool,
  inCenter: PropTypes.bool,
  isAbsolute: PropTypes.bool,
  isFullScreen: PropTypes.bool,
  theme: PropTypes.string
}

Notify.defaultProps = {
  withBackground: false,
  inCenter: false,
  isAbsolute: false,
  isFullScreen: false,
  theme: THEMES.DARK
}

export default Notify
