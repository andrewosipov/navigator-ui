import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { THEMES } from 'src/constants'
import Spinner from 'src/components/common/Spinner'
import './Loader.scss'

const Loader = ({ className, children, withBackground, isBlocking, theme, ...props }) => (
  <div className={cn('loader', `loader--theme-${theme}`, className, { withBackground, isBlocking })} {...props}>
    <Spinner theme={theme} />
    <div className="loader-content">
      {children}
    </div>
  </div>
)

Loader.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  withBackground: PropTypes.bool,
  isBlocking: PropTypes.bool,
  theme: PropTypes.string
}

Loader.defaultProps = {
  withBackground: false,
  isBlocking: false,
  theme: THEMES.DARK
}

export default Loader
