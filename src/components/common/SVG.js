import React, { Component } from 'react'

export default ({ children, className }) => (
    <span className={className} dangerouslySetInnerHTML={{ __html: children }}></span>
)
