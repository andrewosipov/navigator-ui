import React from 'react'
import { FormattedMessage } from 'react-intl'

const DynamicFormattedMessage = (props) => <FormattedMessage {...props} />

export default DynamicFormattedMessage
