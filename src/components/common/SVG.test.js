import React from 'react'
import { shallow } from 'enzyme'

import SVG from './SVG'

test('SVG', () => {
    let el = "<svg><g></g></svg>"
    let component = shallow(
        <SVG className="test-svg">{el}</SVG>
    )
    expect(component.hasClass('test-svg')).toBe(true)
    expect(component.html().includes(el)).toBe(true)
})