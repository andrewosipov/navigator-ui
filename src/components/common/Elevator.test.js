import React from 'react'
import { shallow } from 'enzyme'

import Elevator from './Elevator'
import data from '../../helpers/tests/data'

test('Elevator', () => {
    let component = shallow(
        <Elevator />
    )
    expect(component.hasClass('Elevator')).toBe(true)
    expect(component.find('.triangles').exists()).toBe(true)
    expect(component.hasClass('down')).toBe(false)

    component = shallow(
        <Elevator down />
    )
    expect(component.hasClass('down')).toBe(true)

    component = shallow(
        <Elevator withoutArrows />
    )
    expect(component.find('.triangles').exists()).toBe(false)
})