import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'

import DynamicDeck from './DynamicDeck'
import Deck from './Deck'
import data from '../helpers/tests/data'
import { store } from '../store'

test('DynamicDeck', () => {
  let state = {}
  let component = mount(
    <Provider store={store}>
      <DynamicDeck
        offsetX={0}
        offsetY={0}
        zoom={1}
        perspectiveZoom={1}
        rotation={0}
        perspectiveRotation={0}
        stickToUser

        selectedVenue={null}
        endVenue={null}
        crewOnly={false}
        selectedPaths={[]}
        user={data.user}
        party={[]}

        changeOffsetX={(offset) => state.offsetX = offset}
        changeOffsetY={(offset) => state.offsetY = offset}
        changeRotation={(rotation) => state.rotation = rotation}
        changeZoom={(zoom) => state.zoom = zoom}
        changeDeck={(deck) => state.deck = deck}
      />
    </Provider>
  )

  expect(component.find('.DynamicDeck').exists()).toBe(true)
  expect(component.find(Deck).exists()).toBe(true)
})
