import React, { Component } from 'react'
import PropTypes from 'prop-types'
import $ from 'jquery'
import classnames from 'classnames'
import { FormattedMessage } from 'react-intl'

import 'src/styles/Home.scss'
import DynamicDeck from 'src/containers/DynamicDeck'
import Modal from 'src/containers/Modal'
import Search from 'src/components/common/Search'
import { toInt } from 'src/helpers/misc'

import {
  ORIENTATION_LANDSCAPE,
  ORIENTATION_PORTRAIT,
  PAGES
} from 'src/constants'

class Home extends Component {
  static propTypes = {
    deck: PropTypes.number,
    debug: PropTypes.bool.isRequired,
    crewOnly: PropTypes.bool.isRequired,
    mapMode: PropTypes.string.isRequired,
    selectedVenue: PropTypes.object,
    directionTo: PropTypes.object,
    modalType: PropTypes.string,
    venues: PropTypes.array.isRequired,
    isVenuesFetching: PropTypes.bool.isRequired,
    user: PropTypes.object,
    theme: PropTypes.string,
    connectionType: PropTypes.string.isRequired,
    changePage: PropTypes.func.isRequired,
    selectVenue: PropTypes.func.isRequired,
    deselectVenue: PropTypes.func.isRequired,
    selectTarget: PropTypes.func.isRequired,
    showModal: PropTypes.func.isRequired,
    hideModal: PropTypes.func.isRequired,
    changeMapMode: PropTypes.func.isRequired
  }

  constructor (props) {
    super(props)
    this.state = {
      orientation: this.getOrientation(),
      deckContainerHeight: 0,
      deckContainerWidth: 0,
      showNavBottom: false
    }
    this.addEventListeners()
  }

  addEventListeners () {
    const updateDeckContainerSizeAndOrientation = () => {
      this.setState({ orientation: this.getOrientation() })
      this.updateDeckContainerSize()
    }

    this.onOrientationChange = () => {
      return setTimeout(() => {
        updateDeckContainerSizeAndOrientation()
      }, 500)
    }

    if ('onorientationchange' in window) {
      window.addEventListener('orientationchange', this.onOrientationChange, false)
    }

    window.onresize = updateDeckContainerSizeAndOrientation
  }

  componentWillUnmount () {
    window.onresize = () => {}
    window.removeEventListener('orientationchange', this.onOrientationChange)
    clearTimeout(this.showNavBottom)
  }

  componentDidUpdate () {
    if (this.props.directionTo) {
      this.props.showModal('VENUE_INFO', {
        orientation: this.state.orientation,
        venue: this.props.directionTo,
        changePage: this.props.changePage,
        deselectVenue: this.props.deselectVenue,
        user: this.props.user
      })
    } else {
      if (this.props.modalType === 'VENUE_INFO') {
        this.props.hideModal()
      }
    }
  }

  componentDidMount () {
    this.showNavBottom = setTimeout(() => this.setState({
      showNavBottom: true
    }), 1350)
  }

  getOrientation () {
    return window.innerHeight > window.innerWidth ? ORIENTATION_PORTRAIT : ORIENTATION_LANDSCAPE
  }

  initDeckContainer (el) {
    if (!this.deckContainerWrapper && el) {
      this.deckContainerWrapper = el
      this.updateDeckContainerSize()
    }
  }

  updateDeckContainerSize () {
    this.setState({
      deckContainerWidth: $(this.deckContainerWrapper).width(),
      deckContainerHeight: $(this.deckContainerWrapper).height()
    })
  }

  toggleSelectDeck = (e) => {
    if (e) {
      e.preventDefault()
    }
    this.props.deselectVenue()
    this.props.showModal('SELECT_DECK')
  }

  findEmbarkation () {
    let venue = this.props.venues.find(v => toInt(v.id) === 3486)
    if (venue) {
      this.props.selectTarget(venue)
      this.props.changePage(PAGES.POSITION)
    }
  }

  handleSearchClick = () => {
    this.props.changePage(PAGES.SEARCH)
  }

  render () {
    const { crewOnly } = this.props
    return (
      <div
        className={classnames('Home', {
          'crew-only': crewOnly,
          'landscape': this.state.orientation === ORIENTATION_LANDSCAPE
        })}
        style={{ height: $(window).height() }}
      >
        <div className="Home__bliss-search" onClick={this.handleSearchClick}>
          <FormattedMessage id="Search-placeholder" defaultMessage="Where do you want to go?">
            {
              (placeholder) => <Search placeholder={placeholder} />
            }
          </FormattedMessage>
        </div>
        <div className="deck-container-wrapper" ref={(el) => this.initDeckContainer(el)}>
          {!!this.state.deckContainerHeight &&
            <DynamicDeck
              height={this.state.deckContainerHeight}
              width={this.state.deckContainerWidth}
              orientation={this.state.orientation}
              showOverviewButton
              showDeckButton
              showInfoButton
              hideVenues
            />
          }
        </div>

        <Modal />
      </div>
    )
  }
}

export default Home
