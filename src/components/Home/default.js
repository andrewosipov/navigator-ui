import React, { Component } from 'react'
import $ from 'jquery'
import classnames from 'classnames'
import { FormattedMessage } from 'react-intl'

import 'src/styles/Home.scss'
import DynamicDeck from 'src/containers/DynamicDeck'
import NavBottom from 'src/containers/NavBottom'
import Modal from 'src/containers/Modal'
import { toInt } from 'src/helpers/misc'

import shareSvg from 'public/images/share.svg'
import backSvg from 'public/images/back.svg'

import SVG from 'src/components/common/SVG'
import TryingToConnect from 'src/components/common/TryingToConnect'
import NoConnection from 'src/components/common/NoConnection'
import Loader from 'src/components/common/Loader'
import {
  ORIENTATION_LANDSCAPE,
  ORIENTATION_PORTRAIT,
  CONNECTION_TYPES,
  PAGES
} from 'src/constants'

class Home extends Component {
  constructor (props) {
    super(props)
    this.state = {
      orientation: this.getOrientation(),
      deckContainerHeight: 0,
      deckContainerWidth: 0,
      showNavBottom: false
    }
    this.addEventListeners()
  }

  addEventListeners () {
    const updateDeckContainerSizeAndOrientation = () => {
      this.setState({ orientation: this.getOrientation() })
      this.updateDeckContainerSize()
    }

    this.onOrientationChange = () => {
      return setTimeout(() => {
        updateDeckContainerSizeAndOrientation()
      }, 500)
    }

    if ('onorientationchange' in window) {
      window.addEventListener('orientationchange', this.onOrientationChange, false)
    }

    window.onresize = updateDeckContainerSizeAndOrientation
  }

  componentWillUnmount () {
    window.onresize = () => {}
    window.removeEventListener('orientationchange', this.onOrientationChange)
    clearTimeout(this.showNavBottom)
  }

  componentDidUpdate () {
    if (this.props.directionTo) {
      this.props.showModal('VENUE_INFO', {
        orientation: this.state.orientation,
        venue: this.props.directionTo,
        changePage: this.props.changePage,
        deselectVenue: this.props.deselectVenue,
        user: this.props.user
      })
    } else {
      if (this.props.modalType === 'VENUE_INFO') {
        this.props.hideModal()
      }
    }
  }

  componentDidMount () {
    this.showNavBottom = setTimeout(() => this.setState({
      showNavBottom: true
    }), 1350)
  }

  getOrientation () {
    return window.innerHeight > window.innerWidth ? ORIENTATION_PORTRAIT : ORIENTATION_LANDSCAPE
  }

  initDeckContainer (el) {
    if (!this.deckContainerWrapper && el) {
      this.deckContainerWrapper = el
      this.updateDeckContainerSize()
    }
  }

  updateDeckContainerSize () {
    this.setState({
      deckContainerWidth: $(this.deckContainerWrapper).width(),
      deckContainerHeight: $(this.deckContainerWrapper).height()
    })
  }

  toggleSelectDeck = (e) => {
    if (e) {
      e.preventDefault()
    }
    this.props.deselectVenue()
    this.props.showModal('SELECT_DECK')
  }

  findEmbarkation () {
    let venue = this.props.venues.find(v => toInt(v.id) === 3486)
    if (venue) {
      this.props.selectTarget(venue)
      this.props.changePage(PAGES.POSITION)
    }
  }

  render () {
    const { connectionType, isVenuesFetching } = this.props

    return (
      <div
        className={classnames('Home', {
          'crew-only': this.props.crewOnly,
          'landscape': this.state.orientation === ORIENTATION_LANDSCAPE
        })}
        style={{ height: $(window).height() }}
      >
        <div className="navtop">
          <button
            className="back"
            onClick={() => {
              let actionData = {
                code: 'PRE',
                supportiveData: {}
              }
              window.provideInfoToShell('invokeModule', actionData)
            }}
          >
            <SVG>{backSvg}</SVG>
          </button>
          <a
            className='decks'
            id="home__deck-selector"
            onClick={(e) => this.toggleSelectDeck(e)}
          >
            <img src="/public/images/decks-icon.svg" alt=""/>
            <span>
              <FormattedMessage
                id="Deck-N"
                defaultMessage="Deck {number}"
                values={{
                  number: this.props.deck
                }}
              />
            </span>
            <img src="/public/images/navtop-arrows.svg" alt=""/>
          </a>
          <button className="share" onClick={() => this.props.showModal('MY_PARTY')}>
            <SVG>{shareSvg}</SVG>
          </button>
        </div>

        <div className="deck-container-wrapper" ref={(el) => this.initDeckContainer(el)}>
          {!!this.state.deckContainerHeight &&
            <DynamicDeck
              height={this.state.deckContainerHeight}
              width={this.state.deckContainerWidth}
              orientation={this.state.orientation}
              showOverviewButton
            />
          }
        </div>

        {connectionType === CONNECTION_TYPES.UNKNOWN &&
          <TryingToConnect
            className="home-notify"
            isAbsolute
            withBackground
            theme={this.props.theme}
          />
        }

        {connectionType === CONNECTION_TYPES.NONE &&
          <NoConnection
            className="home-notify"
            isAbsolute
            withBackground
            theme={this.props.theme}
          />
        }

        {isVenuesFetching && <Loader withBackground theme={this.props.theme} />}

        <NavBottom
          showNavBottom={this.state.showNavBottom}
          orientation={this.state.orientation}
          findRestroom={(gender, invalid) => this.props.findRestroom(gender, invalid)}
          findEmbarkation={() => this.findEmbarkation()}
        />

        <Modal />

      </div>
    )
  }
}

export default Home
