/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import CatalogItem from './CatalogItem'
import data from 'src/helpers/tests/data'

test('CatalogItem', () => {
  let venue = data.venues[10]
  let changePage = jest.fn()
  let selectTarget = jest.fn()
  let component = shallow(
    <CatalogItem
      venue={venue}
      changePage={changePage}
      selectTarget={selectTarget}
    />
  )

  expect(component.find('.descr .name').text()).toBe(venue.name)
  expect(component.find('.descr .info').exists()).toBe(true)
  component.simulate('click')
  expect(changePage).toHaveBeenCalled()
  expect(selectTarget).toHaveBeenCalled()
})
