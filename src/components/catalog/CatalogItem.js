import React from 'react'
import { FormattedMessage, injectIntl } from 'react-intl'
import { SECTOR_ID_TO_DECKS, PAGES } from 'src/constants'
import nextArrowSvg from 'public/images/next-arrow.svg'
import SVG from 'src/components/common/SVG'
import DynamicFormattedMessage from 'src/components/common/DynamicFormattedMessage'

const getImages = ({ crewOnly, venue }) => {
  return crewOnly ? null
    : <div className="images-wrapper">
      <div className="image-container">
        <img src={`/public/images/venues/${venue.id}_1.jpg`} />
      </div>
      <div className="next-images-wrapper">
        <div className="image-container">
          <img src={`/public/images/venues/${venue.id}_2.jpg`} />
        </div>
        <div className="image-container">
          <img src={`/public/images/venues/${venue.id}_3.jpg`} />
        </div>
      </div>
    </div>
}

const getInfo = ({ crewOnly, venue, formatTime }) => {
  if (crewOnly) {
    return <FormattedMessage
      id="works-till"
      defaultMessage="Works till {time}"
      values={{ time: formatTime((new Date()).setHours(18, 0, 0)) }}
    />
  }

  return <FormattedMessage
    id="time-min"
    defaultMessage="{time} min."
    values={{ time: Math.round(venue.time) }}
  />
}

const getRepresentations = (venue) => {
  if (!venue.representations.length) {
    return null
  }

  return <div className="name">
    {venue.representations.map((r, index) =>
      <div key={index} className="name-value">
        <DynamicFormattedMessage id={`representation.${r.id}`} defaultMessage={r.name} />
      </div>
    )}
  </div>
}

const CatalogItem = ({ venue, crewOnly, changePage, selectTarget, intl }) => (
  <div
    className='item'
    key={venue.id}
    onClick={() => {
      selectTarget(venue)
      changePage(PAGES.POSITION, false)
    }}
  >
    {getImages({ crewOnly, venue })}
    <div className="descr">
      {crewOnly && getRepresentations(venue)}
      <div className="name">
        <DynamicFormattedMessage
          id={`venue.${venue.id}`}
          defaultMessage={venue.name}
        />
      </div>
      <div className="info">
        <div className='ttu'>
          <FormattedMessage
            id="Deck-N"
            defaultMessage="Deck {number}"
            values={{
              number: SECTOR_ID_TO_DECKS[venue.sectorId]
            }}
          />
        </div>
        {/* hide if user and add class "error" if closed */}
        <span className='middot'>&middot;</span>
        <div>
          {getInfo({ crewOnly, venue, formatTime: intl.formatTime })}
        </div>
        <SVG className="next-arrow">{nextArrowSvg}</SVG>
      </div>
    </div>
  </div>
)

export default injectIntl(CatalogItem)
