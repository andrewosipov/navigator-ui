import React from 'react'
import { mount } from 'enzyme'

import Catalog from './Catalog'
import data from '../../helpers/tests/data'

test('Catalog', () => {
  let goBack = jest.fn()
  let changePage = jest.fn()
  let fetchVenues = jest.fn()
  let selectTarget = jest.fn()
  let component = mount(
    <Catalog
      categories={data.categories}
      venues={data.venues.slice(1)}
      user={data.user}
      crewOnly={false}
      fetchVenues={fetchVenues}
      goBack={goBack}
      changePage={changePage}
      selectTarget={selectTarget}
      windowSize={{ width: 400, height: 400 }}
    />
  )

  let categories = data.categories.filter(c => !c.isService)
  expect(fetchVenues).toHaveBeenCalled()
  expect(component.find('.Catalog').exists()).toBe(true)
  expect(component.find('.tabs .tab')).toHaveLength(categories.length)
  component.find(`.head .search`).simulate('click')
  expect(changePage).toHaveBeenCalled()

  component.find(`.head .back`).simulate('click')
  expect(goBack).toHaveBeenCalled()
})
