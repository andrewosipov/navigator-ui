import $ from 'jquery'
import React, { Component } from 'react'
import classNames from 'classnames'
import { FormattedMessage } from 'react-intl'

import 'src/styles/Catalog.scss'
import backSvg from 'public/images/back.svg'
import catalogSearchSvg from 'public/images/catalog-search.svg'
import SVG from 'src/components/common/SVG'
import { toInt } from 'src/helpers/misc'
import { ICON_COLORS, CONNECTION_TYPES, PAGES } from 'src/constants'
import CatalogItem from './CatalogItem'
import DynamicFormattedMessage from 'src/components/common/DynamicFormattedMessage'
import TryingToConnect from 'src/components/common/TryingToConnect'
import NoConnection from 'src/components/common/NoConnection'

class Catalog extends Component {
  constructor (props) {
    super(props)

    let categories = this.props.categories.filter(category => !category.isService)
    let activeCategoryId = this.props.selected || categories[0].id

    this.fetchVenues(activeCategoryId)

    // it needs a more beauty solution
    if (this.props.crewOnly) {
      categories.forEach(category => {
        category.crewVenuesCount = 0
        category.sailorVenuesCount = 0

        this.props.venues.forEach(venue => {
          if (venue.categories.find(cat => cat.id === category.id)) {
            venue.crewOnly && category.crewVenuesCount++
            !venue.crewOnly && category.sailorVenuesCount++
          }
        })
      })
      if (this.props.crewOnlyCategories) {
        categories = categories.filter(category => category.crewVenuesCount > 0)
      } else {
        categories = categories.filter(category => category.sailorVenuesCount > 0 && !category.crewOnly)
      }
    }

    this.state = {
      activeCategoryId,
      categories
    }
  }

  async fetchVenues (activeCategoryId) {
    // const { activeCategoryId } = this.state
    await this.props.fetchVenues(this.props.user, this.props.crewOnly)
    this.venuesTimeout = setTimeout(() => {
      let wrapperOffset = $(`#category-wrapper-${activeCategoryId}`).offset()
      let tabContentOffset = $('.tab-content').offset()
      if (wrapperOffset && tabContentOffset) {
        $(this.tabs).scrollTop($('.tab-content').scrollTop() + (wrapperOffset.top - tabContentOffset.top))
      }
    }, 100)
  }

  componentDidMount () {
    let pos = $(`#category-${this.state.activeCategoryId}`).position()
    if (pos) {
      $('.tabs').scrollLeft(pos.left - 110)
    }

    this.canChangeCategory = true
    this.tabs.addEventListener('scroll', evt => {
      if (this.canChangeCategory) {
        if (evt.srcElement.scrollHeight - evt.srcElement.offsetHeight === evt.srcElement.scrollTop) {
          const { categories } = this.state
          const last = categories[categories.length - 1]
          this.setState({ activeCategoryId: last.id })
          return
        }

        let docViewTop, docViewBottom, elNextTop, elNextBottom, elPrevBottom, elPrevTop

        docViewTop = $(window).scrollTop()
        docViewBottom = docViewTop + $(window).height() - $('.tab-content').height() + 15

        if ($('.tab-content').scrollTop() >= 30) {
          $(this.tabsHeader).addClass('little')
        } else {
          $(this.tabsHeader).removeClass('little')
        }

        $('.category-wrapper').each((_, el) => {
          elNextTop = $(el).offset().top
          elNextBottom = elNextTop + 10
          elPrevBottom = $(el).offset().top + $(el).height() - 30
          elPrevTop = elPrevBottom + 10
          const seeTop = (elNextBottom <= docViewBottom) && (elNextTop >= docViewTop)
          const seeBottom = (elPrevBottom <= docViewBottom) && (elPrevTop >= docViewTop)

          if (seeTop || seeBottom) {
            const id = toInt($(el).attr('id').replace('category-wrapper-', ''))
            if (this.state.activeCategoryId !== id) {
              this.setState({ activeCategoryId: id })
              $('.tabs').animate({
                scrollLeft: $('.tabs').scrollLeft() + ($(`#category-${id}`).offset().left - $('.tabs').offset().left) - 110
              }, 0)
            }
          }
        })
      }
    })
  }

  componentWillUnmount () {
    clearTimeout(this.venuesTimeout)
  }

  selectCategory (activeCategoryId) {
    clearTimeout(this.timer)
    this.setState({ activeCategoryId })
    this.canChangeCategory = false
    const scrollTop = $('.tab-content').scrollTop() + ($(`#category-wrapper-${activeCategoryId}`).offset().top - $('.tab-content').offset().top)
    let time = Math.abs(scrollTop - $(this.tabs).scrollTop()) / 10
    time = time < 350 ? 350 : time
    $(this.tabs).animate({ scrollTop }, time)
    this.timer = setTimeout(() => {
      this.canChangeCategory = true
    }, time + 50)
  }

  getIconColor (active, color) {
    if (this.props.theme === 'dark') {
      return active ? color : 'rgba(255,255,255,0.7)'
    }

    return 'rgba(30,31,37,1)'
  }

    getEmptySpace = () => {
      const { categories } = this.state
      if (categories.length) {
        const last = categories[categories.length - 1]
        const items = [...this.props.venues]
          .filter(venue => venue.categories.find((cat) => cat.id === last.id))
          .filter(venue => this.props.crewOnlyCategories ? venue.crewOnly : !venue.crewOnly)
        if (items.length) {
          const itemLength = this.props.windowSize.width >= 768
            ? this.props.windowSize.width >= 1366
              ? Math.ceil(items.length / 3) : Math.ceil(items.length / 2)
            : items.length
          const itemMargin = this.props.windowSize.width >= 768 ? 24 : 16
          const height = this.props.windowSize.height - (10 + 16 + ($('.item').outerHeight() + itemMargin) * itemLength + $('.title').outerHeight() + $('.tabs').outerHeight())
          return (
            <div style={{ height: `${Math.max(height, 0)}px` }} />
          )
        }
      } else return false
    }

    render () {
      const { connectionType } = this.props
      return (
        <div className={`Catalog ${classNames({ crewonly: this.props.crewOnly, 'crewonly-catalog': this.props.crewOnlyCategories })}`}>
          <div className="head">
            <button
              className='back'
              onClick={() => this.props.goBack()}
            >
              <SVG>{backSvg}</SVG>
            </button>
            <div className='tit'><FormattedMessage id="Catalog" defaultValue="Catalog" /></div>
            <button
              className='search'
              onClick={() => this.props.changePage(PAGES.SEARCH)}
            >
              <SVG>{catalogSearchSvg}</SVG>
            </button>
            <div ref={ref => { this.tabsHeader = ref }} className="tabs">
              <div className="tabs-inner">
                {this.state.categories.map(category => {
                  const id = category.id
                  const active = id === this.state.activeCategoryId
                  return (
                    <div
                      key={id}
                      id={`category-${id}`}
                      className={classNames('tab', { active })}
                      onClick={() => this.selectCategory(id)}
                      style={{
                        color: ICON_COLORS[id] && this.getIconColor(active, ICON_COLORS[id][0])
                      }}
                    >
                      <SVG>{category.getTabIcon(category.crewOnly)}</SVG>
                      <DynamicFormattedMessage
                        id={`category.${category.id}`}
                        defaultMessage={category.name}
                      />
                      <div
                        className='active-border'
                        style={{
                          display: active ? 'block' : 'none',
                          background: ICON_COLORS[id] && `linear-gradient(to right, ${ICON_COLORS[id][0]}, ${ICON_COLORS[id][1]}`
                        }}
                      />
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
          {connectionType === CONNECTION_TYPES.UNKNOWN &&
            <TryingToConnect
              className="catalog-notify"
              isAbsolute
              withBackground
              theme={this.props.theme}
            />
          }
          {connectionType === CONNECTION_TYPES.NONE &&
            <NoConnection
              className="catalog-notify"
              isAbsolute
              withBackground
              theme={this.props.theme}
            />
          }
          <div ref={ref => { this.tabs = ref }} className="tab-content">
            {this.state.categories.map((category, index) => {
              return (
                <div key={category.id} className='category-wrapper' id={`category-wrapper-${category.id}`}>

                  {index > 0 &&
                    <div className="title">
                      <SVG>{category.getTabIcon(category.crewOnly)}</SVG>
                      <DynamicFormattedMessage
                        id={`category.${category.id}`}
                        defaultMessage={category.name}
                      />
                    </div>
                  }

                  {[...this.props.venues]
                    .filter(venue => venue.categories.find((cat) => cat.id === category.id))
                    .filter(venue => this.props.crewOnlyCategories ? venue.crewOnly : !venue.crewOnly)
                    .sort((a, b) => a.time - b.time)
                    .map(venue =>
                      <CatalogItem
                        key={venue.id}
                        crewOnly={this.props.crewOnly}
                        venue={venue}
                        selectTarget={this.props.selectTarget}
                        changePage={this.props.changePage}
                      />
                    )}
                </div>
              )
            })}
            {this.getEmptySpace()}
          </div>
        </div>
      )
    }
}

export default Catalog
