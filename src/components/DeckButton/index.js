import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import SVG from 'src/components/common/SVG'
import { THEMES } from 'src/constants'

import './index.scss'

const DeckButtons = ({ theme, className, children, ...props }) => (
  <button
    className={cn('deck-button', `deck-button--theme-${theme}`, className)}
    {...props}
  >
    <SVG>{children}</SVG>
  </button>
)

DeckButtons.propTypes = {
  theme: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.node
}

DeckButtons.defaultProps = {
  theme: THEMES.DARK,
  className: ''
}

export default DeckButtons
