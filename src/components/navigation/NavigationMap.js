import React from 'react'

import DynamicDeck from 'src/containers/DynamicDeck'
import { isWideScreen } from 'src/helpers/misc'
import { ORIENTATION_PORTRAIT, DEFAULT_PERSPECTIVE_ROTATION, WIDE_PERSPECTIVE_ROTATION } from '../../constants'

const NavigationMap = ({ initDeckContainer, deckContainerHeight, deckContainerWidth, selected, heading, rotation }) => (
  <div className="deck-container-wrapper" ref={(el) => initDeckContainer(el)}>
    {deckContainerHeight &&
      <DynamicDeck
        height={deckContainerHeight}
        width={deckContainerWidth}
        orientation={ORIENTATION_PORTRAIT}
        selectedVenue={selected}
        perspectiveRotation={isWideScreen() ? WIDE_PERSPECTIVE_ROTATION : DEFAULT_PERSPECTIVE_ROTATION}
        rotation={rotation}

        rotationEnabled={false}
        showSelectedPaths={true}
        displayOnlyNearestVenues={true}
        stickToUser={true}
        selectVenue={null}
        deselectVenue={null}

        showDestinationPoint
        localState
        isNavigation
      />
    }
  </div>
)

export default NavigationMap
