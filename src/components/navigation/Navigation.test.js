import React from 'react'
import { shallow } from 'enzyme'

import Navigation from './Navigation'
import NavigationTip from './NavigationTip'
import NavigationMap from './NavigationMap'
import NavigationInfo from './NavigationInfo'
import data from '../../helpers/tests/data'

test('Navigation', () => {
    window.onresize = null
    let changePage = jest.fn()
    let eraseSelectedDirections = jest.fn()
    let component = shallow(
        <Navigation
            user={data.user}
            venues={data.venues}
            selectedPaths={data.selectedPaths}
            from={data.directions.from}
            to={data.directions.to}
            heading={30}
            changePage={changePage}
            eraseSelectedDirections={eraseSelectedDirections}
        />
    )
    expect(component.hasClass('Navigation')).toBe(true)
    expect(component.find(NavigationTip).exists()).toBe(true)
    expect(component.find(NavigationMap).exists()).toBe(true)
    expect(component.find(NavigationInfo).exists()).toBe(true)
    expect(window.onresize).not.toBeNull()

    component.find('.end-button').simulate('click')
    expect(changePage).toHaveBeenCalled()
    expect(eraseSelectedDirections).toHaveBeenCalled()

    component.unmount()
    expect(window.onresize).toBeNull()
})