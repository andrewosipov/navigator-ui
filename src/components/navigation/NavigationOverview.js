import $ from 'jquery'
import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import classNames from 'classnames'
import 'src/styles/Navigation.scss'
import 'src/styles/Directions.scss'

import Path from 'src/models/Path'
import Decks from 'src/components/directions/Decks'
import NavigationInfo from 'src/containers/navigation/NavigationInfo'
import {
  ORIENTATION_LANDSCAPE,
  ORIENTATION_PORTRAIT,
  CONNECTION_TYPES,
  PAGES
} from 'src/constants'
import NavigationTip from 'src/containers/navigation/NavigationTip'
import TryingToConnect from 'src/components/common/TryingToConnect'
import NoConnection from 'src/components/common/NoConnection'

export default class NavigationOverview extends Component {
  constructor (props) {
    super(props)
    this.state = {
      deckContainerHeight: 0,
      deckContainerWidth: 0,
      orientation: this.getOrientation()
    }

    this.addEventListeners()
  }

  getOrientation () {
    return window.innerHeight > window.innerWidth ? ORIENTATION_PORTRAIT : ORIENTATION_LANDSCAPE
  }

  addEventListeners () {
    this.onOrientationChange = () => {
      return setTimeout(() => {
        this.updateDeckContainerSize()
      }, 500)
    }

    if ('onorientationchange' in window) {
      window.addEventListener('orientationchange', this.onOrientationChange, false)
    }

    window.onresize = this.updateDeckContainerSize
  }

  initDeckContainer (el) {
    if (!this.deckContainerWrapper) {
      this.deckContainerWrapper = el
      this.updateDeckContainerSize()
    }
  }

  updateDeckContainerSize () {
    this.setState({
      deckContainerWidth: $(this.deckContainerWrapper).width(),
      deckContainerHeight: $(this.deckContainerWrapper).height()
    })
  }

  isLandscape () {
    return $(window).width() > $(window).height()
  }

  toHomeScreen () {
    this.props.eraseSelectedDirections()
    this.props.changePage(PAGES.HOME)
  }

  render () {
    const { connectionType } = this.props
    let data = Path.getUserPathData(this.props.user, this.props.selectedPaths, this.props.venues)
    return (
      <div className={classNames('Directions', 'Navigation', { landscape: this.isLandscape() })}>

        <NavigationTip landscape={this.isLandscape()} direction={data.direction} isArrived={data.isArrived} />

        {connectionType === CONNECTION_TYPES.UNKNOWN &&
          <TryingToConnect
            className="navigation-notify"
            isAbsolute
            withBackground
            theme={this.props.theme}
          />
        }

        {connectionType === CONNECTION_TYPES.NONE &&
          <NoConnection
            className="navigation-notify"
            isAbsolute
            withBackground
            theme={this.props.theme}
          />
        }

        <div className="nav-bottom">
          <div className="deck-info">
            <FormattedMessage id="Deck-N" defaultMessage="Deck {number}" values={{ number: this.props.user.deck }} />
          </div>
        </div>

        <div className="deck-container-wrapper" ref={(el) => this.initDeckContainer(el)}>
          <Decks
            venues={this.props.venues}
            selectedPaths={this.props.selectedPaths}
            to={this.props.to}
            user={this.props.user}
            theme={this.props.theme}
            windowSize={this.props.windowSize}
          />
        </div>

        <NavigationInfo
          name={this.props.to.name}
          time={Math.round(data.time)}
          distance={data.distance / 100}
        >
          <div className="title" onClick={() => this.props.goBack()}>
            <FormattedMessage id="turn-by-turn" defaultMessage="turn by turn" />
          </div>

          <button onClick={() => this.toHomeScreen()}>
            <FormattedMessage id="end" defaultMessage="end" />
          </button>
        </NavigationInfo>

      </div>
    )
  }
}
