import React from 'react'
import { FormattedMessage } from 'react-intl'
import { meterToFoot } from 'src/helpers/misc'
import 'src/styles/NavigationInfo.scss'

const NavigationInfo = ({ name, time, distance, children, isArrived, useImperialSystem }) => (
  <div className='NavigationInfo'>
    <div className="text-wrapper">
      {!isArrived && !isNaN(time) &&
                <div className="text">
                  {time}
                  <span><FormattedMessage id="min" defaultMessage="min" /></span>
                </div>
      }
      {!isArrived && !isNaN(distance) &&
        <div className="text">
          {useImperialSystem ? Math.round(meterToFoot(distance)) : Math.round(distance)}
          <span>
            { useImperialSystem
              ? <FormattedMessage id="ft" defaultMessage="ft" />
              : <FormattedMessage id="m" defaultMessage="m" />
            }
          </span>
        </div>
      }
    </div>
    {children}
  </div>
)

export default NavigationInfo
