import React from 'react'
import { FormattedMessage } from 'react-intl'
import { isWideScreen, meterToFoot } from 'src/helpers/misc'

const getTipTranslate = (key, landscape) => {
  if (landscape && isWideScreen()) {
    if (key.includes('right')) {
      return 'translateX(67%) scale(1.5)'
    }

    if (key.includes('left')) {
      return 'translateX(-67%) scale(1.5)'
    }
  }

  return 'translateX(0%)'
}

const NavigationTip = ({ direction, isArrived, landscape, useImperialSystem }) => (
  <div className="navigation-tips-wrapper">
    <div className="navigation-tips">
      {!isArrived && direction &&
        <img
          className='imgr'
          style={{ transform: getTipTranslate(direction.key, landscape) }}
          src={`/public/images/nav-tips/tip-${direction.key}.svg`}
        />
      }
      {isArrived && <img className='imgr' src={`/public/images/directions/destination.svg`} />}
      <div className="inner">
        <div className='text'>
          {direction && isArrived && <FormattedMessage id="you-have-arrived" defaultMessage="You have arrived!" />}
          {direction && direction.portalDirection && (
            direction.portalDirection === 'Up'
              ? <FormattedMessage id="Up-to" defaultMessage="Up to" />
              : <FormattedMessage id="Down-to" defaultMessage="Down to" />
          )}
        </div>
        {!isArrived && direction && direction.distance !== undefined && direction.distance !== null && !direction.portalDirection &&
          <div className='distance'>
            {direction.distance < 150 && direction.key !== 'straight'
              ? <FormattedMessage id="Now" defaultMessage="Now" />
              : useImperialSystem
                ? <FormattedMessage id="distance-ft" defaultMessage="{distance} ft" values={{ distance: Math.round(meterToFoot(direction.distance / 100)) }} />
                : <FormattedMessage id="distance-m" defaultMessage="{distance} m" values={{ distance: Math.round(direction.distance / 100) }} />
            }
          </div>
        }
        {direction && direction.deck &&
          <div className='distance'><FormattedMessage id="DECK-TO-N" defaultMessage="DECK {number}" values={{ number: direction.deck }} /></div>
        }
      </div>

    </div>
  </div>
)

export default NavigationTip
