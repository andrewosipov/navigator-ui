/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'

import NavigationInfo from './NavigationInfo'

test('NavigationInfo', () => {
  const component = shallow(
    <NavigationInfo />
  )
  component.setProps({ isArrived: false, time: 10, distance: 30 })
  expect(component.find('.text-wrapper .text').first().text()).toEqual('10min')
  expect(component.find('.text-wrapper .text').last().text()).toEqual('30m')

  component.setProps({ isArrived: false, time: 10, distance: 'dist' })
  expect(component.find('.text-wrapper .text').text()).toEqual('10min')

  component.setProps({ isArrived: false, time: 'time', distance: 30 })
  expect(component.find('.text-wrapper .text').text()).toEqual('30m')
})
