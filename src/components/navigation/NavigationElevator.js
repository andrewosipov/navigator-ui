import React from 'react'

import Elevator from '../common/Elevator'


export default ({ direction, down }) => (
    <div className="elevator-screen">
        <Elevator down={down || direction && direction.portalDirection === 'Down'} />
    </div>
)