import $ from 'jquery'
import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import classNames from 'classnames'
import 'src/styles/Navigation.scss'

import NavigationInfo from 'src/containers/navigation/NavigationInfo'
import Path from 'src/models/Path'
import Venue from 'src/models/Venue'
import User from 'src/models/User'
import {
  ORIENTATION_LANDSCAPE,
  ORIENTATION_PORTRAIT,
  DECK_TO_SECTORS_ID,
  CONNECTION_TYPES,
  PAGES
} from 'src/constants'
import { toInt, inRange } from 'src/helpers/misc'
import NavigationTip from 'src/containers/navigation/NavigationTip'
import NavigationMap from './NavigationMap'
import NavigationElevator from './NavigationElevator'
import TryingToConnect from 'src/components/common/TryingToConnect'
import NoConnection from 'src/components/common/NoConnection'

export default class Navigation extends Component {
  constructor (props) {
    super(props)
    this.state = {
      deckContainerHeight: 0,
      deckContainerWidth: 0,
      orientation: this.getOrientation(),
      isArrived: false,
      pathRebuilded: null,
      rotation: this.getRotation(props),

      // this is only for development
      goingDown: false
    }
    this.addEventListeners()
  }

  getRotation () {
    let pathIndex = 0
    if (this.data) {
      pathIndex = this.data.pathIndex
    }
    let paths = this.props.selectedPaths.slice(pathIndex)
    let current = this.props.selectedPaths[pathIndex]
    let portal = paths.find(p => p.vertexes.end.sectorId !== current.vertexes.begin.sectorId)
    let target = this.props.to
    if (portal) {
      target = portal.vertexes.begin
    }
    return this.props.user.x > target.x ? 180 : 0
  }

  getOrientation () {
    return window.innerHeight > window.innerWidth ? ORIENTATION_PORTRAIT : ORIENTATION_LANDSCAPE
  }

  componentWillUnmount () {
    window.onkeydown = null
    window.onresize = null
  }

  addEventListeners () {
    this.onOrientationChange = () => {
      return setTimeout(() => {
        this.updateDeckContainerSize()
      }, 200)
    }

    if ('onorientationchange' in window) {
      window.addEventListener('orientationchange', this.onOrientationChange, false)
    }

    window.onresize = this.updateDeckContainerSize

    window.onkeydown = (e) => {
      let { x, y } = this.props.user
      let deck = toInt(this.props.user.deck)
      let inElevator = false
      let newDeck
      switch (e.code) {
        case 'ArrowUp':
          if (e.shiftKey) {
            if (DECK_TO_SECTORS_ID[deck + 1]) {
              newDeck = deck + (e.ctrlKey ? 4 : 1)
              inElevator = true
              this.setState({ goingDown: false })
            }
          } else {
            x += 100 * Math.cos(this.state.rotation * Math.PI / 180) * (e.altKey ? 10 : 1)
          }
          break
        case 'ArrowDown':
          if (e.shiftKey) {
            if (DECK_TO_SECTORS_ID[deck - 1]) {
              newDeck = deck - (e.ctrlKey ? 4 : 1)
              inElevator = true
              this.setState({ goingDown: true })
            }
          } else {
            x -= 100 * Math.cos(this.state.rotation * Math.PI / 180) * (e.altKey ? 10 : 1)
          }
          break
        case 'ArrowLeft':
          y += 100 * Math.cos(this.state.rotation * Math.PI / 180) * (e.altKey ? 10 : 1)
          break
        case 'ArrowRight':
          y -= 100 * Math.cos(this.state.rotation * Math.PI / 180) * (e.altKey ? 10 : 1)
          break
      }

      if (newDeck) {
        newDeck = inRange(
          toInt(newDeck),
          toInt(Object.keys(DECK_TO_SECTORS_ID)[0]),
          toInt(Object.keys(DECK_TO_SECTORS_ID)[Object.keys(DECK_TO_SECTORS_ID).length - 1])
        )
      }

      $.ajax({
        type: 'post',
        url: 'https://api-admin.simis.ai/restkafka/user_location',
        data: JSON.stringify({
          x,
          y,
          user_id: this.props.user.id,
          sectorId: DECK_TO_SECTORS_ID[deck],
          ts: +(new Date())
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: () => {
          // if (inElevator) {
          setTimeout(() => {
            this.props.setUserPosition(x, y, deck, inElevator)
          }, e.ctrlKey ? 0 : 250)
          // }
        }
      })

      if (inElevator) {
        setTimeout(() => {
          // this.props.setUserPosition(x, y, newDeck, false)
          $.ajax({
            type: 'post',
            url: 'https://api-admin.simis.ai/restkafka/user_location',
            data: JSON.stringify({
              x,
              y,
              user_id: this.props.user.id,
              sectorId: DECK_TO_SECTORS_ID[newDeck],
              ts: +(new Date())
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: () => {
              this.props.setUserPosition(x, y, newDeck, false)
            }
          })
        }, e.ctrlKey ? 250 : 5000)
      }
    }
  }

  initDeckContainer (el) {
    if (!this.deckContainerWrapper) {
      this.deckContainerWrapper = el
      this.updateDeckContainerSize()
    }
  }

  updateDeckContainerSize () {
    this.setState({
      deckContainerWidth: $(window).width(),
      deckContainerHeight: $(window).height()
    })
  }

  componentDidUpdate () {
    if (this.data.isArrived && !this.state.isArrived) {
      this.setState({ isArrived: true })
    }
    let pathRebuilded = +(new Date())
    if (
      this.data.needRebuildPath &&
            (!this.state.pathRebuilded || (pathRebuilded - (this.state.pathRebuilded || 0) > 10000)) &&
            !this.state.isArrived && !this.data.isArrived
    ) {
      if (this.props.to instanceof Venue) {
        this.props.fetchUserToVenuePath(this.props.user, this.props.to, this.props.user.isCrew)
      } else if (this.props.to instanceof User) {
        this.props.fetchUserToUserPath(this.props.user, this.props.to, this.props.user.isCrew)
      }

      this.setState({
        pathRebuilded: pathRebuilded
      })
    }

    if (this.data.path) {
      let sectorId = this.data.path.vertexes.begin.sectorId
      if (sectorId !== this.previousSectorId) {
        let rotation = this.getRotation()
        if (rotation !== this.state.rotation) {
          this.setState({ rotation })
        }
      }

      this.previousSectorId = sectorId
    }
  }

  toHomeScreen () {
    this.props.eraseSelectedDirections()
    this.props.changePage(PAGES.HOME)
  }

  render () {
    const { connectionType } = this.props
    this.data = Path.getUserPathData(this.props.user, this.props.selectedPaths, this.props.venues)
    return (
      <div
        className={classNames('Home', 'Navigation', {
          'crew-only': this.props.crewOnly,
          landscape: this.state.orientation === ORIENTATION_LANDSCAPE
        })}
        style={{ height: $(window).height() }}
      >
        <NavigationTip direction={this.data.direction} isArrived={this.state.isArrived} />

        {connectionType === CONNECTION_TYPES.UNKNOWN &&
          <TryingToConnect
            className="navigation-notify"
            isAbsolute
            withBackground
            theme={this.props.theme}
          />
        }

        {connectionType === CONNECTION_TYPES.NONE &&
          <NoConnection
            className="navigation-notify"
            isAbsolute
            withBackground
            theme={this.props.theme}
          />
        }

        <div className="deck-info">
          <FormattedMessage id="Deck-N" defaultMessage="Deck {number}" values={{ number: this.props.user.deck }} />
        </div>

        {this.props.user.inElevator
          ? <NavigationElevator
            direction={this.data.direction}
            down={this.state.goingDown}
          />
          : <NavigationMap
            initDeckContainer={(e) => this.initDeckContainer(e)}
            deckContainerHeight={this.state.deckContainerHeight}
            deckContainerWidth={this.state.deckContainerWidth}
            orientation={this.state.orientation}
            selected={this.props.to}
            heading={this.props.heading}
            rotation={this.state.rotation}
          />
        }

        <NavigationInfo
          name={this.props.to && this.props.to.name}
          time={Math.round(this.data.time)}
          distance={this.data.distance / 100}
          isArrived={this.state.isArrived}
        >
          {!this.state.isArrived &&
                        <div className="title" onClick={() => this.props.changePage(PAGES.NAVIGATION_OVERVIEW)}>
                          <FormattedMessage id="overview" defaultMessage="overview" />
                        </div>
          }

          <button
            className={classNames('end-button', { arrived: this.state.isArrived })}
            onClick={() => this.toHomeScreen()}>
            <FormattedMessage id="end" defaultMessage="end" />
          </button>
        </NavigationInfo>

      </div>
    )
  }
}
