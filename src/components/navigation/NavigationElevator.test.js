import React from 'react'
import { shallow } from 'enzyme'

import NavigationElevator from './NavigationElevator'
import Elevator from '../common/Elevator'
import data from '../../helpers/tests/data'

test('NavigationElevator', () => {
    let component = shallow(
        <NavigationElevator />
    )

    expect(component.find(Elevator).exists()).toBe(true)
})