import React, { PureComponent } from 'react'
import $ from 'jquery'
import classnames from 'classnames'

import '../styles/Deck.scss'

import AnimatedPaths from './layers/collections/AnimatedPaths'
import Paths from './layers/collections/Paths'
import Venues from './layers/collections/Venues'
import HeatMap from './layers/mapModes/HeatMap'
import User from '../containers/User'
import DestinationContainer from './layers/elements/DestinationContainer'

import {
  WIDTH,
  HEIGHT,
  DECK_SHIFT_X,
  PATH_STROKE_WIDTH,
  DEFAULT_PERSPECTIVE_ROTATION,
  SECTOR_ID_TO_DECKS,
  DECK_TO_SECTORS_ID,
  HEATMAP_MAP_MODE,
  NORMAL_MAP_MODE
} from '../constants'
import { last } from '../helpers/collections'
import { toInt, isSafari } from '../helpers/misc'

/* eslint-disable */
const sectors = require(`./sectors/${SIM_ID}/sectors`).default
/* eslint-enable */

export default class Deck extends PureComponent {
    static defaultProps = {
      perspectiveRotation: 0,
      perspective: 200,
      rotation: 0,
      zoom: 1,
      offsetX: 0,
      offsetY: 0,
      inMove: false,
      isHorizontalOrientation: true,
      svgBaseTransform: [],
      deckPostTransform: [],
      pathStrokeWidth: PATH_STROKE_WIDTH,
      pathBorderWidth: 20,
      rotation3d: 0,
      perspectiveRotation3d: 0,
      scale3d: 0,
      translate3d: null,
      hideVenues: false,
      showDestinationPoint: false,
      initialized: true,
      hideUser: false,

      deck: undefined,
      width: undefined,
      height: undefined,

      selectedVenue: null,
      endVenue: null,
      selectedPaths: [],
      pathsAnimation: false,

      venues: [],
      paths: [],
      user: null,
      users: [],
      party: [],

      onSvgReady: () => null,
      onDeckGroupReady: () => null,
      selectVenue: () => null,
      selectEndVenue: () => null
    }

    constructor (props) {
      super(props)
      this.state = {
        svgWidth: 0,
        svgHeight: 0,
        offsetX: props.offsetX,
        offsetY: props.offsetY,
        zoom: props.zoom
      }
      $(document).on('click', '#svg-main-container .venues-map *, #svg-main-container [id*=-cabins-] *', ({ currentTarget }) => {
        if (this.props.selectVenue && this.props.mapMode === NORMAL_MAP_MODE) { // click on venue area on a map
          let id = last(currentTarget.id.split('-'))
          let venue = this.props.venues.find(e => toInt(e.id) === toInt(id))
          if (venue) {
            this.props.selectVenue(venue)
          }
        }
      })
    }

    componentWillUnmount () {
      $(document).off('click', '#svg-main-container .venues-map *, #svg-main-container [id*=-cabins-] *')
      clearTimeout(this.offsetTimeout)
      clearTimeout(this.zoomTimeout)
    }

    getSvgHeight () {
      return this.props.height
    }

    get3dTransform () {
      let transforms = [...this.props.svgBaseTransform]
      if (this.props.perspectiveRotation) {
        transforms.push(`rotateX(${this.getPerspectiveRotation()}deg)`)
        transforms.push(`rotateY(0deg)`)
        transforms.push(`rotateZ(0deg)`)
        transforms.push(`translate3d(0px, -20px, -20px)`)
      }

      return transforms.join(' ')
    }

    getPerspectiveRotation () {
      if (this.props.perspectiveRotation && this.props.zoom > 5) {
        // Google Chrome has rendering issues here
        // (it crops a part of the transformed object when scale in some rotateX values),
        // so we have to use fixed value here.
        // It even looks like a feature!
        return DEFAULT_PERSPECTIVE_ROTATION
      }
      return this.props.perspectiveRotation
    }

    getRotation () {
      let rotation = 0
      if (!this.props.isHorizontalOrientation) {
        rotation -= 90
      }
      if (this.props.rotation) {
        rotation += this.props.rotation
      }

      return rotation
    }

    getScale () {
      return this.cleanScale(this.props.zoom)
    }

    cleanScale (zoom) {
      let scale = 1
      if (!this.props.isHorizontalOrientation && !this.props.perspectiveRotation) {
        scale = this.props.height / this.props.width
      }

      if (zoom) {
        scale *= zoom
      }

      return scale
    }

    getDeckGroupTransform () {
      let transforms = []
      transforms.push(`scale(${this.getScale()})`)
      return transforms.join(' ')
    }

    getDeckTransform (layer3d = false) {
      let transforms = []

      transforms.push(`rotate(${this.getRotation()}deg)`)

      if (this.props.offsetX) {
        let offsetX = this.props.offsetX / (WIDTH / this.state.svgWidth)
        if (!layer3d) {
          offsetX *= this.getScale()
        }
        transforms.push(`translateX(${offsetX}px)`)
      }

      if (this.props.offsetY) {
        let offsetY = this.props.offsetY / (WIDTH / this.state.svgWidth)
        if (!layer3d) {
          offsetY *= this.getScale()
        }
        transforms.push(`translateY(${offsetY}px)`)
      }

      if (this.props.deckPostTransform) {
        transforms = transforms.concat(this.props.deckPostTransform)
      }

      return transforms.join(' ')
    }

    get3dLayerTransform () {
      return [
        this.getDeckGroupTransform(),
        this.getDeckTransform(true),
        'translate3d(0px, 0px, 20px)',
        `translateY(${this.state.svgHeight / 2}px)`
      ].join(' ')
    }

    getDeck () {
      const deck = this.props.deck || 6
      const props = {
        selected: this.props.selectedVenue,
        endVenue: this.props.endVenue,
        user: this.props.user,
        hideVenues: this.props.hideVenues,
        sectorId: DECK_TO_SECTORS_ID[deck],
        theme: this.props.theme
      }
      const SectorComponent = sectors[DECK_TO_SECTORS_ID[deck]]
      return <SectorComponent {...props} />
    }

    render () { // TODO make shouldRender for submap modes
      return (
        <div
          className={classnames('Deck', { 'in-move': this.props.inMove, initialized: this.props.initialized }, this.props.className)}
          style={{
            perspective: this.props.perspectiveRotation ? '900px' : ''
          }}
        >
          <div
            className="deck-3d-container"
            style={{ transform: this.get3dTransform() }}
          >
            {!this.props.hideVenues && !!this.state.svgWidth &&
              <div
                className="deck-3d-layer"
                style={{
                  transform: this.get3dLayerTransform(),
                  height: `${this.state.svgHeight}px`,
                  width: `${this.state.svgWidth}px`
                }}
              >
                <Venues
                  instances={this.props.venues}
                  categories={this.props.categories}
                  selectVenue={this.props.selectVenue}
                  selected={this.props.selectedVenue}
                  rotation={this.getRotation() + this.props.rotation3d}
                  scale={this.getScale() + this.props.scale3d}
                  perspectiveRotation={this.getPerspectiveRotation() + this.props.perspectiveRotation3d}
                  coordsFactor={WIDTH / this.state.svgWidth}
                  selectedPaths={this.props.selectedPaths}
                  user={this.props.user}
                  deck={this.props.deck}
                  crewOnly={this.props.crewOnly}
                  mapMode={this.props.mapMode}
                />

                {this.props.showDestinationPoint && this.props.selectedPaths.length > 0 && toInt(this.props.deck) === toInt(SECTOR_ID_TO_DECKS[this.props.selectedVenue.sectorId]) &&
                      <DestinationContainer
                        to={this.props.selectedVenue}
                        instance={this.props.selectedPaths[this.props.selectedPaths.length - 1].vertexes.end}
                        rotation={this.getRotation() + this.props.rotation3d}
                        scale={this.getScale() + this.props.scale3d}
                        perspectiveRotation={this.getPerspectiveRotation() + this.props.perspectiveRotation3d}
                        coordsFactor={WIDTH / this.state.svgWidth}
                      />
                }
              </div>
            }

            {this.props.mapMode === HEATMAP_MAP_MODE &&
              <div
                className="deck-3d-layer"
                style={{
                  transform: this.get3dLayerTransform(),
                  height: `${this.state.svgHeight}px`,
                  width: `${this.state.svgWidth}px`
                }}
              >
                <HeatMap
                  {...this.props}
                  rotation={this.getRotation() + this.props.rotation3d}
                  scale={this.getScale() + this.props.scale3d}
                  perspectiveRotation={this.getPerspectiveRotation() + this.props.perspectiveRotation3d}
                  coordsFactor={WIDTH / this.state.svgWidth}
                />
              </div>
            }

            <div style={{ transform: isSafari() && !this.props.dynamicDeck ? 'translate(0,0)' : undefined }}>
              {/* ^^^ this is patch for ios safari
                this.props.dynamicDeck is for fix bug in navigate mode with hiding icons while swapping a deck
              */}

              <div
                className="deck-layers-container"
                style={{
                  height: this.props.height,
                  transform: this.getDeckTransform()
                }}
              >
                <div style={{ transform: this.getDeckGroupTransform() }}>
                  <svg
                    viewBox={`0 0 ${WIDTH} ${HEIGHT}`}
                    xmlns='http://www.w3.org/2000/svg'
                    xmlnsXlink='http://www.w3.org/1999/xlink'
                    ref={el => {
                      this.props.onSvgReady && this.props.onSvgReady(el)
                      let svgWidth = toInt($(el).width())
                      let svgHeight = toInt($(el).height())

                      if (svgWidth && (svgWidth !== this.state.svgWidth || svgHeight !== this.state.svgHeight)) {
                        this.setState({ svgWidth, svgHeight })
                      }
                    }}
                    height={this.props.height}
                    width={this.props.width}
                  >
                    <defs>
                      <filter x="0" y="0" width="1" height="1" id="white-background">
                        <feFlood floodColor="white"/>
                        <feComposite in="SourceGraphic"/>
                      </filter>

                      <filter id="user-shadow" height="130%">
                        <feGaussianBlur in="SourceAlpha" stdDeviation="8"/>
                        <feOffset dx="-8" dy="0" result="offsetblur"/>
                        <feComponentTransfer>
                          <feFuncA type="linear" slope="0.5"/>
                        </feComponentTransfer>
                        <feMerge>
                          <feMergeNode/>
                          <feMergeNode in="SourceGraphic"/>
                        </feMerge>
                      </filter>

                      <filter id="deck-shadow" height="130%">
                        <feDropShadow floodColor={this.props.theme === 'dark' ? '#000' : '#fff'} dx="-200" dy="0" stdDeviation="450" floodOpacity="1" />
                      </filter>

                      <filter id="path-shadow" height="130%">
                        <feGaussianBlur in="SourceAlpha" stdDeviation="80"/>
                        <feOffset dx="-10" dy="-60" result="offsetblurBlack"/>
                        <feFlood floodColor="#000" floodOpacity="1" result="offsetColor"/>
                        <feComposite in="offsetColor" in2="offsetblurBlack" operator="in" result="offsetblurBlack"/>

                        <feFlood floodColor="#fff" result="border"/>
                        <feMorphology in="SourceAlpha" operator="dilate" radius={this.props.pathBorderWidth} />
                        <feComposite in="border" operator="in" result="border"/>

                        <feComponentTransfer>
                          <feFuncA type="linear" slope="2"/>
                        </feComponentTransfer>
                        <feMerge>
                          <feMergeNode in="border" />
                          <feMergeNode/>
                          <feMergeNode in="offsetblurBlack" />
                          <feMergeNode/>
                          <feMergeNode in="SourceGraphic"/>
                        </feMerge>
                      </filter>

                      <radialGradient id="user-heading-shadow" cx="0" cy="0">
                        <stop offset="0%" style={{ stopColor: 'rgb(66, 133, 244)', stopOpacity: '0.6' }} />
                        <stop offset="100%" style={{ stopColor: 'rgb(119, 187, 250)', stopOpacity: '0' }} />
                      </radialGradient>

                      <linearGradient x1="35.4208767%" y1="50%" x2="98.9472063%" y2="50%" id="lighting-gradient-1">
                        <stop stopColor="#34353B" stopOpacity="0" offset="0%"></stop>
                        <stop stopColor="#34353B" stopOpacity="0.151183197" offset="100%"></stop>
                      </linearGradient>
                      <linearGradient x1="0%" y1="50%" x2="100%" y2="50%" id="lighting-gradient-2">
                        <stop stopColor="#FFFFFF" stopOpacity="0.158542799" offset="0%"></stop>
                        <stop stopColor="#FFFFFF" stopOpacity="0" offset="100%"></stop>
                      </linearGradient>
                    </defs>

                    <g
                      fill='none'
                      fillRule='evenodd'
                      id="main-container"
                      style={{
                        filter: this.props.hideVenues && `url(#deck-shadow)`
                        // transform: this.getDeckGroupTransform(),
                      }}
                    >
                      <g ref={el => this.props.onDeckGroupReady && this.props.onDeckGroupReady(el)}>
                        <g style={{ transform: `translateX(${DECK_SHIFT_X}px)` }}>
                          <rect id='box' fillRule='nonzero' width='1' height='4592' />
                          <g className="deck-container____" ref={el => {
                            this.deckContainer = el
                            this.props.onDeckContainerReady && this.props.onDeckContainerReady(el)
                          }}>
                            {this.getDeck()}
                          </g>
                        </g>
                      </g>

                      {!!this.props.paths.length &&
                        <Paths instances={this.props.paths} />
                      }

                      <AnimatedPaths
                        instances={this.props.selectedPaths}
                        deck={this.props.deck}
                        strokeWidth={this.props.pathStrokeWidth}
                        animation={this.props.pathsAnimation}
                        zoom={this.props.zoom}
                      />

                      {!this.props.hideUser && this.props.user && +this.props.user.deck === +this.props.deck &&
                        <User instance={this.props.user} zoom={this.props.zoom} />
                      }

                      {this.props.party.filter(u => +u.deck === +this.props.deck && u.hardcoded).map(u =>
                        <User key={u.id} instance={u} zoom={this.props.zoom} party />
                      )}
                    </g>

                  </svg>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }
}
