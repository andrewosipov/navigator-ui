import { get } from 'lodash'

export const getSelectedVenue = state => {
  const venueId = get(state, 'deck.selectedVenue.id')

  return state.venues.venues.find(venue => venue.id === venueId)
}
