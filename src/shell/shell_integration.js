/* eslint-disable */
import get from 'lodash/get'
import integrationHandler from './integrationHandler'
import { getMobileOperatingSystem } from '../helpers/misc'

let devicePlatform
let globalBridge = ''
const isGuestTokenRecieved = false

export default (window.initializeShellIntegration = function() {
    devicePlatform = getMobileOperatingSystem()
    if (devicePlatform === null || devicePlatform === undefined || devicePlatform === '') {
        devicePlatform = 'IOS'
    }
    if (devicePlatform.toLowerCase() === 'ios') {
        setupWebViewJavascriptBridge(function(bridge) {
            globalBridge = bridge
            bridge.registerHandler('provideInfoToJS', function(data, responseCallback) {
                provideInfoToJS(data)
                responseCallback(data)
            })
        })
    }
})

function setupWebViewJavascriptBridge(callback) {
    if (window.WebViewJavascriptBridge) {
        return callback(WebViewJavascriptBridge)
    }

    if (window.WVJBCallbacks) {
        return window.WVJBCallbacks.push(callback)
    }

    window.WVJBCallbacks = [callback]
    const WVJBIframe = document.createElement('iframe')
    WVJBIframe.style.display = 'none'
    WVJBIframe.src = 'wvjbscheme://__BRIDGE_LOADED__'
    document.documentElement.appendChild(WVJBIframe)
    setTimeout(function() {
        document.documentElement.removeChild(WVJBIframe)
    }, 0)
    return true
}

window.provideInfoToJS = function({ informationType, actionData }) {
    integrationHandler(informationType, actionData)
}

let provideInfoToJS = window.provideInfoToJS

window.provideInfoToShell = function(infoType, actionData) {
    try {
        if (devicePlatform) {
            if (devicePlatform.toLowerCase() === 'android') {
                const data = JSON.stringify({ informationType: infoType, actionData })
                try {
                    android && android.provideInfoToNativeShell(data) // this function will be register in android shell
                } catch (error) {
                    console.log('error: ')
                    console.log(error)
                }

            } else if (devicePlatform.toLowerCase() === 'ios') {
                const data = {
                    informationType: infoType,
                    actionData: actionData
                }
                globalBridge.callHandler && globalBridge.callHandler('provideInfoToShell', data, response => {})
            }
        }
    } catch (e) {
        console.log(e)
    }
}
