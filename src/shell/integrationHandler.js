/* global CustomEvent */
import {
  receiveUserHeading,
  changeCrewOnly,
  changePage,
  changeTheme,
  updateUser,
  fetchReservationGuest,
  receiveAccessToken,
  changeConnection,
  communicationChannelEstablished,
  fetchUserPreferences
} from 'src/actions/app'
import {
  selectInitialVenue
} from 'src/actions/deck'
import { store } from 'src/store'
import { SHELL_CONFIG } from 'src/constants'
import { toInt } from 'src/helpers/misc'

const integrationHandler = (informationType, actionData) => {
  console.log(informationType, actionData)
  const state = store.getState()

  switch (informationType.toLowerCase()) {
    case 'shareadvancedmoduleconfiguration':
      if (actionData.Account) {
        if (actionData.Account.PersonalDetails) {
          store.dispatch(changeCrewOnly(true))
          store.dispatch(updateUser(actionData.Account))
          if (actionData.Account.ReservationGuestId && state.app.accessToken) {
            store.dispatch(fetchReservationGuest(actionData.Account.ReservationGuestId, state.app.accessToken))
            store.dispatch(fetchUserPreferences())
          }
        }
      }
      break
    case 'changepage':
      store.dispatch(changePage(actionData.page))
      break
    case 'proximity':
      const proximityEvent = new CustomEvent('proximity', { detail: actionData })
      window.dispatchEvent(proximityEvent)
      break
    case 'communicationchannelestablished':
      try {
        window.provideInfoToShell('getConfiguration', { code: SHELL_CONFIG.code })
        window.provideInfoToShell('startSendingCompassHeading', { ThresholdChangeInDegrees: 5.0 })
        store.dispatch(communicationChannelEstablished(actionData))

        if (state.venues.venues) {
          store.dispatch(selectInitialVenue(state.venues.venues, actionData.venueId, actionData.globalVenueId))
        }
      } catch (e) {
        console.log(e)
      }
      break
    case 'sharecompassheading':
      let nR = actionData.MagneticHeading
      let rot = parseFloat(state.app.userHeading)
      let aR = rot % 360
      if (aR < 0) {
        aR += 360
      }
      if (aR < 180 && (nR > (aR + 180))) {
        rot -= 360
      }
      if (aR >= 180 && (nR <= (aR - 180))) {
        rot += 360
      }
      rot += (nR - aR)
      store.dispatch(receiveUserHeading(rot))
      break

    case 'sessiondata':
      if (actionData.theme) {
        store.dispatch(changeTheme(actionData.theme))
      }
      store.dispatch(receiveAccessToken(actionData.authToken))
      if (state.app.user.ReservationGuestId) {
        store.dispatch(fetchReservationGuest(state.app.user.ReservationGuestId, actionData.authToken))
      }
      // const actionData = {
      // module code
      //     code: 'EXR',
      // token information
      //     expireTime: expireTime,
      //     authToken: accessTokenWithoutType,
      //     tokenType: tokenType,
      //     guestUUIDToken: accessTokenWithoutType,
      //     refreshToken: refreshToken,
      // nfc beacon related information
      //     isNFCEnabled: isNFCEnabled,
      // language selected
      //     language: Language,
      // theme selected
      //     theme: Theme,
      // reservation number of the guest
      //     reservationNumber: reservationNumber,
      // basic guest information
      //     userDetails: userDetails
      // };

      // console.log(actionData)
      break

    case 'shareconfiguration':
      // const actionData = {
      // 'https://dxp-shore.decurtiscorp.com/svc/activity-reservation-system-bff/'
      // }

      break

    case 'sharechangedtheme':
    case 'devicetheme':
      if (toInt(actionData.Theme) === 2) {
        store.dispatch(changeTheme('light'))
      } else {
        store.dispatch(changeTheme('dark'))
      }
      break

    case 'devicelanguage':
      // const actionData = {
      // Language: 'es-ES'
      // }
      break

    case 'connectionchange':
      store.dispatch(changeConnection(actionData.type))
      break
  }
}

export default integrationHandler
